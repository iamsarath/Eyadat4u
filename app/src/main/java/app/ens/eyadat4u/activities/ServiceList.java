package app.ens.eyadat4u.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.ServiceListAdapter;
import app.ens.eyadat4u.items.FilterDayCheckItems;
import app.ens.eyadat4u.items.FilterFeesCheckItems;
import app.ens.eyadat4u.items.FilterTimeCheckItems;
import app.ens.eyadat4u.items.MainCategoryItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class ServiceList extends BaseActivity {

    Toolbar             toolbar;
    ListView            list_lv;
    TextView            toolbarTitle_tv, toolbarSubTitle_tv, totalResults_tv, sortText_tv, filterText_tv, loadMoreText_tv;
    ServiceListAdapter  listAdapter;
    String              titleText, subTitleText, serviceId;
    LinearLayout        parentLayout_ll, progressLayout_ll, dataLayout_ll, noDataLayout_ll, filterLayout_ll, sortLayout_ll,
                        sortByPriceLow_ll, sortByPriceHigh_ll, sortByRating_ll, searchMedicine_ll, loadMore_ll;
    CardView            customSearchLayout_cv;
    EditText            medicineSearchText_et;
    ProgressBar         progressBar_pb, loadMoreProgressBar_pb;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;
    Point               p;
    PopupWindow         popup;
    View                footerView_vw;

    boolean             isVisitingDoc           = false;
    boolean             sort_by_top_rated       = false;
    boolean             sort_by_availability    = false;

    String fromTime                 = "";
    String toTime                   = "";
    String fromFees                 = "";
    String toFees                   = "";
    String filterDays               = "";
    String medicineSearchString     = "";
    String customSearch             = "";
    String serviceType              = "";
    String serviceName              = "";
    boolean isFeeSelected           = false;

    int                 totalRecords            = 0;
    int                 currentItemCount        = 0;
    boolean             loadingMore = false;
    int                 pageNumber =0;

    public static ArrayList<SearchItems> searchItemsArrayList   = new ArrayList<SearchItems>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_list);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        int[] location = new int[2];
        p = new Point();
        p.x = location[0];
        p.y = location[1];

        queue           = Volley.newRequestQueue(ServiceList.this);

        searchItemsArrayList.clear();

        titleText       = getIntent().getExtras().getString("TITLE");
        subTitleText    = getIntent().getExtras().getString("SUB_TITLE");
        serviceId       = getIntent().getExtras().getString("SERVICE_ID");
        isVisitingDoc   = getIntent().getExtras().getBoolean("VISITING_DOCTOR");
        serviceType     = getIntent().getExtras().getString("SERVICE_TYPE");
        customSearch    = getIntent().getExtras().getString("CUSTOM_SEARCH");

        setDaysArrayList();
        setTimeArrayList();
        setFeesArrayList();

        listAdapter = new ServiceListAdapter(ServiceList.this, searchItemsArrayList, customSearch);
        initViews();
        setToolbar();
        list_lv.setAdapter(listAdapter);

        if(customSearch.equals("medicine_search")){

            customSearchLayout_cv.setVisibility(View.VISIBLE);
        }
        else{

            customSearchLayout_cv.setVisibility(View.GONE);
        }

        setTexts();

        loadMore_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pageNumber = currentItemCount/10;

                //calling search API
                loadMoreProgressBar_pb.setVisibility(View.VISIBLE);
                loadMoreText_tv.setText("");
                loadMoreText_tv.setText("Loading ...");
                String obj = createFilterParamsObj(UserManager.getUserLanguage(ServiceList.this), ""+pageNumber, serviceId,
                        filterDays, fromTime, toTime, fromFees, toFees, isFeeSelected, sort_by_top_rated, sort_by_availability,
                        customSearch, medicineSearchString, serviceType);
                serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
            }
        });

        list_lv.setOnScrollListener(new AbsListView.OnScrollListener(){

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                currentItemCount = lastInScreen;
                if((lastInScreen == totalItemCount) && (loadingMore) && (currentItemCount-1 != totalRecords)){

                    int balItems = totalRecords - (currentItemCount-1);
                    int tot = balItems +10;
//                    loadMoreText_tv.setText("More "+serviceName+" ("+balItems+" More) ");
                    loadMoreText_tv.setText("Load More "+"("+balItems+" Items)");

                    loadMore_ll.setVisibility(View.VISIBLE);
                }
                else{

                    loadMore_ll.setVisibility(View.GONE);
                }
            }
        });

        searchMedicine_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                medicineSearchString = medicineSearchText_et.getText().toString();

                if(medicineSearchString.equals("")){

                    Toast.makeText(ServiceList.this,"Type a medicine name for searching !",Toast.LENGTH_LONG).show();
                }
                else{

                    searchItemsArrayList.clear();
                    pageNumber = 0;
                    showProgressView();
                    String obj = createFilterParamsObj(UserManager.getUserLanguage(ServiceList.this), ""+pageNumber, serviceId,
                            filterDays, fromTime, toTime, fromFees, toFees, isFeeSelected, sort_by_top_rated, sort_by_availability,
                            customSearch, medicineSearchString, serviceType);
                    serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
                }
            }
        });

        filterLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(ServiceList.this, Filter.class);
                intent.putExtra("SERVICE_ID",serviceId);
                startActivityForResult(intent, 1);
            }
        });

        sortLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                showSortPopup(ServiceList.this, p);
            }
        });

        //calling search API

        if(isVisitingDoc){

            searchItemsArrayList.clear();
            pageNumber = 0;

            showProgressView();
            String obj = visitingDocsObj(UserManager.getUserLanguage(ServiceList.this),""+pageNumber);
            serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
        }
        else{

            searchItemsArrayList.clear();
            pageNumber = 0;

            showProgressView();
            String obj = createFilterParamsObj(UserManager.getUserLanguage(ServiceList.this), ""+pageNumber, serviceId,
                    filterDays, fromTime, toTime, fromFees, toFees, isFeeSelected, sort_by_top_rated, sort_by_availability,
                    customSearch, medicineSearchString, serviceType);
            serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
        }
    }

    void setTexts(){

        if(UserManager.getUserLanguage(ServiceList.this).equals("en")){

            sortText_tv.setText(R.string.sort_en);
            filterText_tv.setText(R.string.filter_en);
        }else{

            sortText_tv.setText(R.string.sort_ar);
            filterText_tv.setText(R.string.filter_ar);
        }
    }

    void setDaysArrayList(){

        String [] daysArray;
        daysArray               = new String[7];
        daysArray [0]           = "Mon";
        daysArray [1]           = "Tue";
        daysArray [2]           = "Wed";
        daysArray [3]           = "Thu";
        daysArray [4]           = "Fri";
        daysArray [5]           = "Sat";
        daysArray [6]           = "Sun";
        Constants.filterDayCheckItemsArrayList.clear();

        for (int i=0; i< 7; i++){

            FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[i], false);
            Constants.filterDayCheckItemsArrayList.add(tempCheckList);
        }

    }

    void setTimeArrayList() {
        String [] timeArray;
        timeArray = new String[4];
        timeArray [0] = "Before 12:00 PM";
        timeArray [1] = "12:00 PM To 4:00 PM";
        timeArray [2] = "4:00 PM To 8:00 PM";
        timeArray [3] = "After 8:00 PM";
        Constants.filterTimeCheckItemsArrayList.clear();

        for (int i=0; i< 4; i++){

            FilterTimeCheckItems tempCheckList = new FilterTimeCheckItems(timeArray[i], false);
            Constants.filterTimeCheckItemsArrayList.add(tempCheckList);
        }
    }

    void setFeesArrayList() {
        String [] feesArray;
        feesArray = new String[4];
        feesArray [0] = "Below 50KD";
        feesArray [1] = "B/W 50KD & 100KD";
        feesArray [2] = "B/W 100KD & 500KD";
        feesArray [3] = "Above 500KD";
        Constants.filterFeesCheckItemsArrayList.clear();

        for (int i=0; i< 4; i++){

            FilterFeesCheckItems tempCheckList = new FilterFeesCheckItems(feesArray[i], false);
            Constants.filterFeesCheckItemsArrayList.add(tempCheckList);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            if(resultCode == Activity.RESULT_OK){
//                String result=data.getStringExtra("FILTER_RESULT");

                searchItemsArrayList.clear();
                pageNumber = 0;

                fromTime        = data.getStringExtra("FILTER_FROM_TIME");
                toTime          = data.getStringExtra("FILTER_TO_TIME");
                fromFees        = data.getStringExtra("FILTER_FROM_FEES");
                toFees          = data.getStringExtra("FILTER_TO_FEES");
                filterDays      = data.getStringExtra("FILTER_DAYS");
                isFeeSelected   = data.getExtras().getBoolean("IS_FEE_SELECTED");

                String obj = createFilterParamsObj(UserManager.getUserLanguage(ServiceList.this), ""+pageNumber, serviceId,
                        filterDays, fromTime, toTime, fromFees, toFees, isFeeSelected, sort_by_top_rated, sort_by_availability,
                        customSearch, medicineSearchString, serviceType);

                showProgressView();
                serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
            }
        }
    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        toolbarSubTitle_tv      = (TextView) findViewById(R.id.toolbar_sub_title);
        totalResults_tv         = (TextView) findViewById(R.id.total_results);
        list_lv                 = (ListView) findViewById(R.id.list);
        progressLayout_ll       = (LinearLayout) findViewById(R.id.progress_view);
        dataLayout_ll           = (LinearLayout) findViewById(R.id.data_view);
        noDataLayout_ll         = (LinearLayout) findViewById(R.id.no_data_view);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        filterLayout_ll         = (LinearLayout) findViewById(R.id.filter_layout);
        sortLayout_ll           = (LinearLayout) findViewById(R.id.sort_layout);
        progressBar_pb          = (ProgressBar) findViewById(R.id.progressBar);
        sortText_tv             = (TextView) findViewById(R.id.sort_text);
        filterText_tv           = (TextView) findViewById(R.id.filter_text);
        searchMedicine_ll       = (LinearLayout) findViewById(R.id.search_medicine);
        medicineSearchText_et   = (EditText) findViewById(R.id.search_text);
        customSearchLayout_cv   = (CardView) findViewById(R.id.custom_search_layout);
        footerView_vw           = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_loadmore_layout, null, false);
        loadMoreProgressBar_pb  = (ProgressBar) footerView_vw.findViewById(R.id.load_more_progress_bar);

        loadMore_ll             = (LinearLayout) footerView_vw.findViewById(R.id.load_more);
        loadMoreText_tv         = (TextView) footerView_vw.findViewById(R.id.load_more_text);
        list_lv.addFooterView(footerView_vw);
        loadMoreProgressBar_pb.setVisibility(View.GONE);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText(trimText(subTitleText));
//        toolbarSubTitle_tv.setText(trimText(subTitleText));
        toolbarSubTitle_tv.setVisibility(View.GONE);
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
        noDataLayout_ll.setVisibility(View.GONE);
        loadMoreProgressBar_pb.setVisibility(View.GONE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.GONE);
    }

    public void showNoDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.VISIBLE);
    }

    private String createFilterParamsObj(String language, String pageNumber, String serviceId,
                                         String filter_by_day, String time_aval_from, String time_aval_to,
                                         String consult_fee_from, String consult_fee_to,  boolean is_fee_selected,
                                         boolean sort_by_top_rated, boolean sort_by_availability,
                                         String custom_search, String medicine_name, String service_type) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );
            mainObj.put( "page_number", pageNumber );
            mainObj.put( "service_id", serviceId );
            mainObj.put( "is_fee_selected", is_fee_selected);
            mainObj.put( "search_days", filter_by_day );
            mainObj.put( "time_aval_from", time_aval_from );
            mainObj.put( "time_aval_to", time_aval_to );
            mainObj.put( "consult_fee_from", consult_fee_from );
            mainObj.put( "consult_fee_to", consult_fee_to );
            mainObj.put( "sort_by_top_rated", sort_by_top_rated );
            mainObj.put( "sort_by_availability", sort_by_availability );
            mainObj.put( "custom_search", custom_search);
            mainObj.put( "medicinename", medicine_name);
            mainObj.put( "service_type", service_type);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String visitingDocsObj(String language, String pageNumber) {

        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put( "language", language );
            mainObj.put( "page_number", pageNumber );
            mainObj.put( "is_visiting", true );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void serviceListApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(ServiceList.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseServiceList(result);
                        loadingMore = true;

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseServiceList(String response){

        JSONObject jObj;
        try {
//            searchItemsArrayList.clear();
            jObj = new JSONObject(response);
            JSONObject jObjSearchResult= null;
            String result, totalCount;
            result= jObj.getString("search_result");
            jObjSearchResult= new JSONObject(result);
            totalCount = jObjSearchResult.getString("total_count");
            if(jObjSearchResult.has("total_count")){

                totalRecords = jObjSearchResult.getInt("total_count");
            }

            /*clinics cat data array variables*/
            if(jObjSearchResult.has("results")){

                /* clinic list array variables */
                String searchListArray, type_name_ = null, type_= null, item_id_= null, item_name_= null, item_thumb_= null, clinic_name_= null, location_= null,
                 qualification_= null,  experience_= null, rating_= null, views_= null;
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjSearchResult.getJSONArray("results");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        searchListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(searchListArray);

                        if (jObjNewCatList.has("id")) {

                            item_id_ = jObjNewCatList.getString("id");
                        }
                        if (jObjNewCatList.has("service_type_name")) {

                            type_name_ = jObjNewCatList.getString("service_type_name");
                            serviceName = type_name_;
                        }
                        if (jObjNewCatList.has("service_type")) {

                            type_ = jObjNewCatList.getString("service_type");
                        }
                        if (jObjNewCatList.has("name")) {

                            item_name_ = jObjNewCatList.getString("name");
                        }
                        if (jObjNewCatList.has("thumbnail_url")) {

                            item_thumb_ = jObjNewCatList.getString("thumbnail_url");
                        }
                        if (jObjNewCatList.has("clinic_name")) {

                            clinic_name_ = jObjNewCatList.getString("clinic_name");
                        }
                        if (jObjNewCatList.has("locality_name")) {

                            location_ = jObjNewCatList.getString("locality_name");
                        }
                        if (jObjNewCatList.has("qualifications")) {

                            qualification_ = jObjNewCatList.getString("qualifications");
                        }
                        if (jObjNewCatList.has("years_of_experience")) {

                            experience_ = jObjNewCatList.getString("years_of_experience");
                        }
                        if (jObjNewCatList.has("rating")) {

                            rating_ = jObjNewCatList.getString("rating");
                        }
                        if (jObjNewCatList.has("counter_views")) {

                            views_ = jObjNewCatList.getString("counter_views");
                        }

                        SearchItems tempCatList = new SearchItems(type_name_, type_, item_id_, item_name_, item_thumb_, clinic_name_, location_, qualification_, experience_, rating_, views_);
                        searchItemsArrayList.add(tempCatList);
                    }
                }
            }

            loadMore_ll.setVisibility(View.GONE);
            if(searchItemsArrayList.size()==0){
                showNoDataView();
            }
            else{
                listAdapter.notifyDataSetChanged();
                showDataView();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }

    public String trimText(String text){

        if(text.length() >= 20){
            String s = text.substring(0,20);
            return s+"...";
        }
        else{

            return text;
        }
    }

    // The method that displays the popup.
    private void showSortPopup(final Activity context, Point p) {

        final int TIME_OUT  = 1500;

        final WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        int popupWidth = width;
        int popupHeight = height;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout   sortByPriceLow_ll, sortByPriceHigh_ll, sortByAvailability_ll, sortByRating_ll;
//        final ImageView tickAvailability_iv, tickRating_iv;
        final TextView topRated_tv, availablity_tv, sortByText_tv;

        View layout             = layoutInflater.inflate(R.layout.popup_sort, viewGroup);
        sortByPriceLow_ll       = (LinearLayout) layout.findViewById(R.id.sort_by_price_low);
        sortByPriceHigh_ll      = (LinearLayout) layout.findViewById(R.id.sort_by_price_high);
        sortByAvailability_ll   = (LinearLayout) layout.findViewById(R.id.sort_by_availability);
        sortByRating_ll         = (LinearLayout) layout.findViewById(R.id.sort_by_rating);
//        tickAvailability_iv     = (ImageView) layout.findViewById(R.id.tick_availability);
//        tickRating_iv           = (ImageView) layout.findViewById(R.id.tick_rating);
        topRated_tv             = (TextView) layout.findViewById(R.id.text_top_rated);
        availablity_tv          = (TextView) layout.findViewById(R.id.text_availability);
        sortByText_tv           = (TextView) layout.findViewById(R.id.sort_by_text);

        if(UserManager.getUserLanguage(ServiceList.this).equals("en")){

            topRated_tv.setText(R.string.top_rated_en);
            availablity_tv.setText(R.string.available_today_en);
            sortByText_tv.setText(R.string.sort_by_en);
        }else{

            topRated_tv.setText(R.string.top_rated_ar);
            availablity_tv.setText(R.string.available_today_ar);
            sortByText_tv.setText(R.string.sort_by_ar);
        }

        if (sort_by_availability){

//            tickAvailability_iv.setVisibility(View.VISIBLE);
            availablity_tv.setTextColor(ContextCompat.getColor(context, R.color.ColorPrimary));
        }
        else{

//            tickAvailability_iv.setVisibility(View.GONE);
            availablity_tv.setTextColor(ContextCompat.getColor(context, R.color.TextMain));
        }

        if (sort_by_top_rated){

//            tickRating_iv.setVisibility(View.VISIBLE);
            topRated_tv.setTextColor(ContextCompat.getColor(context, R.color.ColorPrimary));

        }
        else{

//            tickRating_iv.setVisibility(View.GONE);
            topRated_tv.setTextColor(ContextCompat.getColor(context, R.color.TextMain));

        }

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);
//
//        popup.getContentView().setFocusableInTouchMode(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 10;
        int OFFSET_Y = 10;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

//        sortByPriceHigh_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                popup.dismiss();
//            }
//        });
//
//        sortByPriceLow_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                popup.dismiss();
//            }
//        });

        sortByAvailability_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sort_by_availability){

                    sort_by_availability = false;
                }
                else{
                    sort_by_availability = true;
                }

                if (sort_by_availability){

//                    tickAvailability_iv.setVisibility(View.VISIBLE);
                    availablity_tv.setTextColor(ContextCompat.getColor(context, R.color.ColorPrimary));
                }
                else{

//                    tickAvailability_iv.setVisibility(View.GONE);
                    availablity_tv.setTextColor(ContextCompat.getColor(context, R.color.TextMain));
                }

                searchItemsArrayList.clear();
                pageNumber = 0;

                popup.dismiss();
                String obj = createFilterParamsObj(UserManager.getUserLanguage(ServiceList.this), ""+pageNumber, serviceId,
                        filterDays, fromTime, toTime, fromFees, toFees, isFeeSelected, sort_by_top_rated, sort_by_availability,
                        customSearch, medicineSearchString, serviceType);

                showProgressView();
                serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
            }
        });

        sortByRating_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sort_by_top_rated){

                    sort_by_top_rated = false;
                }
                else{
                    sort_by_top_rated = true;
                }

                if (sort_by_top_rated){

//                    tickRating_iv.setVisibility(View.VISIBLE);
                    topRated_tv.setTextColor(ContextCompat.getColor(context, R.color.ColorPrimary));
                }
                else{

//                    tickRating_iv.setVisibility(View.GONE);
                    topRated_tv.setTextColor(ContextCompat.getColor(context, R.color.TextMain));
                }

                searchItemsArrayList.clear();
                pageNumber = 0;

                popup.dismiss();
                String obj = createFilterParamsObj(UserManager.getUserLanguage(ServiceList.this), ""+pageNumber, serviceId,
                        filterDays, fromTime, toTime, fromFees, toFees, isFeeSelected, sort_by_top_rated, sort_by_availability,
                        customSearch, medicineSearchString, serviceType);

                showProgressView();
                serviceListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_SEARCH_LIST), obj);
            }
        });

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
