
package app.ens.eyadat4u.activities;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.MedicineListAdapter;
import app.ens.eyadat4u.adapters.NewsAndEventsAdapter;
import app.ens.eyadat4u.items.MedicineItems;
import app.ens.eyadat4u.items.NewsAndEventsItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class MedicineSearch extends BaseActivity {

    Toolbar             toolbar;
    ListView            medicinesList_lv;
    TextView            toolbarTitle_tv, loadMoreText_tv;
    EditText            medicineSearchText_et;
    LinearLayout        parentLayout_ll, progressLayout_ll, dataLayout_ll, searchMedicine_ll, loadMore_ll;
    View                footerView_vw;
    ProgressBar         progressBar_pb, loadMoreProgressBar_pb;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;

    String              medicineSearchString    = "";
    String              serviceId               = "";
    int                 totalRecords            = 0;
    int                 currentItemCount        = 0;
    boolean             loadingMore = false;
    int                 pageNumber =0;

    MedicineListAdapter medicineListAdapter;
    ArrayList<MedicineItems> medicineItemsArrayList_   = new ArrayList<MedicineItems>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_search);

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue                  = Volley.newRequestQueue(MedicineSearch.this);
        medicineListAdapter    = new MedicineListAdapter(MedicineSearch.this, medicineItemsArrayList_);
        initViews();
        setToolbar();

        serviceId              = getIntent().getExtras().getString("SERVICE_ID");
        medicinesList_lv.setAdapter(medicineListAdapter);

        medicineItemsArrayList_.clear();

        //calling News & Events API
        showProgressView();
        String obj = medicineListObj(UserManager.getUserLanguage(MedicineSearch.this), "0", serviceId, "");
        medicineListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICINE_SEARCH), obj);

        loadMore_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pageNumber = currentItemCount/10;

                //calling News & Events API
                loadMoreProgressBar_pb.setVisibility(View.VISIBLE);
                String obj = medicineListObj(UserManager.getUserLanguage(MedicineSearch.this), ""+pageNumber, serviceId, "");
                medicineListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICINE_SEARCH), obj);
            }
        });

        searchMedicine_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                medicineSearchString = medicineSearchText_et.getText().toString();

                if(medicineSearchString.equals("")){

                    Toast.makeText(MedicineSearch.this,"Type a medicine name for searching !",Toast.LENGTH_LONG).show();
                }
                else{
                    medicineItemsArrayList_.clear();
                    pageNumber = 0;
                    showProgressView();
                    String obj = medicineListObj(UserManager.getUserLanguage(MedicineSearch.this), ""+pageNumber, serviceId, medicineSearchString);
                    medicineListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICINE_SEARCH), obj);
                }
            }
        });

        medicinesList_lv.setOnScrollListener(new AbsListView.OnScrollListener(){

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                currentItemCount = lastInScreen;

                if((lastInScreen == totalItemCount) && (loadingMore) && (currentItemCount-1 != totalRecords)){

                    int balItems = totalRecords - (currentItemCount-1);

                    loadMoreText_tv.setText("Load Medicines ("+balItems+" More) ");

                    loadMore_ll.setVisibility(View.VISIBLE);
                }
                else{

                    loadMore_ll.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        medicinesList_lv        = (ListView) findViewById(R.id.medicines_list);
        progressLayout_ll       = (LinearLayout) findViewById(R.id.progress_view);
        dataLayout_ll           = (LinearLayout) findViewById(R.id.data_view);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        progressBar_pb          = (ProgressBar) findViewById(R.id.progressBar);
        medicineSearchText_et   = (EditText) findViewById(R.id.search_text);
        searchMedicine_ll       = (LinearLayout) findViewById(R.id.search_medicine);
        footerView_vw           = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_loadmore_layout, null, false);
        loadMore_ll             = (LinearLayout) footerView_vw.findViewById(R.id.load_more);
        loadMoreText_tv         = (TextView) footerView_vw.findViewById(R.id.load_more_text);
        loadMoreProgressBar_pb  = (ProgressBar) footerView_vw.findViewById(R.id.load_more_progress_bar);

        medicinesList_lv.addFooterView(footerView_vw);
        loadMoreProgressBar_pb.setVisibility(View.GONE);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);

        loadMore_ll.setVisibility(View.GONE);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(MedicineSearch.this).equals("en")){

            toolbarTitle_tv.setText(R.string.medicine_search_en);
        }else{

            toolbarTitle_tv.setText(R.string.medicine_search_ar);
        }
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
        loadMoreProgressBar_pb.setVisibility(View.GONE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
    }

    //method for parsing category list data
    void parseMedicineList(String response){

        JSONObject jObj;
        try {
            jObj = new JSONObject(response);


            if(jObj.has("total_records")){

                totalRecords = jObj.getInt("total_records");
            }

            /*news and events array variables*/
            if(jObj.has("medicines")){

                /* news & events list array variables */
                String medicineListArray, id_ = null, clinicalservice_id_= null, added_date_= null, name_= null, price_= null, desc_= null;
                JSONObject jObjNewsEventsList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObj.getJSONArray("medicines");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        medicineListArray = tempCatListArray.getString(i);
                        jObjNewsEventsList = new JSONObject(medicineListArray);

                        if (jObjNewsEventsList.has("id")) {

                            id_ = jObjNewsEventsList.getString("id");
                        }
                        if (jObjNewsEventsList.has("clinicalservice_id")) {

                            clinicalservice_id_ = jObjNewsEventsList.getString("clinicalservice_id");
                        }
                        if (jObjNewsEventsList.has("added_date")) {

                            added_date_ = jObjNewsEventsList.getString("added_date");
                        }
                        if (jObjNewsEventsList.has("name")) {

                            name_ = jObjNewsEventsList.getString("name");
                        }
                        if (jObjNewsEventsList.has("price")) {

                            price_ = jObjNewsEventsList.getString("price");
                        }
                        if (jObjNewsEventsList.has("desc")) {

                            desc_ = jObjNewsEventsList.getString("desc");
                        }
                        MedicineItems tempCatList = new MedicineItems(id_, clinicalservice_id_, added_date_, name_, price_, desc_);
                        medicineItemsArrayList_.add(tempCatList);
                    }
                }
            }

            loadMore_ll.setVisibility(View.GONE);
            medicineListAdapter.notifyDataSetChanged();
            showDataView();

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String medicineListObj(String language, String pageNumber, String clinicalservice_id, String search_word) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );
            mainObj.put( "page_number", pageNumber );
            mainObj.put( "clinicalservice_id", clinicalservice_id );
            mainObj.put( "search_word", search_word );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void medicineListApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(MedicineSearch.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseMedicineList(result);
                        loadingMore = true;

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
