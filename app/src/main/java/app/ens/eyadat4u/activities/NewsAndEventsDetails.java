package app.ens.eyadat4u.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;

public class NewsAndEventsDetails extends BaseActivity {

    Toolbar     toolbar;
    TextView    toolbarTitle_tv;
    TextView    desc_tv, day_tv, month_tv, year_tv;
    ImageView   itemImage_iv;
    UILHandler  uilHandler;
    String      title, description, image, day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_and_events_details);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        uilHandler      = (UILHandler) getApplication();

        title           = getIntent().getExtras().getString("ITEM_TITLE");
        description     = getIntent().getExtras().getString("ITEM_DESC");
        image           = getIntent().getExtras().getString("ITEM_IMAGE");
        day             = getIntent().getExtras().getString("ITEM_DAY");
        month           = getIntent().getExtras().getString("ITEM_MONTH");
        year            = getIntent().getExtras().getString("ITEM_YEAR");

        initViews();
        setToolbar();

        desc_tv.setText(description);
        day_tv.setText(day);
        month_tv.setText(month);
        year_tv.setText(year);

        uilHandler.loadImage(image, itemImage_iv , Constants.imageSpecifier.NORMAL);
    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        itemImage_iv        = (ImageView) findViewById(R.id.item_image);
        desc_tv             = (TextView) findViewById(R.id.description);
        day_tv              = (TextView) findViewById(R.id.day);
        month_tv            = (TextView) findViewById(R.id.month);
        year_tv             = (TextView) findViewById(R.id.year);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }
}
