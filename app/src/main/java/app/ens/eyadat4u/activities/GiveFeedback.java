package app.ens.eyadat4u.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class GiveFeedback extends BaseActivity {

    Toolbar             toolbar;
    TextView            toolbarTitle_tv, itemName_tv, giveFb_tv, giveRating_tv, ourService_tv,
                        ourFees_tv, ourLocation_tv, ourAppointment_tv, ourReception_tv, overAllRating_tv, submitFb_tv;
    ImageView           itemImage_iv;
    String              itemName_;
    String              itemImage_;
    String              serviceId_;
    UILHandler          uilHandler;
    EditText            title_et, message_et;
    RatingBar           ratingBar_service_rb, ratingBar_fees_rb, ratingBar_location_rb,
                        ratingBar_appoint_rb, ratingBar_recept_rb, ratingBar_overall_rb;

    LinearLayout        parentLayout_ll, submitFeedback_ll;
    ProgressDialog      progressdialog;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;
    float               serviceRating, feesRating, locationRating, appointRating, receptRating, overallRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_give_feedback);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue       = Volley.newRequestQueue(GiveFeedback.this);
        uilHandler  = (UILHandler) getApplication();
        itemName_   = getIntent().getExtras().getString("ITEM_NAME");
        itemImage_  = getIntent().getExtras().getString("ITEM_IMAGE");
        serviceId_  = getIntent().getExtras().getString("ITEM_ID");

        initViews();
        setToolbar();

        itemName_tv.setText(itemName_);
        uilHandler.loadImage(itemImage_, itemImage_iv , Constants.imageSpecifier.ROUND_EDGE_LARGE);

        submitFeedback_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                        serviceRating   =  ratingBar_service_rb.getRating();
                        feesRating      =  ratingBar_fees_rb.getRating();
                        locationRating  =  ratingBar_location_rb.getRating();
                        appointRating   =  ratingBar_appoint_rb.getRating();
                        receptRating    =  ratingBar_recept_rb.getRating();
                        overallRating   =  ratingBar_overall_rb.getRating();

                if((title_et.getText().toString() != null && title_et.getText().toString().equals(""))||
                        (message_et.getText().toString() != null && message_et.getText().toString().equals(""))) {

                    showSnackBar("Please fill all the fields");
                }
                else{
                    progressdialog = new ProgressDialog(GiveFeedback.this);
                    progressdialog.setMessage("Creating please wait...");
                    progressdialog.setCancelable(false);
                    progressdialog.show();

                    String obj = feedbackObj(title_et.getText().toString(), message_et.getText().toString(), serviceRating,
                            feesRating, locationRating, appointRating, receptRating, overallRating);
                    feedbackApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_GIVE_FEEDBACK), obj);
                }

            }
        });

    }

    private void initViews() {

        toolbar                     = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv             = (TextView) findViewById(R.id.toolbar_title);
        itemName_tv                 = (TextView) findViewById(R.id.item_name);
        itemImage_iv                = (ImageView) findViewById(R.id.item_image);
        title_et                    = (EditText) findViewById(R.id.title);
        message_et                  = (EditText) findViewById(R.id.message);
        ratingBar_service_rb        = (RatingBar) findViewById(R.id.rating_bar_1);
        ratingBar_fees_rb           = (RatingBar) findViewById(R.id.rating_bar_2);
        ratingBar_location_rb       = (RatingBar) findViewById(R.id.rating_bar_3);
        ratingBar_appoint_rb        = (RatingBar) findViewById(R.id.rating_bar_4);
        ratingBar_recept_rb         = (RatingBar) findViewById(R.id.rating_bar_5);
        ratingBar_overall_rb        = (RatingBar) findViewById(R.id.rating_bar_6);
        submitFeedback_ll           = (LinearLayout) findViewById(R.id.submit_feedback);
        parentLayout_ll             = (LinearLayout) findViewById(R.id.parent_layout);
        giveFb_tv                   = (TextView) findViewById(R.id.give_fb_text);
        giveRating_tv               = (TextView) findViewById(R.id.give_rating_text);
        ourService_tv               = (TextView) findViewById(R.id.how_was_service_text);
        ourFees_tv                  = (TextView) findViewById(R.id.fees_text);
        ourLocation_tv              = (TextView) findViewById(R.id.location_text);
        ourAppointment_tv           = (TextView) findViewById(R.id.how_was_appointment_text);
        ourReception_tv             = (TextView) findViewById(R.id.how_was_reception_text);
        overAllRating_tv            = (TextView) findViewById(R.id.overall_rating_text);
        submitFb_tv                 = (TextView) findViewById(R.id.submit_fb_text);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(GiveFeedback.this).equals("en")){

            toolbarTitle_tv.setText(R.string.give_feedback_en);
            title_et.setHint(R.string.title_en);
            message_et.setHint(R.string.message_en);
            giveFb_tv.setText(R.string.give_your_feedback_en);
            giveRating_tv.setText(R.string.give_your_rating_en);
            ourService_tv.setText(R.string.how_was_service_en);
            ourFees_tv.setText(R.string.fees_text_en);
            ourLocation_tv.setText(R.string.location_text_en);
            ourAppointment_tv.setText(R.string.how_was_appointment_en);
            ourReception_tv.setText(R.string.how_was_reception_en);
            overAllRating_tv.setText(R.string.overall_rating_en);
            submitFb_tv.setText(R.string.submit_fb_en);
        }else{

            toolbarTitle_tv.setText(R.string.give_feedback_ar);
            title_et.setHint(R.string.title_ar);
            message_et.setHint(R.string.message_ar);
            giveFb_tv.setText(R.string.give_your_feedback_ar);
            giveRating_tv.setText(R.string.give_your_rating_ar);
            ourService_tv.setText(R.string.how_was_service_ar);
            ourFees_tv.setText(R.string.fees_text_ar);
            ourLocation_tv.setText(R.string.location_text_ar);
            ourAppointment_tv.setText(R.string.how_was_appointment_ar);
            ourReception_tv.setText(R.string.how_was_reception_ar);
            overAllRating_tv.setText(R.string.overall_rating_ar);
            submitFb_tv.setText(R.string.submit_fb_ar);
        }
    }

    private String feedbackObj(String title, String message, float serviceRating, float feesRating, float locationRating, float appointRating,
                               float receptRating, float overallRating) {

        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put( "user_id", UserManager.getUserId(GiveFeedback.this));
            mainObj.put( "api_key", UserManager.getUserApiKey(GiveFeedback.this));
            mainObj.put( "title", title );
            mainObj.put( "message", message );
            mainObj.put( "language", UserManager.getUserLanguage(GiveFeedback.this) );
            mainObj.put( "clinicalservice_id", serviceId_ );

            mainObj.put( "service_rating", serviceRating );
            mainObj.put( "fee_rating", feesRating );
            mainObj.put( "location_rating", locationRating );
            mainObj.put( "appoint_rating", appointRating );
            mainObj.put( "recept_rating", receptRating );
            mainObj.put( "overall_rating", overallRating );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void feedbackApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(GiveFeedback.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        progressdialog.dismiss();
                        String result = response.toString();

                        JSONObject jObj;
                        jObj = new JSONObject(result);
                        if(jObj.has("message")){

                            showSnackBar(jObj.getString("message"));
                        }

                        if(jObj.has("status")){

                            if(jObj.getBoolean("status")){

                                // Send feedback submitted broadcast message
                                Intent registrationComplete = new Intent(Constants.Keys.SUBMITTED_FEEDBACK);
                                LocalBroadcastManager.getInstance(GiveFeedback.this).sendBroadcast(registrationComplete);

                                Toast.makeText(GiveFeedback.this, "Feedback submitted successfully !", Toast.LENGTH_LONG).show();

                                finish();
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                            }
                        }


                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            showAlertDialogue("Exit ?","Are sure want to exit without giving feedback");
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }

    /* alert dialogue  */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(GiveFeedback.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
                flushAllNetworkConnections();
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        })
                .create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            showAlertDialogue("Exit ?","Are sure want to exit without giving feedback");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
