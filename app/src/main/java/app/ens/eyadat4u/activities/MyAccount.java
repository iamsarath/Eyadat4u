package app.ens.eyadat4u.activities;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.DefaultTabsAdapter;
import app.ens.eyadat4u.adapters.MyAppointmentsAdapter;
import app.ens.eyadat4u.fragments.MyAppointmentsFragment;
import app.ens.eyadat4u.fragments.MyProfileFragment;
import app.ens.eyadat4u.items.MyAppointmentItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class MyAccount extends BaseActivity {

    Toolbar                 toolbar;
    TextView                toolbarTitle_tv;
    TabLayout               tabLayout_tl;
    ViewPager               viewPager_vp;
    DefaultTabsAdapter      myAccountsAdapter;
    MyAppointmentsFragment  myAppointmentsFragment;
    MyProfileFragment       myProfileFragment;

    public static LinearLayout  parentLayout_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        myAccountsAdapter       = new DefaultTabsAdapter(getSupportFragmentManager());
        initViews();
        setToolbar();

        setupViewPager(viewPager_vp);
        tabLayout_tl.post(new Runnable() {
            @Override
            public void run() {

                tabLayout_tl.setupWithViewPager(viewPager_vp);
            }
        });

    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        parentLayout_ll     = (LinearLayout) findViewById(R.id.parent_layout);
        tabLayout_tl        = (TabLayout) findViewById(R.id.tabs);
        viewPager_vp        = (ViewPager) findViewById(R.id.my_accounts_view_pager);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        if(UserManager.getUserLanguage(MyAccount.this).equals("en")){

            toolbarTitle_tv.setText(R.string.my_appointments_en);
        }
        else {

            toolbarTitle_tv.setText(R.string.my_appointments_ar);
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        myAppointmentsFragment  = new MyAppointmentsFragment();
        myProfileFragment       = new MyProfileFragment();

        if(UserManager.getUserLanguage(MyAccount.this).equals("en")){

            myAccountsAdapter.addFragment(myAppointmentsFragment, getResources().getString(R.string.my_appointments_en));
            myAccountsAdapter.addFragment(myProfileFragment, getResources().getString(R.string.my_profile_en));
        }
        else {

            myAccountsAdapter.addFragment(myAppointmentsFragment, getResources().getString(R.string.my_appointments_ar));
            myAccountsAdapter.addFragment(myProfileFragment, getResources().getString(R.string.my_profile_ar));
        }

        viewPager.setAdapter(myAccountsAdapter);
        viewPager.setCurrentItem(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    public static void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
