package app.ens.eyadat4u.activities;

import android.graphics.Typeface;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.SearchAdapter;
import app.ens.eyadat4u.items.CommonSearchItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.Fonts;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class Search extends BaseActivity {

    Toolbar             toolbar;
    TextView            toolbarTitle_tv;
    SearchAdapter       searchAdapter;
    SearchView          searchView_sv;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;
    LinearLayout        parentLayout_ll, progressLayout_ll, dataLayout_ll;
    ListView            searchList_lv;
    ProgressBar         progressBar_pb;
    ArrayList<CommonSearchItems> searchItemsListArrayList_  = new ArrayList<CommonSearchItems>();

    boolean canSearch = true;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue         = Volley.newRequestQueue(Search.this);
        searchAdapter = new SearchAdapter(Search.this, searchItemsListArrayList_);

        initViews();
        setToolbar();

        searchList_lv.setAdapter(searchAdapter);
//        searchView_sv.setQueryHint("Specialities, Doctors, Clinics, Labs & more...");
        searchView_sv.requestFocus();
        showDataView();

        int id = searchView_sv.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView_sv.findViewById(id);
        Typeface type = Typeface.createFromAsset(getAssets(), Fonts.FONT_REGULAR);
        textView.setTypeface(type);

        searchView_sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {

//                if(TextUtils.isEmpty(newText)){
//
//                }
//                else{

                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {

                            if(TextUtils.isEmpty(newText)){

                            }
                            else{

                                if(canSearch){
                                    canSearch= false;
                                    showProgressView();
                                    connectToServer( UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_COMMON_SEARCH)+
                                            UserManager.getUserLanguage(Search.this)+"/"+newText);
                                }
                            }
                        }
                    };
                    handler.removeCallbacks(runnable);
                    handler.postDelayed(runnable, 1000);

//                }
                return false;
            }
        });
    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        searchView_sv           = (SearchView) findViewById(R.id.searchView);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        progressLayout_ll       = (LinearLayout) findViewById(R.id.progress_view);
        dataLayout_ll           = (LinearLayout) findViewById(R.id.data_view);
        searchList_lv           = (ListView) findViewById(R.id.search_list);
        progressBar_pb          = (ProgressBar) findViewById(R.id.progressBar);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(Search.this).equals("en")){

            toolbarTitle_tv.setText(R.string.search_en);
            searchView_sv.setQueryHint("Specialties, Doctors, Clinics, Hospitals, Labs, Spas and Fitness Centers, etc.");
        }else{

            toolbarTitle_tv.setText(R.string.search_ar);
            searchView_sv.setQueryHint("إبحث عن الأطباء ، العيادات ، المستشفيات ، المعامل الطبية ، مراكز العلاج الطبيعى ، مراكز اللياقة ، والصيدليات");
        }
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
    }

    public void connectToServer(String url_){

        if(!CheckNetwork.isInternetAvailable(Search.this))
        {
            try {

                showSnackBar("Bad Network, please check your internet connection");

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        else {

                jsonObjectRequest = new JsonObjectRequest(url_,null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String result =response.toString();
                                    parseSearchList(result);
                                    canSearch= true;

                                } catch (Exception b) {
                                    b.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse == null) {
                            if (error.getClass().equals(TimeoutError.class)) {
                                // Show timeout error message
                                showSnackBar("Can't reach our server right now, please retry");
                            }
                        }
                        error.printStackTrace();
                    }
                }) ;

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                        150000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                queue.add(jsonObjectRequest);
            }
        }

    //method for parsing likes list data
    void parseSearchList(String response){

        JSONObject jObj;
        searchItemsListArrayList_.clear();
        try {

            jObj = new JSONObject(response);

            /*user data array variables*/
            if(jObj.has("result")){

                /* extras list array variables */
                String searchListArray, service_id_ = null, service_name_ = null, service_type_ = null, custom_search_ = "", type_ = "";
                JSONObject jObjNewSearchList= null;
                JSONArray tempSearchListArray = null;

                tempSearchListArray = jObj.getJSONArray("result");
                if (tempSearchListArray.length() != 0) {
                    for (int i = 0; i < tempSearchListArray.length(); i++) {

                        searchListArray = tempSearchListArray.getString(i);
                        jObjNewSearchList = new JSONObject(searchListArray);

                        if (jObjNewSearchList.has("service_id")) {

                            service_id_ = jObjNewSearchList.getString("service_id");
                        }
                        if (jObjNewSearchList.has("sname")) {

                            service_name_ = jObjNewSearchList.getString("sname");
                        }
                        if (jObjNewSearchList.has("stype")) {

                            service_type_ = jObjNewSearchList.getString("stype");
                        }
                        if (jObjNewSearchList.has("custom_search")) {

                            custom_search_ = jObjNewSearchList.getString("custom_search");
                        }
                        if (jObjNewSearchList.has("service_type")) {

                            type_ = jObjNewSearchList.getString("service_type");
                        }

                        CommonSearchItems tempSearchList = new CommonSearchItems(service_id_, service_name_, service_type_, custom_search_, type_);
                        searchItemsListArrayList_.add(tempSearchList);
                    }
                }

            }
                    showDataView();
                    searchAdapter = new SearchAdapter(Search.this, searchItemsListArrayList_);
                    searchList_lv.setAdapter(searchAdapter);
                    progressLayout_ll.setVisibility(View.GONE);
                    canSearch= true;

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showSnackBar(String message){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, message, Snackbar.LENGTH_INDEFINITE)
                .setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }
}
