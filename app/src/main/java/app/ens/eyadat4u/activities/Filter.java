package app.ens.eyadat4u.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.FilterFeesAdapter;
import app.ens.eyadat4u.adapters.FilterTimeAdapter;
import app.ens.eyadat4u.items.FilterDayCheckItems;
import app.ens.eyadat4u.items.FilterFeesCheckItems;
import app.ens.eyadat4u.items.FilterTimeCheckItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.views.HorizontalListView;

public class Filter extends BaseActivity {

    Toolbar             toolbar;
    TextView            toolbarTitle_tv, clearText_tv;
    TextView            monText_tv, tueText_tv, wedText_tv, thuText_tv, friText_tv, satText_tv, sunText_tv, filterDayText_tv, filterTimeText_tv,
                        flterFeesText_tv, submitText_tv;
    LinearLayout        monLayout_ll, tueLayout_ll, wedLayout_ll, thuLayout_ll, friLayout_ll, satLayout_ll, sunLayout_ll, applyFilter_ll;
    GridView            timeGrid_gv, feesGrid_gv;
    ProgressDialog progressdialog;
    private static int CLEARING_TIME_OUT = 1250;

    public static LinearLayout          clearFilter_ll;
    public static FilterTimeAdapter     filterTimeAdapter;
    public static FilterFeesAdapter     filterFeesAdapter;

    String [] daysArray;
    String serviceId;
    static boolean is_day_filter_selected   = false;
    static boolean is_time_filter_selected  = false;
    static boolean is_fees_filter_selected  = false;

    String selectedDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        overridePendingTransition(R.animator.slide_up, R.animator.no_change);

        serviceId        = getIntent().getExtras().getString("SERVICE_ID");

//        filterDaysAdapter = new FilterDaysAdapter(Filter.this);
        filterTimeAdapter = new FilterTimeAdapter(Filter.this);
        filterFeesAdapter = new FilterFeesAdapter(Filter.this);

        daysArray               = new String[7];
        daysArray [0]           = "Mon";
        daysArray [1]           = "Tue";
        daysArray [2]           = "Wed";
        daysArray [3]           = "Thu";
        daysArray [4]           = "Fri";
        daysArray [5]           = "Sat";
        daysArray [6]           = "Sun";

        initViews();
        setToolbar();

        initDaysTextColors();
        validateFilterSelection();

        timeGrid_gv.setAdapter(filterTimeAdapter);
        feesGrid_gv.setAdapter(filterFeesAdapter);

        applyFilter_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedDays = "";
                Intent returnIntent = new Intent();
                returnIntent.putExtra("FILTER_RESULT", createFilterParamsObj(UserManager.getUserLanguage(Filter.this),  "0",
                        serviceId, getSelectedDays(),  getFromTime(getSelectedTime()), getToTime(getSelectedTime()),
                        getFromFees(getSelectedFees()),  getToFees(getSelectedFees()), isFeeSelected(getSelectedFees()),
                        false,  false));
                returnIntent.putExtra("FILTER_FROM_TIME",  getFromTime(getSelectedTime()));
                returnIntent.putExtra("FILTER_TO_TIME", getToTime(getSelectedTime()));
                returnIntent.putExtra("FILTER_FROM_FEES", getFromFees(getSelectedFees()));
                returnIntent.putExtra("FILTER_TO_FEES", getToFees(getSelectedFees()));
                returnIntent.putExtra("FILTER_DAYS",  getSelectedDays());
                returnIntent.putExtra("IS_FEE_SELECTED", isFeeSelected(getSelectedFees()));

                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

        monLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(0).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[0], false);
                    Constants.filterDayCheckItemsArrayList.set(0,tempCheckList);

                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[0], true);
                    Constants.filterDayCheckItemsArrayList.set(0,tempCheckList);

                }

                if(Constants.filterDayCheckItemsArrayList.get(0).is_checked_){

                    monText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    monText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }

                validateFilterSelection();
            }
        });

        tueLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(1).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[1], false);
                    Constants.filterDayCheckItemsArrayList.set(1,tempCheckList);
                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[1], true);
                    Constants.filterDayCheckItemsArrayList.set(1,tempCheckList);
                }

                if(Constants.filterDayCheckItemsArrayList.get(1).is_checked_){

                    tueText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    tueText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }

                validateFilterSelection();
            }
        });

        wedLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(2).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[2], false);
                    Constants.filterDayCheckItemsArrayList.set(2,tempCheckList);
                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[2], true);
                    Constants.filterDayCheckItemsArrayList.set(2,tempCheckList);
                }

                if(Constants.filterDayCheckItemsArrayList.get(2).is_checked_){

                    wedText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    wedText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }

                validateFilterSelection();
            }
        });

        thuLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(3).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[3], false);
                    Constants.filterDayCheckItemsArrayList.set(3,tempCheckList);
                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[3], true);
                    Constants.filterDayCheckItemsArrayList.set(3,tempCheckList);
                }

                if(Constants.filterDayCheckItemsArrayList.get(3).is_checked_){

                    thuText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    thuText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }

                validateFilterSelection();
            }
        });

        friLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(4).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[4], false);
                    Constants.filterDayCheckItemsArrayList.set(4,tempCheckList);
                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[4], true);
                    Constants.filterDayCheckItemsArrayList.set(4,tempCheckList);
                }

                if(Constants.filterDayCheckItemsArrayList.get(4).is_checked_){

                    friText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    friText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }
                validateFilterSelection();
            }
        });

        satLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(5).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[5], false);
                    Constants.filterDayCheckItemsArrayList.set(5,tempCheckList);
                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[5], true);
                    Constants.filterDayCheckItemsArrayList.set(5,tempCheckList);
                }

                if(Constants.filterDayCheckItemsArrayList.get(5).is_checked_){

                    satText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    satText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }
                validateFilterSelection();
            }
        });

        sunLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.filterDayCheckItemsArrayList.get(6).is_checked_){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[6], false);
                    Constants.filterDayCheckItemsArrayList.set(6,tempCheckList);
                }
                else {

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[6], true);
                    Constants.filterDayCheckItemsArrayList.set(6,tempCheckList);
                }

                if(Constants.filterDayCheckItemsArrayList.get(6).is_checked_){

                    sunText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                }
                else {

                    sunText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                }
                validateFilterSelection();
            }
        });

        clearFilter_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressdialog = new ProgressDialog(Filter.this);
                progressdialog.setMessage("Resetting filter please wait...");
                progressdialog.setCancelable(false);
                progressdialog.show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                       progressdialog.dismiss();
                       clearFilter_ll.setVisibility(View.INVISIBLE);
                    }
                }, CLEARING_TIME_OUT);

//                Constants.is_filter_selected = false;

                monText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                tueText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                wedText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                thuText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                friText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                satText_tv.setTextColor(getResources().getColor(R.color.TextMain));
                sunText_tv.setTextColor(getResources().getColor(R.color.TextMain));

//                daysArray               = new String[7];
//                daysArray [0]           = "Mon";
//                daysArray [1]           = "Tue";
//                daysArray [2]           = "Wed";
//                daysArray [3]           = "Thu";
//                daysArray [4]           = "Fri";
//                daysArray [5]           = "Sat";
//                daysArray [6]           = "Sun";

                Constants.filterDayCheckItemsArrayList.clear();

                for (int i=0; i< 7; i++){

                    FilterDayCheckItems tempCheckList = new FilterDayCheckItems(daysArray[i], false);
                    Constants.filterDayCheckItemsArrayList.add(tempCheckList);
                }

                setTimeArrayList();
                setFeesArrayList();

                filterTimeAdapter = new FilterTimeAdapter(Filter.this);
                filterFeesAdapter = new FilterFeesAdapter(Filter.this);
                timeGrid_gv.setAdapter(filterTimeAdapter);
                feesGrid_gv.setAdapter(filterFeesAdapter);

            }
        });
    }

    void initDaysTextColors(){

        if(Constants.filterDayCheckItemsArrayList.get(0).is_checked_){

            monText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            monText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

        if(Constants.filterDayCheckItemsArrayList.get(1).is_checked_){

            tueText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            tueText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

        if(Constants.filterDayCheckItemsArrayList.get(2).is_checked_){

            wedText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            wedText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

        if(Constants.filterDayCheckItemsArrayList.get(3).is_checked_){

            thuText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            thuText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

        if(Constants.filterDayCheckItemsArrayList.get(4).is_checked_){

            friText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            friText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

        if(Constants.filterDayCheckItemsArrayList.get(5).is_checked_){

            satText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            satText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

        if(Constants.filterDayCheckItemsArrayList.get(6).is_checked_){

            sunText_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
        else {

            sunText_tv.setTextColor(getResources().getColor(R.color.TextMain));
        }

    }

    void setTimeArrayList() {
        String [] timeArray;
        timeArray = new String[4];
        timeArray [0] = "Before 12:00 PM";
        timeArray [1] = "12:00 PM To 4:00 PM";
        timeArray [2] = "4:00 PM To 8:00 PM";
        timeArray [3] = "After 8:00 PM";
        Constants.filterTimeCheckItemsArrayList.clear();

        for (int i=0; i< 4; i++){

            FilterTimeCheckItems tempCheckList = new FilterTimeCheckItems(timeArray[i], false);
            Constants.filterTimeCheckItemsArrayList.add(tempCheckList);
        }
    }

    void setFeesArrayList() {
        String [] feesArray;
        feesArray = new String[4];
        feesArray [0] = "Below 50KD";
        feesArray [1] = "B/W 50KD & 100KD";
        feesArray [2] = "B/W 100KD & 500KD";
        feesArray [3] = "Above 500KD";
        Constants.filterFeesCheckItemsArrayList.clear();

        for (int i=0; i< 4; i++){

            FilterFeesCheckItems tempCheckList = new FilterFeesCheckItems(feesArray[i], false);
            Constants.filterFeesCheckItemsArrayList.add(tempCheckList);
        }
    }


    String getSelectedDays(){

        StringBuilder sb = new StringBuilder();

        String prefix = "";
        for (int i=0; i< 7; i++){

            if(Constants.filterDayCheckItemsArrayList.get(i).is_checked_){

                sb.append(prefix);
                prefix = ",";
                sb.append(Constants.filterDayCheckItemsArrayList.get(i).day_);
            }
        }

        return sb.toString();
    }

    String getSelectedTime(){

        String time = "";
        for (int i=0; i< 4; i++){

            if(Constants.filterTimeCheckItemsArrayList.get(i).is_checked_){

                time = Constants.filterTimeCheckItemsArrayList.get(i).time_;
                break;
            }
        }

        return time;
    }

    boolean isFeeSelected(String value){

            if(value.equals("")){

                return false;
            }
        else{
                return true;
            }
    }

    String getSelectedFees(){

        String fees = "";
        for (int i=0; i< 4; i++){

            if(Constants.filterFeesCheckItemsArrayList.get(i).is_checked_){

                fees = Constants.filterFeesCheckItemsArrayList.get(i).fees_;
                break;
            }
        }
        return fees;
    }

    public static void validateFilterSelection(){

        for (int i=0; i< 7; i++){

            if(Constants.filterDayCheckItemsArrayList.get(i).is_checked_){

                is_day_filter_selected = true;
                break;
            }
            else{

                is_day_filter_selected = false;
            }
        }

        //Day validation
        if(is_day_filter_selected){

            clearFilter_ll.setVisibility(View.VISIBLE);
        }
        else{
            //Time Filter
            for (int i=0; i< 4; i++){

                if(Constants.filterTimeCheckItemsArrayList.get(i).is_checked_){

                    is_time_filter_selected = true;
                    break;
                }
                else{

                    is_time_filter_selected = false;
                }
            }

            //Time Validation
            if (is_time_filter_selected){

                clearFilter_ll.setVisibility(View.VISIBLE);
            }
            else{

//                clearFilter_ll.setVisibility(View.INVISIBLE);

                //Fees Filter
                for (int i=0; i< 4; i++){

                    if(Constants.filterFeesCheckItemsArrayList.get(i).is_checked_){

                        is_fees_filter_selected = true;
                        break;
                    }
                    else{

                        is_fees_filter_selected = false;
                    }
                }

                //Fees Validation
                if (is_fees_filter_selected){

                    clearFilter_ll.setVisibility(View.VISIBLE);
                }
                else {

                    clearFilter_ll.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    private String createFilterParamsObj(String language, String pageNumber, String serviceId,
                   String filter_by_day, String time_aval_from, String time_aval_to, String consult_fee_from, String consult_fee_to,  boolean is_fee_selected,
                   boolean sort_by_top_rated, boolean sort_by_availability) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );
            mainObj.put( "page_number", pageNumber );
            mainObj.put( "service_id", serviceId );
            mainObj.put( "is_fee_selected", is_fee_selected);
            mainObj.put( "search_days", filter_by_day );
            mainObj.put( "time_aval_from", time_aval_from );
            mainObj.put( "time_aval_to", time_aval_to );
            mainObj.put( "consult_fee_from", consult_fee_from );
            mainObj.put( "consult_fee_to", consult_fee_to );
            mainObj.put( "sort_by_top_rated", sort_by_top_rated );
            mainObj.put( "sort_by_availability", sort_by_availability );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        clearFilter_ll          = (LinearLayout) findViewById(R.id.clear_filter);
        timeGrid_gv             = (GridView) findViewById(R.id.time_grid);
        feesGrid_gv             = (GridView) findViewById(R.id.fees_grid);
        applyFilter_ll          = (LinearLayout) findViewById(R.id.apply_filter);

        monLayout_ll            = (LinearLayout) findViewById(R.id.mon_layout);
        tueLayout_ll            = (LinearLayout) findViewById(R.id.tue_layout);
        wedLayout_ll            = (LinearLayout) findViewById(R.id.wed_layout);
        thuLayout_ll            = (LinearLayout) findViewById(R.id.thu_layout);
        friLayout_ll            = (LinearLayout) findViewById(R.id.fri_layout);
        satLayout_ll            = (LinearLayout) findViewById(R.id.sat_layout);
        sunLayout_ll            = (LinearLayout) findViewById(R.id.sun_layout);

        monText_tv              = (TextView) findViewById(R.id.mon_text);
        tueText_tv              = (TextView) findViewById(R.id.tue_text);
        wedText_tv              = (TextView) findViewById(R.id.wed_text);
        thuText_tv              = (TextView) findViewById(R.id.thu_text);
        friText_tv              = (TextView) findViewById(R.id.fri_text);
        satText_tv              = (TextView) findViewById(R.id.sat_text);
        sunText_tv              = (TextView) findViewById(R.id.sun_text);

        filterDayText_tv        = (TextView) findViewById(R.id.filter_day_text);
        filterTimeText_tv       = (TextView) findViewById(R.id.filter_time_text);
        flterFeesText_tv        = (TextView) findViewById(R.id.filter_fees_text);
        submitText_tv           = (TextView) findViewById(R.id.apply_filter_text);
        clearText_tv            = (TextView) findViewById(R.id.clear_text);

        clearFilter_ll.setVisibility(View.INVISIBLE);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(Filter.this).equals("en")){

            filterDayText_tv.setText(R.string.filter_day_en);
            filterTimeText_tv.setText(R.string.filter_by_time_en);
            flterFeesText_tv.setText(R.string.filter_by_fees_en);
            submitText_tv.setText(R.string.apply_filter_en);
            toolbarTitle_tv.setText(R.string.filter_en);
            clearText_tv.setText(R.string.clear_en);

        }else{

            filterDayText_tv.setText(R.string.filter_day_ar);
            filterTimeText_tv.setText(R.string.filter_by_time_ar);
            flterFeesText_tv.setText(R.string.filter_by_fees_ar);
            submitText_tv.setText(R.string.apply_filter_ar);
            toolbarTitle_tv.setText(R.string.filter_ar);
            clearText_tv.setText(R.string.clear_ar);
        }
    }

    public static String getFromTime(String token){

        String fromTime      = "";
        switch (token) {

            case "Before 12:00 PM" :
                fromTime = "1";
                break;

            case "12:00 PM To 4:00 PM" :
                fromTime = "12";
                break;

            case "4:00 PM To 8:00 PM" :
                fromTime = "16";
                break;

            case "After 8:00 PM" :
                fromTime = "20";
                break;

        }
        return fromTime;
    }

    public static String getToTime(String token){

        String toTime      = "";
        switch (token) {

            case "Before 12:00 PM" :
                toTime = "12";
                break;

            case "12:00 PM To 4:00 PM" :
                toTime = "16";
                break;

            case "4:00 PM To 8:00 PM" :
                toTime = "20";
                break;

            case "After 8:00 PM" :
                toTime = "24";
                break;

        }
        return toTime;
    }

    public static String getFromFees(String token){

        String fromFees      = "";
        switch (token) {

            case "Below 50KD" :
                fromFees = "0";
                break;

            case "B/W 50KD & 100KD" :
                fromFees = "50";
                break;

            case "B/W 100KD & 500KD" :
                fromFees = "100";
                break;

            case "Above 500KD" :
                fromFees = "500";
                break;
        }
        return fromFees;
    }

    public static String getToFees(String token){

        String toFees      = "";
        switch (token) {

            case "Below 50KD" :
                toFees = "50";
                break;

            case "B/W 50KD & 100KD" :
                toFees = "100";
                break;

            case "B/W 100KD & 500KD" :
                toFees = "500";
                break;

            case "Above 500KD" :
                toFees = "2000";
                break;

        }
        return toFees;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            finish();
            overridePendingTransition(R.animator.no_change, R.animator.slide_down);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            finish();
            overridePendingTransition(R.animator.no_change, R.animator.slide_down);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
