package app.ens.eyadat4u.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.InsuranceAdapter;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.Constants;

public class InsuranceDetails extends BaseActivity {

    Toolbar                     toolbar;
    TextView                    toolbarTitle_tv, description_tv, companyName_tv, eligibility_tv, benefits_tv, renewal_tv,
                                exclusions_tv, pricing_tv, about_text_tv, eligibilityText_tv, benefitsText_tv, renewalPolicyText_tv, exclusionText_tv;
    LinearLayout                expEligibilityList_ll, expBenefits_ll, expRenewal_ll, expExclusion_ll;
    ExpandableRelativeLayout    eligibility_erl, benefits_erl, renewelPolicy_erl, exclusions_erl;
    ImageView                   itemImage_iv;
    UILHandler                  uilHandler;
    String                      title, description, companyName, pricing, image, benefits, eligibility, renewalPolicy, exclusion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance_details);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        uilHandler      = (UILHandler) getApplication();

        title           = getIntent().getExtras().getString("ITEM_TITLE");
        description     = getIntent().getExtras().getString("ITEM_DESC");
        companyName     = getIntent().getExtras().getString("COMPANY_NAME");
        pricing         = getIntent().getExtras().getString("PRICING");
        image           = getIntent().getExtras().getString("ITEM_IMAGE");
        benefits        = getIntent().getExtras().getString("ITEM_BENEFITS");
        eligibility     = getIntent().getExtras().getString("ITEM_ELIGIBILITY");
        renewalPolicy   = getIntent().getExtras().getString("ITEM_RENEWAL_POLICY");
        exclusion       = getIntent().getExtras().getString("ITEM_EXCLUSION");

        initViews();
        setToolbar();

        description_tv.setText(description);
        eligibility_tv.setText(eligibility);
        benefits_tv.setText(benefits);
        renewal_tv.setText(renewalPolicy);
        exclusions_tv.setText(exclusion);
        companyName_tv.setText(companyName);
        pricing_tv.setText(pricing+" KD");

        uilHandler.loadImage(image, itemImage_iv , Constants.imageSpecifier.NORMAL);

        expEligibilityList_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eligibility_erl.toggle();
            }
        });

        expBenefits_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                benefits_erl.toggle();
            }
        });

        expRenewal_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                renewelPolicy_erl.toggle();
            }
        });

        expExclusion_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exclusions_erl.toggle();
            }
        });
    }

    private void initViews() {

        toolbar                     = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv             = (TextView) findViewById(R.id.toolbar_title);
        description_tv              = (TextView) findViewById(R.id.description);
        companyName_tv              = (TextView) findViewById(R.id.company_name);
        pricing_tv                  = (TextView) findViewById(R.id.pricing);
        itemImage_iv                = (ImageView) findViewById(R.id.item_image);
        expEligibilityList_ll       = (LinearLayout) findViewById(R.id.button_layout_eligibility);
        expBenefits_ll              = (LinearLayout) findViewById(R.id.button_layout_benefits);
        expRenewal_ll               = (LinearLayout) findViewById(R.id.button_layout_renewal_policies);
        expExclusion_ll             = (LinearLayout) findViewById(R.id.button_layout_exclusions);
        eligibility_erl             = (ExpandableRelativeLayout) findViewById(R.id.exp_eligibility_layout);
        benefits_erl                = (ExpandableRelativeLayout) findViewById(R.id.exp_benefits_layout);
        renewelPolicy_erl           = (ExpandableRelativeLayout) findViewById(R.id.exp_renewal_policy_layout);
        exclusions_erl              = (ExpandableRelativeLayout) findViewById(R.id.exp_exclusion_layout);
        eligibility_tv              = (TextView) findViewById(R.id.eligibility);
        benefits_tv                 = (TextView) findViewById(R.id.benefits);
        renewal_tv                  = (TextView) findViewById(R.id.renewal_policy);
        exclusions_tv               = (TextView) findViewById(R.id.exclusions);
        about_text_tv               = (TextView) findViewById(R.id.about_text);
        eligibilityText_tv          = (TextView) findViewById(R.id.eligibility_text);
        benefitsText_tv             = (TextView) findViewById(R.id.benefits_text);
        renewalPolicyText_tv        = (TextView) findViewById(R.id.renewal_text);
        exclusionText_tv            = (TextView) findViewById(R.id.exclusion_text);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText(title);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(InsuranceDetails.this).equals("en")){

            about_text_tv.setText(R.string.about_en);
            eligibilityText_tv.setText(R.string.eligibility_text_en);
            benefitsText_tv.setText(R.string.benefits_text_en);
            renewalPolicyText_tv.setText(R.string.renewal_text_en);
            exclusionText_tv.setText(R.string.exclusion_text_en);
        }else{

            about_text_tv.setText(R.string.about_ar);
            eligibilityText_tv.setText(R.string.eligibility_text_ar);
            benefitsText_tv.setText(R.string.benefits_text_ar);
            renewalPolicyText_tv.setText(R.string.renewal_text_ar);
            exclusionText_tv.setText(R.string.exclusion_text_ar);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }
}
