package app.ens.eyadat4u.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.MedicalServiceCatItems;
import app.ens.eyadat4u.items.NewsAndEventsItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class MedicalServices extends BaseActivity {

    Toolbar                 toolbar;
    TextView                toolbarTitle_tv, medicalServicesText_tv, submitText_tv;
    LinearLayout            parentLayout_ll, progressLayout_ll, dataLayout_ll, submit_ll;
    ProgressBar             progressBar_pb;
    Spinner                 medicalCatSpinner_sp;
    EditText                qnty_et, name_et, email_et, phone_et, msg_et;
    RequestQueue            queue;
    JsonObjectRequest       jsonObjectRequest;
    ProgressDialog          progressdialog;
    String                  serviceId, countryCode;
    Spinner                 countryCode_sp;
    String []               countryCodeArray, codeTempArray;
    ArrayAdapter<String>    codeArrayAdapter;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    boolean is_spinner_selected    = false;
    int     spinnerSelection       = 0;

    boolean is_code_spinner_selected    = false;
    int     codeSpinnerSelection       = 0;

    ArrayList<MedicalServiceCatItems> medicalServiceCatArrayList   = new ArrayList<MedicalServiceCatItems>();
    ArrayAdapter<String> catArray;
    String[] categoryArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_services);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue     = Volley.newRequestQueue(MedicalServices.this);

        initViews();
        setToolbar();

        countryCodeArray = new String[6];
        countryCodeArray[0]= "+ 965 (KWT)";
        countryCodeArray[1]= "+ 971 (ARE)";
        countryCodeArray[2]= "+ 973 (BHR)";
        countryCodeArray[3]= "+ 968 (OMN)";
        countryCodeArray[4]= "+ 974 (QAT)";
        countryCodeArray[5]= "+ 966 (SAU)";

        codeTempArray= new String[6];
        codeTempArray[0]= "965";
        codeTempArray[1]= "971";
        codeTempArray[2]= "973";
        codeTempArray[3]= "968";
        codeTempArray[4]= "974";
        codeTempArray[5]= "966";

        codeArrayAdapter= new ArrayAdapter<String>(MedicalServices.this,android.R.layout.simple_spinner_item, countryCodeArray);
        codeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode_sp.setAdapter(codeArrayAdapter);

        countryCode =  codeTempArray[0];

        countryCode_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(is_code_spinner_selected){
//                    is_spinner_selected= false;
                    String selection = (String) parent.getItemAtPosition(position);
                    int pos = -1;

                    for (int i = 0; i < countryCodeArray.length; i++) {
                        if (countryCodeArray[i].equals(selection)) {

                            pos = i;
                            break;
                        }
                    }
                    codeSpinnerSelection = pos;
                    countryCode =  codeTempArray[codeSpinnerSelection];
                }
                else{
                    is_code_spinner_selected= true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        medicalCatSpinner_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(is_spinner_selected){
//                    is_spinner_selected= false;
                    String selection = (String) parent.getItemAtPosition(position);
                    int pos = -1;

                    for (int i = 0; i < categoryArray.length; i++) {
                        if (categoryArray[i].equals(selection)) {

                            pos = i;
                            break;
                        }
                    }
                    spinnerSelection = pos;
                    serviceId = medicalServiceCatArrayList.get(pos).cat_id_;
                }
                else{
                    is_spinner_selected= true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = email_et.getText().toString().trim();
//                if (email.matches(emailPattern))
//                {

                    if((qnty_et.getText().toString() != null && qnty_et.getText().toString().equals("")) || (name_et.getText().toString() != null && name_et.getText().toString().equals("")) ||
                            (email_et.getText().toString() != null && email_et.getText().toString().equals(""))|| (phone_et.getText().toString() != null && phone_et.getText().toString().equals(""))
                            || (msg_et.getText().toString() != null && msg_et.getText().toString().equals(""))) {

                        showSnackBar("Please fill all the fields !");
                    }
                    else{

                        if (email.matches(emailPattern))
                        {
                            progressdialog = new ProgressDialog(MedicalServices.this);
                            progressdialog.setMessage("Submitting please wait...");
                            progressdialog.setCancelable(false);
                            progressdialog.show();

                            String obj = medicalServiceRequestObj(UserManager.getUserLanguage(MedicalServices.this), serviceId, msg_et.getText().toString(),
                                    qnty_et.getText().toString(), name_et.getText().toString(), email_et.getText().toString(), phone_et.getText().toString(), countryCode);
                            submitMedicalServiceRequestApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICAL_SERVICE_REQUEST), obj);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Please enter a valid email address", Toast.LENGTH_SHORT).show();
                        }
                    }
            }
        });

        //calling promotions API
        showProgressView();
        String obj = medicalServiceCategoryObj(UserManager.getUserLanguage(MedicalServices.this));
        getMedicalServiceCatApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICAL_SERVICE_CATEGORY), obj);

    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        dataLayout_ll           = (LinearLayout) findViewById(R.id.data_view);
        progressLayout_ll       = (LinearLayout) findViewById(R.id.progress_view);
        progressBar_pb          = (ProgressBar) findViewById(R.id.progressBar);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        submit_ll               = (LinearLayout) findViewById(R.id.submit);
        medicalCatSpinner_sp    = (Spinner) findViewById(R.id.medical_service_spinner);
        qnty_et                 = (EditText) findViewById(R.id.qnty);
        name_et                 = (EditText) findViewById(R.id.name);
        email_et                = (EditText) findViewById(R.id.email);
        phone_et                = (EditText) findViewById(R.id.phone);
        msg_et                  = (EditText) findViewById(R.id.message);
        countryCode_sp          = (Spinner) findViewById(R.id.country_code_spinner);
        medicalServicesText_tv  = (TextView) findViewById(R.id.medical_services_text);
        submitText_tv           = (TextView) findViewById(R.id.submit_text);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(MedicalServices.this).equals("en")){

            toolbarTitle_tv.setText(R.string.menu_med_services_en);
            medicalServicesText_tv.setText(R.string.pickup_medical_services_text_en);
            submitText_tv.setText(R.string.submit_en);
            name_et.setHint(R.string.name_en);
            email_et.setHint(R.string.email_en);
            qnty_et.setHint(R.string.menu_med_services_en);
            phone_et.setHint(R.string.phone_en);
            msg_et.setHint(R.string.message_en);
        }else{

            toolbarTitle_tv.setText(R.string.menu_med_services_ar);
            medicalServicesText_tv.setText(R.string.pickup_medical_services_text_ar);
            submitText_tv.setText(R.string.submit_ar);
            name_et.setHint(R.string.name_ar);
            email_et.setHint(R.string.email_ar);
            qnty_et.setHint(R.string.menu_med_services_ar);
            phone_et.setHint(R.string.phone_ar);
            msg_et.setHint(R.string.message_ar);
        }
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
    }

    private String medicalServiceCategoryObj(String language) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String medicalServiceRequestObj(String language, String serviceId, String message, String qnty,
                                     String name, String email, String phone, String countryCode) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language);
            mainObj.put( "medicalservicecategory", serviceId);
            mainObj.put( "message", message);
            mainObj.put( "qty", qnty);
            mainObj.put( "name", name);
            mainObj.put( "email", email);
            mainObj.put( "phone", phone);
            mainObj.put( "country_code", countryCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void getMedicalServiceCatApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(MedicalServices.this))
        {
            try {

                showDataView();
                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseMedicalServiceCategory(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showDataView();
                            showSnackBar("Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    public void submitMedicalServiceRequestApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(MedicalServices.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        progressdialog.dismiss();

                        JSONObject jObj;
                            jObj = new JSONObject(result);

                            if(jObj.has("status")){

                                if(jObj.getBoolean("status")){

                                    showSnackBar("Submitted succesfully !");

                                    qnty_et.setText("");
                                    name_et.setText("");
                                    email_et.setText("");
                                    phone_et.setText("");
                                    msg_et.setText("");
                                    medicalCatSpinner_sp.setAdapter(catArray);
                                }
                                else{

                                }
                            }

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                            progressdialog.dismiss();
                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseMedicalServiceCategory(String response){

        JSONObject jObj;
        try {
            medicalServiceCatArrayList.clear();
            jObj = new JSONObject(response);

            /*news and events array variables*/
            if(jObj.has("medicalservicecategory")){

                /* news & events list array variables */
                String medCatListArray, cat_id_ = null, cat_name_ = null;
                JSONObject jObjMedCatList  = null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObj.getJSONArray("medicalservicecategory");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        medCatListArray = tempCatListArray.getString(i);
                        jObjMedCatList = new JSONObject(medCatListArray);

                        if (jObjMedCatList.has("medi_service_cat_id")) {

                            cat_id_ = jObjMedCatList.getString("medi_service_cat_id");
                        }

                        if (jObjMedCatList.has("name")) {

                            cat_name_ = jObjMedCatList.getString("name");
                        }

                        MedicalServiceCatItems tempCatList = new MedicalServiceCatItems( cat_id_, cat_name_);
                        medicalServiceCatArrayList.add(tempCatList);
                    }
                }

            }

            populatePropertyArray(medicalServiceCatArrayList);

            catArray= new ArrayAdapter<String>(MedicalServices.this,android.R.layout.simple_spinner_item, categoryArray);
            catArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            medicalCatSpinner_sp.setAdapter(catArray);

            if(medicalServiceCatArrayList.size() !=0){

                serviceId = medicalServiceCatArrayList.get(0).cat_id_;
            }

            showDataView();

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void populatePropertyArray(ArrayList<MedicalServiceCatItems> catArrayList) {

        categoryArray = new String[catArrayList.size()];

        for(int i = 0; i< catArrayList.size(); i++){
            categoryArray[i]= catArrayList.get(i).cat_name_;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Ok", new View.OnClickListener() {
                                        @Override
                    public void onClick(View view) {

                    }
                });
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
