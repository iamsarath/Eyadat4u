package app.ens.eyadat4u.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.TimeSlotPickerAdapter;
import app.ens.eyadat4u.items.TimeSlotItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.DatePickerFragment;
import app.ens.eyadat4u.utils.MonthPicker;
import app.ens.eyadat4u.utils.MonthPickerCustomized;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class TimeSlotPicker extends BaseActivity {

    Toolbar                     toolbar;
    ListView                    timeSlotList_lv;
    TextView                    toolbarTitle_tv, itemName_tv, itemLocation_tv, todaysDate_tv;
    TimeSlotPickerAdapter       timeSlotPickerAdapter;
    RequestQueue                queue;
    JsonObjectRequest           jsonObjectRequest;
    LinearLayout                parentLayout_ll, progressLayout_ll, dataLayout_ll, noDataView_tv, changeDate_ll;
    String                      itemId, itemName, clinicName, itemPic, location, selectedDate,  customizedDate;
    ProgressBar                 progressBar_pb;
    ImageView                   itemImage_iv;
    UILHandler                  uilHandler;

    ArrayList<TimeSlotItems>    timeSlotItemsArrayList_ = new ArrayList<TimeSlotItems>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot_picker);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue                   = Volley.newRequestQueue(TimeSlotPicker.this);
        uilHandler              = (UILHandler) getApplication();
        timeSlotPickerAdapter   = new TimeSlotPickerAdapter(TimeSlotPicker.this, timeSlotItemsArrayList_);

        itemId                  = getIntent().getExtras().getString("ITEM_ID");
        itemName                = getIntent().getExtras().getString("ITEM_NAME");
        clinicName              = getIntent().getExtras().getString("CLINIC_NAME");
        itemPic                 = getIntent().getExtras().getString("ITEM_PIC");
        location                = getIntent().getExtras().getString("LOCATION");

        initViews();
        setToolbar();

        itemName_tv.setText(itemName);
        itemLocation_tv.setText(clinicName);
        uilHandler.loadImage(itemPic, itemImage_iv , Constants.imageSpecifier.ROUND_EDGE_MEDIUM);

        timeSlotList_lv.setAdapter(timeSlotPickerAdapter);

        timeSlotList_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(TimeSlotPicker.this, BookAppointment.class);
                intent.putExtra("ITEM_ID",  itemId);
                intent.putExtra("ITEM_NAME", itemName);
                intent.putExtra("CLINIC_NAME", clinicName);
                intent.putExtra("ITEM_PIC",  itemPic);
                intent.putExtra("LOCATION", location);
                intent.putExtra("APPOINTMENT_TIME", ""+customizedDate);
                intent.putExtra("TIME_SLOT", timeSlotItemsArrayList_.get(position).time_slot_);
                intent.putExtra("TIME_SLOT_ID", timeSlotItemsArrayList_.get(position).time_slot_id_);
                startActivity(intent);
            }
        });

        changeDate_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDatePicker();
            }
        });

        //calling appointment checker API
        showProgressView();
        String obj = appointmentTimeSlotPickerObj(UserManager.getUserLanguage(TimeSlotPicker.this), itemId, getDateTime());
        timeSlotListingApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_APPOINTMENT_TIME_SLOT_PICKER), obj);

    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        dataLayout_ll       = (LinearLayout) findViewById(R.id.data_view);
        progressLayout_ll   = (LinearLayout) findViewById(R.id.progress_view);
        noDataView_tv       = (LinearLayout) findViewById(R.id.no_data_view);
        progressBar_pb      = (ProgressBar) findViewById(R.id.progressBar);
        timeSlotList_lv     = (ListView) findViewById(R.id.time_slot_list);
        itemName_tv         = (TextView) findViewById(R.id.item_name);
        itemLocation_tv     = (TextView) findViewById(R.id.item_location);
        todaysDate_tv       = (TextView) findViewById(R.id.todays_date);
        itemImage_iv        = (ImageView) findViewById(R.id.item_image);
        changeDate_ll       = (LinearLayout) findViewById(R.id.change_date);
        parentLayout_ll     = (LinearLayout) findViewById(R.id.parent_layout);

        DateFormat dateFormat = new SimpleDateFormat("dd");
        Date date_day = new Date();
        dateFormat.format(date_day);

        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date_month = new Date();
        monthFormat.format(date_month);

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);


        todaysDate_tv.setText(dateFormat.format(date_day)+" "+ MonthPicker.getMonth(monthFormat.format(date_month))+", "+year);
        selectedDate    = ""+dateFormat.format(date_day)+" "+ MonthPicker.getMonth(monthFormat.format(date_month))+" "+year;
        customizedDate  = dateFormat.format(date_day)+" "+ MonthPicker.getMonth(monthFormat.format(date_month))+", "+year;

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);

    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText("Pick an appointment time");
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
        noDataView_tv.setVisibility(View.GONE);
        changeDate_ll.setVisibility(View.VISIBLE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataView_tv.setVisibility(View.GONE);
        changeDate_ll.setVisibility(View.INVISIBLE);
    }

    public void showNoDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataView_tv.setVisibility(View.VISIBLE);
        changeDate_ll.setVisibility(View.VISIBLE);
    }

    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondatePicked);
        date.show(getSupportFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondatePicked = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            todaysDate_tv.setText(String.valueOf(dayOfMonth) + " " +  MonthPickerCustomized.getMonth(String.valueOf(monthOfYear+1))
                    + ", " + String.valueOf(year));

            String selectedDateNew = String.valueOf(dayOfMonth) + "/" +  String.valueOf(monthOfYear+1)
                    + "/" + String.valueOf(year);
            selectedDate = selectedDateNew;
            customizedDate = String.valueOf(dayOfMonth) + " " +  MonthPickerCustomized.getMonth(String.valueOf(monthOfYear+1))
                    + ", " + String.valueOf(year);

            //calling appointment checker API
            showProgressView();
            String obj = appointmentTimeSlotPickerObj(UserManager.getUserLanguage(TimeSlotPicker.this), itemId, selectedDateNew);
            timeSlotListingApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_APPOINTMENT_TIME_SLOT_PICKER), obj);

        }
    };

    private String appointmentTimeSlotPickerObj(String language, String itemId, String date) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );
            mainObj.put( "clinicalservice_id", itemId );
            mainObj.put( "date", date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();

        return dateFormat.format(date);
    }

    public void timeSlotListingApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(TimeSlotPicker.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        showDataView();
                        parseTimeSlotList(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing time slots list data
    void parseTimeSlotList(String response){

        JSONObject jObj;
        try {
            timeSlotItemsArrayList_.clear();
            jObj = new JSONObject(response);
//            JSONObject jObjSearchResult= null;
            String result, totalCount;

            /*clinics cat data array variables*/
            if(jObj.has("timing_data")){

                /* clinic list array variables */

                String searchListArray, time_slot_= null, schedule_id_= null;
                JSONObject jObjNewTimSlotList= null;
                JSONArray timingListArray = null;
                String timingArray = jObj.getString("timing_data");
                timingListArray = new JSONArray(timingArray);
                if (timingListArray.length() != 0) {
                    for (int i = 0; i < timingListArray.length(); i++) {

                        searchListArray = timingListArray.getString(i);
                        jObjNewTimSlotList = new JSONObject(searchListArray);

                        if (jObjNewTimSlotList.has("time_slot")) {

                            time_slot_ = jObjNewTimSlotList.getString("time_slot");
                        }
                        if (jObjNewTimSlotList.has("schedule_id")) {

                            schedule_id_ = jObjNewTimSlotList.getString("schedule_id");
                        }

                        TimeSlotItems tempCatList = new TimeSlotItems(schedule_id_, time_slot_);
                        timeSlotItemsArrayList_.add(tempCatList);
                    }
                }
            }

            timeSlotPickerAdapter.notifyDataSetChanged();
            if (timeSlotItemsArrayList_.size()==0){

                showNoDataView();
            }
            else {

                showDataView();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.Keys.APPOINTMENT_COMPLETE));

    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // checking for type intent filter
            if (intent.getAction().equals(Constants.Keys.APPOINTMENT_COMPLETE)) {
                finish();
                Log.e("Broadcast Reciever", "Appointment completed");
            }

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
