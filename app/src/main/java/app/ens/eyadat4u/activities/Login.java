package app.ens.eyadat4u.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.utils.L;

import org.json.JSONException;
import org.json.JSONObject;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class Login extends BaseActivity {

    TextView            signupText_tv, forgetPassword_tv, emailText_tv, passwordText_tv, submitText_tv, dontHaveAccountText_tv;
    EditText            username_et, password_et;
    LinearLayout        loginLayout_ll, parentLayout_ll, forgetPwd_ll, signUp_ll;
    String              username_, password_;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;
    ProgressDialog      progressdialog;
    Point               p;
    PopupWindow         popup;

    String org_OTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        int[] location = new int[2];
        p = new Point();
        p.x = location[0];
        p.y = location[1];

        queue    = Volley.newRequestQueue(Login.this);

        initViews();

        forgetPwd_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showForgetPasswordPopup(Login.this, p, UserManager.getUserId(Login.this));
            }
        });

        signUp_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentSignout = new Intent(Login.this, SignUp.class);
                startActivity(intentSignout);
                finish();
            }
        });

        loginLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username_   = username_et.getText().toString();
                password_   = password_et.getText().toString();

                if((username_ != null && username_.equals(""))|| (password_ != null && password_.equals(""))) {

                    showSnackBar("Please fill all the fields !");
                }
                else{

                    progressdialog = new ProgressDialog(Login.this);
                    progressdialog.setMessage("Login please wait...");
                    progressdialog.setCancelable(false);
                    progressdialog.show();

                    String obj = loginObj(username_, password_);
                    System.out.println("obj--> "+obj);
                    loginApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_USER_LOGIN), obj);
                }
            }
        });
    }

    private String resendOtpObj(String email) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "email", email);
            mainObj.put( "language", UserManager.getUserLanguage(Login.this));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void resendOtpApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(Login.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();


                        JSONObject jObj;
                        try {


                            jObj = new JSONObject(result);


//                                if(jObj.getBoolean("registration_status")) {



                            progressdialog.dismiss();
                            if(jObj.has("message")) {

                                showSnackBar(jObj.getString("message"));
                                Toast.makeText(Login.this,""+jObj.getString("message"),Toast.LENGTH_LONG).show();
                            }
                            if(jObj.has("email_otp")) {

                                org_OTP = jObj.getString("email_otp");
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }


    // Method for showing email verify popup.
    private void showEmailVerifyPopup(final Activity context, Point p, final String userId, final String name, final String email) {

        final int TIME_OUT  = 1500;

        final WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        int popupWidth = width;
        int popupHeight = height;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout                                 = layoutInflater.inflate(R.layout.popup_email_otp, viewGroup);
        TextView welcomeText_tv                     = (TextView) layout.findViewById(R.id.welcome_text);
        TextView resendOtpToEmail_tv                = (TextView) layout.findViewById(R.id.resend_otp);
        LinearLayout submitOtp_ll                   = (LinearLayout) layout.findViewById(R.id.submit_otp);
        final EditText userOtp_et                   = (EditText) layout.findViewById(R.id.user_otp);
        final TextView userEmail_tv                 = (TextView) layout.findViewById(R.id.email);

        welcomeText_tv.setText("Welcome back "+name+", your email is not yet verified.");
        userEmail_tv.setText(username_);

        resendOtpToEmail_tv.setPaintFlags(resendOtpToEmail_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        resendOtpToEmail_tv.setText("Resend OTP to your email");

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);
//
//        popup.getContentView().setFocusableInTouchMode(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 10;
        int OFFSET_Y = 10;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        resendOtpToEmail_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showResendOtpDialogue("Resend OTP", "Do you want to resend OTP to your email id: "+email, email);
            }
        });

        submitOtp_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(( userOtp_et.getText().toString() != null &&  userOtp_et.getText().toString().equals(""))) {

                    Toast.makeText(Login.this,"Please enter your OTP from email",Toast.LENGTH_LONG).show();
                }
                else{

                    if((userOtp_et.getText().toString().equals(org_OTP))){

                        progressdialog = new ProgressDialog(Login.this);
                        progressdialog.setMessage("Updating please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = verifyEmailObj(userId, userOtp_et.getText().toString());

                        emailVerifyApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_USER_VERIFY_EMAIL), obj);
                    }
                    else{
                        Toast.makeText(Login.this,"Please enter a valid OTP from email",Toast.LENGTH_LONG).show();
                    }

                }

            }
        });

    }

    private void showForgetPasswordPopup(final Activity context, Point p, final String userId) {

        final int TIME_OUT  = 1500;

        String userId_ = userId;

        final WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        int popupWidth = width;
        int popupHeight = height;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout                                 = layoutInflater.inflate(R.layout.popup_forget_password, viewGroup);
        ImageView closePopup_iv                     = (ImageView) layout.findViewById(R.id.close_popup);
        final EditText email_et                     = (EditText) layout.findViewById(R.id.email_id);
        LinearLayout submitNewPwd_ll                = (LinearLayout) layout.findViewById(R.id.reset_pwd_layout);
        TextView  resetPwd_tv                       = (TextView) layout.findViewById(R.id.reset_pwd_text);
        TextView  submitText_tv                     = (TextView) layout.findViewById(R.id.submit_text);

        if(UserManager.getUserLanguage(Login.this).equals("en")){

            resetPwd_tv.setText(R.string.reset_pwd_text_en);
            submitText_tv.setText(R.string.submit_en);
            email_et.setHint(R.string.email_text_en);
        }
        else {

            resetPwd_tv.setText(R.string.reset_pwd_text_ar);
            submitText_tv.setText(R.string.submit_ar);
            email_et.setHint(R.string.email_text_ar);
        }

        submitNewPwd_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((email_et.getText().toString() != null && email_et.getText().toString().equals(""))) {

                    showSnackBar("Please enter your email id.");

                } else {

                    progressdialog = new ProgressDialog(Login.this);
                    progressdialog.setMessage("Resetting please wait...");
                    progressdialog.setCancelable(false);
                    progressdialog.show();

                    String obj = forgetPasswordObj(UserManager.getUserLanguage(Login.this), email_et.getText().toString());
                    forgetPwdApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_FORGET_PASSWORD), obj);
                }
            }
        });

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);
//
//        popup.getContentView().setFocusableInTouchMode(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 10;
        int OFFSET_Y = 10;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        closePopup_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.dismiss();
            }
        });

    }

    private String forgetPasswordObj(String language, String email) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language);
            mainObj.put( "email", email);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void forgetPwdApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(Login.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            if(jObj.has("message")){
                                showSnackBar(jObj.getString("message"));
                            }
                            else{
                            }
                            progressdialog.dismiss();
                            popup.dismiss();

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    private String verifyEmailObj(String userId, String otp) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "user_id", userId);
            mainObj.put( "code", otp );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void emailVerifyApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(Login.this))
        {
            try {
                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        String userId_          = null;
                        String userName_        = null;
                        String email_           = null;
                        String phone_           = null;
                        String session_key_     = null;
                        String name_            = null;

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            //Validating the api
                            if(jObj.has("status")){

                                if(jObj.getBoolean("status")) {

                                    if(jObj.has("user_id")) {

                                        userId_= jObj.getString("user_id");
                                    }

                                    if(jObj.has("name_surname")) {

                                        name_= jObj.getString("name_surname");
                                    }
                                    if(jObj.has("username")) {

                                        userName_= jObj.getString("username");
                                    }
                                    if(jObj.has("email")) {

                                        email_= jObj.getString("email");
                                    }
                                    if(jObj.has("mobile")) {

                                        phone_= jObj.getString("mobile");
                                    }
                                    if(jObj.has("api_key")) {

                                        session_key_= jObj.getString("api_key");
                                    }

                                    progressdialog.dismiss();
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                    }

                                    progressdialog.dismiss();

                                    UserManager.updateUserLoginStatus(Login.this, "true");
                                    UserManager.updateUserStatus(Login.this, userId_,  name_, userName_, email_, phone_, session_key_);

                                    // Send registration complete broadcast message
                                    Intent registrationComplete = new Intent(Constants.Keys.REGISTRATION_COMPLETE);
                                    registrationComplete.putExtra("token", "Broadcast Message");
                                    LocalBroadcastManager.getInstance(Login.this).sendBroadcast(registrationComplete);
                                    popup.dismiss();

                                    finish();
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

                                } else{

                                    progressdialog.dismiss();
                                    showSnackBar(jObj.getString("message"));
                                }
                            }
                            else{

                                if(jObj.has("message")){

                                    showSnackBar(jObj.getString("message"));
                                }
                                else{
                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }


    private String loginObj(String username,String password) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "username", username );
            mainObj.put( "password", password );
            mainObj.put( "device_platform", "android");
            mainObj.put( "language", UserManager.getUserLanguage(Login.this));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void initViews() {

        signupText_tv           = (TextView) findViewById(R.id.signup);
        username_et             = (EditText) findViewById(R.id.username);
        password_et             = (EditText) findViewById(R.id.password);
        loginLayout_ll          = (LinearLayout) findViewById(R.id.login);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        forgetPassword_tv       = (TextView) findViewById(R.id.forget_password);
        forgetPwd_ll            = (LinearLayout) findViewById(R.id.forget_password_layout);
        signUp_ll               = (LinearLayout) findViewById(R.id.signup_layout);

        emailText_tv            = (TextView) findViewById(R.id.email_text);
        passwordText_tv         = (TextView) findViewById(R.id.password_text);
        submitText_tv           = (TextView) findViewById(R.id.submit_text);
        dontHaveAccountText_tv  = (TextView) findViewById(R.id.dont_hav_account_text);

        signupText_tv.setPaintFlags(signupText_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        signupText_tv.setText("Sign Up");
        setTexts();
    }

    public void setTexts(){

        if(UserManager.getUserLanguage(Login.this).equals("en")){

            emailText_tv.setText(R.string.email_text_en);
            passwordText_tv.setText(R.string.password_en);
            submitText_tv.setText(R.string.submit_en);
            dontHaveAccountText_tv.setText(R.string.dont_have_account_text_en);
            forgetPassword_tv.setText(R.string.forget_password_en);
            signupText_tv.setText(R.string.sign_up_en);
            username_et.setHint(R.string.email_text_en);
            password_et.setHint(R.string.password_en);
        }
        else {

            emailText_tv.setText(R.string.email_text_ar);
            passwordText_tv.setText(R.string.password_ar);
            submitText_tv.setText(R.string.submit_ar);
            dontHaveAccountText_tv.setText(R.string.dont_have_account_text_ar);
            forgetPassword_tv.setText(R.string.forget_password_ar);
            signupText_tv.setText(R.string.sign_up_ar);
            username_et.setHint(R.string.email_text_ar);
            password_et.setHint(R.string.password_ar);
        }
    }


    public void loginApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(Login.this))
        {
            try {
                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();
                        System.out.println("result--> "+result);

                        String userId_          = null;
                        String name_            = null;
                        String userName_        = null;
                        String email_           = null;
                        String phone_           = null;
                        String session_key_     = null;
                        String email_otp_       = null;

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            //Validating the api
                            if(jObj.has("login_status")){

                                if(jObj.getBoolean("login_status")) {

                                    if(jObj.has("user_id")) {

                                        userId_= jObj.getString("user_id");
                                    }
                                    if(jObj.has("name_surname")) {

                                        name_= jObj.getString("name_surname");
                                    }
                                    if(jObj.has("username")) {

                                        userName_= jObj.getString("username");
                                    }
                                    if(jObj.has("email")) {

                                        email_= jObj.getString("email");
                                    }
                                    if(jObj.has("phone")) {

                                        phone_= jObj.getString("phone");
                                    }
                                    if(jObj.has("api_key")) {

                                        session_key_= jObj.getString("api_key");
                                    }
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                    }

                                    progressdialog.dismiss();

                                    UserManager.updateUserLoginStatus(Login.this, "true");
                                    UserManager.updateUserStatus(Login.this, userId_,  name_, userName_, email_, phone_, session_key_);

                                    // Send registration complete broadcast message
                                    Intent registrationComplete = new Intent(Constants.Keys.REGISTRATION_COMPLETE);
                                    registrationComplete.putExtra("token", "Broadcast Message");
                                    LocalBroadcastManager.getInstance(Login.this).sendBroadcast(registrationComplete);

                                    finish();
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

//                                    if(jObj.has("mail_verified")) {
//
//                                        if(jObj.getBoolean("mail_verified")) {
//
//                                            UserManager.updateUserLoginStatus(Login.this, "true");
//                                            UserManager.updateUserStatus(Login.this, userId_,  name_, userName_, email_, phone_, session_key_);
//
//                                            // Send registration complete broadcast message
//                                            Intent registrationComplete = new Intent(Constants.Keys.REGISTRATION_COMPLETE);
//                                            registrationComplete.putExtra("token", "Broadcast Message");
//                                            LocalBroadcastManager.getInstance(Login.this).sendBroadcast(registrationComplete);
//
//                                            finish();
//                                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//
//                                        }
//                                        else{
//                                            if(jObj.has("email_otp")) {
//
//                                                org_OTP= jObj.getString("email_otp");
//                                            }
//
//                                            showSnackBar("Please verify your email id and continue");
//                                            showEmailVerifyPopup(Login.this, p, userId_, name_, email_);
//                                        }
//                                    }

                                } else{

                                    progressdialog.dismiss();
                                    showSnackBar(jObj.getString("message"));
                                }
                            }
                            else{

                                if(jObj.has("message")){

                                    showSnackBar(jObj.getString("message"));
                                }
                                else{
                                }

                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            if(jsonObjectRequest != null){

                queue.cancelAll(jsonObjectRequest);
            }
        }

    }

    private void showResendOtpDialogue(String title, String msg, final String email) {
        new AlertDialog.Builder(Login.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Resend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //calling book appointment API
                        progressdialog = new ProgressDialog(Login.this);
                        progressdialog.setMessage("Please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = resendOtpObj(email);
                        resendOtpApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_RESEND_OTP), obj);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        })
                .create().show();
    }

}
