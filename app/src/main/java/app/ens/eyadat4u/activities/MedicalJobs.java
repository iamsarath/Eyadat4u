package app.ens.eyadat4u.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.http.multipart.MultipartEntity;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.UUID;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.MedicalJobsItems;
import app.ens.eyadat4u.items.MedicalServiceCatItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class MedicalJobs extends BaseActivity {

    Toolbar                 toolbar;
    TextView                toolbarTitle_tv, fileName_tv, jobCatText_tv, submitText_tv;
    LinearLayout            parentLayout_ll, progressLayout_ll, dataLayout_ll, submit_ll;
    Spinner                 medicalJobsSpinner_sp;
    EditText                name_et, email_et, phone_et, msg_et;
    ProgressBar             progressBar_pb;
    Button                  upload_btn;
    RequestQueue            queue;
    JsonObjectRequest       jsonObjectRequest;
    ProgressDialog          progressdialog;
    File                    file1;
    String fileName ="";
    boolean isUploaded = false;

    Spinner                         countryCode_sp;
    String []                       countryCodeArray, codeTempArray;
    ArrayAdapter<String>            codeArrayAdapter;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    boolean is_code_spinner_selected    = false;
    int     codeSpinnerSelection       = 0;

    private static final int FILE_SELECT_CODE = 0;
    String TAG = "MedicalJobs";

    ArrayList<MedicalJobsItems> medicalJobsTypeArrayList   = new ArrayList<MedicalJobsItems>();
    ArrayAdapter<String>        jobTypeArrayList;
    String[]                    jobTypeArray;
    String                      jobId, countryCode;
    boolean                     is_spinner_selected    = false;
    int                         spinnerSelection       = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_jobs);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue     = Volley.newRequestQueue(MedicalJobs.this);

        initViews();
        setToolbar();

        countryCodeArray = new String[6];
        countryCodeArray[0]= "+ 965 (KWT)";
        countryCodeArray[1]= "+ 971 (ARE)";
        countryCodeArray[2]= "+ 973 (BHR)";
        countryCodeArray[3]= "+ 968 (OMN)";
        countryCodeArray[4]= "+ 974 (QAT)";
        countryCodeArray[5]= "+ 966 (SAU)";

        codeTempArray= new String[6];
        codeTempArray[0]= "965";
        codeTempArray[1]= "971";
        codeTempArray[2]= "973";
        codeTempArray[3]= "968";
        codeTempArray[4]= "974";
        codeTempArray[5]= "966";

        codeArrayAdapter= new ArrayAdapter<String>(MedicalJobs.this,android.R.layout.simple_spinner_item, countryCodeArray);
        codeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode_sp.setAdapter(codeArrayAdapter);

        countryCode =  codeTempArray[0];

        countryCode_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(is_code_spinner_selected){
//                    is_spinner_selected= false;
                    String selection = (String) parent.getItemAtPosition(position);
                    int pos = -1;

                    for (int i = 0; i < countryCodeArray.length; i++) {
                        if (countryCodeArray[i].equals(selection)) {

                            pos = i;
                            break;
                        }
                    }
                    codeSpinnerSelection = pos;
                    countryCode =  codeTempArray[codeSpinnerSelection];
                }
                else{
                    is_code_spinner_selected= true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        medicalJobsSpinner_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(is_spinner_selected){
//                    is_spinner_selected= false;
                    String selection = (String) parent.getItemAtPosition(position);
                    int pos = -1;

                    for (int i = 0; i < jobTypeArray.length; i++) {
                        if (jobTypeArray[i].equals(selection)) {

                            pos = i;
                            break;
                        }
                    }
                    spinnerSelection = pos;
                    jobId = medicalJobsTypeArrayList.get(pos).job_id_;
                }
                else{
                    is_spinner_selected= true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showFileChooser();
            }
        });

        submit_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = email_et.getText().toString().trim();
//                if (email.matches(emailPattern))
//                {
                    if((name_et.getText().toString() != null && name_et.getText().toString().equals("")) ||
                            (email_et.getText().toString() != null && email_et.getText().toString().equals(""))|| (phone_et.getText().toString() != null && phone_et.getText().toString().equals(""))
                            || (msg_et.getText().toString() != null && msg_et.getText().toString().equals(""))) {

                        showSnackBar("Please fill all the fields !");

                    }
                    else{

                        if (email.matches(emailPattern))
                        {
                            progressdialog = new ProgressDialog(MedicalJobs.this);
                            progressdialog.setMessage("Submitting please wait...");
                            progressdialog.setCancelable(false);
                            progressdialog.show();

                            String obj = medicalJobsRequestObj(UserManager.getUserLanguage(MedicalJobs.this),
                                    jobId, msg_et.getText().toString(),fileName, name_et.getText().toString(), email_et.getText().toString(), phone_et.getText().toString(), countryCode);
                            submitMedicalJobRequestApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICAL_SERVICE_REQUEST), obj);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Please enter a valid email address", Toast.LENGTH_SHORT).show();

                        }

                    }

            }
        });

        //calling medical jobs API
        showProgressView();
        String obj = medicalJobsTypeObj(UserManager.getUserLanguage(MedicalJobs.this));
        getMedicalJobTypestApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MEDICAL_JOB_TYPES), obj);
    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        jobCatText_tv           = (TextView) findViewById(R.id.job_cat_text);
        submitText_tv           = (TextView) findViewById(R.id.submit_text);
        dataLayout_ll           = (LinearLayout) findViewById(R.id.data_view);
        progressLayout_ll       = (LinearLayout) findViewById(R.id.progress_view);
        progressBar_pb          = (ProgressBar) findViewById(R.id.progressBar);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        submit_ll               = (LinearLayout) findViewById(R.id.submit);
        medicalJobsSpinner_sp   = (Spinner) findViewById(R.id.medical_jobs_spinner);
        name_et                 = (EditText) findViewById(R.id.name);
        email_et                = (EditText) findViewById(R.id.email);
        phone_et                = (EditText) findViewById(R.id.phone);
        msg_et                  = (EditText) findViewById(R.id.message);
        upload_btn              = (Button) findViewById(R.id.upload);
        fileName_tv             = (TextView) findViewById(R.id.file_name);
        countryCode_sp          = (Spinner) findViewById(R.id.country_code_spinner);

        fileName_tv.setText("Upload Resume");
        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(MedicalJobs.this).equals("en")){

            toolbarTitle_tv.setText(R.string.menu_med_jobs_en);
            jobCatText_tv.setText(R.string.job_cat_en);
            submitText_tv.setText(R.string.submit_en);
            name_et.setHint(R.string.name_en);
            email_et.setHint(R.string.email_text_en);
            phone_et.setHint(R.string.phone_text_en);
            msg_et.setHint(R.string.message_en);
            upload_btn.setText(R.string.upload_resume_en);
        }else{

            toolbarTitle_tv.setText(R.string.menu_med_jobs_ar);
            jobCatText_tv.setText(R.string.job_cat_ar);
            submitText_tv.setText(R.string.submit_ar);
            name_et.setHint(R.string.name_ar);
            email_et.setHint(R.string.email_text_ar);
            phone_et.setHint(R.string.phone_text_ar);
            msg_et.setHint(R.string.message_ar);
            upload_btn.setText(R.string.upload_resume_ar);
        }
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("application/pdf");
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult( Intent.createChooser(intent, "Select a File to Upload"), FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();

                    File f = new File(getPath(MedicalJobs.this, uri));

                    System.out.println("Path: "+ getPath(MedicalJobs.this, uri));
                    Log.d("Medical Jobs : ","Path:"+getPath(MedicalJobs.this, uri));

                    fileName_tv.setText(""+f.getName());
                    new UploadFileAsync().execute(getPath(MedicalJobs.this, uri));
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

//    @TargetApi(Build.VERSION_CODES.KITKAT)
//    public static String getPath(final Context context, final Uri uri) {
//
//        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//
//        // DocumentProvider
//        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//            // ExternalStorageProvider
////            if (isExternalStorageDocument(uri)) {
//                final String docId = DocumentsContract.getDocumentId(uri);
//                final String[] split = docId.split(":");
//                final String type = split[0];
//
//                if ("primary".equalsIgnoreCase(type)) {
//                    return Environment.getExternalStorageDirectory() + "/" + split[1];
//                }
//
//                // TODO handle non-primary volumes
////            }
////            // DownloadsProvider
////            else if (isDownloadsDocument(uri)) {
////
////                final String id = DocumentsContract.getDocumentId(uri);
////                final Uri contentUri = ContentUris.withAppendedId(
////                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
////
////                return getDataColumn(context, contentUri, null, null);
////            }
////            // MediaProvider
////            else if (isMediaDocument(uri)) {
////                final String docId = DocumentsContract.getDocumentId(uri);
////                final String[] split = docId.split(":");
////                final String type = split[0];
////
////                Uri contentUri = null;
////                if ("image".equals(type)) {
////                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
////                } else if ("video".equals(type)) {
////                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
////                } else if ("audio".equals(type)) {
////                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
////                }
////
////                final String selection = "_id=?";
////                final String[] selectionArgs = new String[] {
////                        split[1]
////                };
////
////                return getDataColumn(context, contentUri, selection, selectionArgs);
////            }
//        }
//////         MediaStore (and general)
////        else if ("content".equalsIgnoreCase(uri.getScheme())) {
////
////            // Return the remote address
////            if (isGooglePhotosUri(uri))
////                return uri.getLastPathSegment();
////
////            return getDataColumn(context, uri, null, null);
////        }
////        // File
////        else if ("file".equalsIgnoreCase(uri.getScheme())) {
////            return uri.getPath();
////        }
//
//        return null;
//    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private class UploadFileAsync extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressdialog = new ProgressDialog(MedicalJobs.this);
            progressdialog.setCancelable(false);
            progressdialog.setMessage("Uploading file please wait ...");
            progressdialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {

//                String sourceFileUri = "/sdcard/PERSONAL/NewApi.pdf";
                String sourceFileUri = params[0];

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;

//                if (sourceFile.isFile()) {

                    try {

//                        String upLoadServerUri = "http://arworksdemo.com/eyadat4ulatest/index.php/api/file_upload";
                        String upLoadServerUri = UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_FILE_UPLOAD);

//                        String fullPath = Environment.getExternalStorageDirectory().toString()+sourceFileUri;
                        String fullPath = sourceFileUri;
                        File sourceFile = new File(sourceFileUri);
                        FileInputStream fileInputStream = new FileInputStream( sourceFile);
                        URL url = new URL(upLoadServerUri);

                        // Open a HTTP connection to the URL
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setDoInput(true); // Allow Inputs
                        conn.setDoOutput(true); // Allow Outputs
                        conn.setUseCaches(false); // Don't use a Cached Copy
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        conn.setRequestProperty("ENCTYPE",
                                "multipart/form-data");
                        conn.setRequestProperty("Content-Type",
                                "multipart/form-data;boundary=" + boundary);
                        conn.setRequestProperty("userfile", sourceFileUri);

                        dos = new DataOutputStream(conn.getOutputStream());

                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"userfile\";filename=\""
                                + sourceFile + "\"" + lineEnd);

                        dos.writeBytes(lineEnd);

                        // create a buffer of maximum size
                        bytesAvailable = fileInputStream.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {

                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math
                                    .min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0,
                                    bufferSize);

                        }

                        // send multipart form data necesssary after file
                        // data...
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens
                                + lineEnd);

                        // Responses from the server (code and message)
//                        serverResponseCode = conn.getResponseCode();
                        String serverResponseMessage = conn.getResponseMessage();
                        if (serverResponseMessage.equals("OK")) {

//                            Toast.makeText(MedicalJobs.this, "File Upload Complete : "+ serverResponseMessage, Toast.LENGTH_SHORT).show();
                            InputStream in = null;
                            try {
                                in = conn.getInputStream();
//                                byte[] buffer = new byte[1024];
                                int read;
                                while ((read = in.read(buffer)) > 0) {
                                    return new String(buffer, 0, read, "utf-8");
                                }

                            } finally {
                                in.close();
                            }

                        }

                        // close the streams //
                        fileInputStream.close();
                        dos.flush();
                        dos.close();

                        return serverResponseMessage;

                    } catch (Exception e) {

                        // dialog.dismiss();
                        e.printStackTrace();

                    }

            } catch (Exception ex) {
                // dialog.dismiss();

                ex.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            progressdialog.dismiss();


            try {

                JSONObject jObj = new JSONObject(result.toString());

                if(jObj.has("status")){

                    if(jObj.getBoolean("status")){
                        Toast.makeText(MedicalJobs.this, "Uploaded successfully !", Toast.LENGTH_LONG).show();

                        fileName = jObj.getString("file_name");
                    }
                }
                else{

                    Toast.makeText(MedicalJobs.this, "Error in uploading", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {


        }
    }

    private String medicalJobsTypeObj(String language) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String medicalJobsRequestObj(String language, String serviceId, String message, String resume,
                                      String name, String email, String phone, String countryCode) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );
            mainObj.put( "medicaljobtype_id", serviceId );
            mainObj.put( "message", message );
            mainObj.put( "resume", resume );
            mainObj.put( "name", name );
            mainObj.put( "email", email );
            mainObj.put( "phone", phone );
            mainObj.put( "country_code", countryCode);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void submitMedicalJobRequestApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(MedicalJobs.this))
        {
            try {

                showDataView();
                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        progressdialog.dismiss();
                        showSnackBar("Job request send successfully !");
                        String result = response.toString();
                        fileName = "";
                        fileName_tv.setText("Upload Resume");
                        name_et.setText("");
                        email_et.setText("");
                        phone_et.setText("");
                        msg_et.setText("");

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showDataView();
                            showSnackBar("Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    public void getMedicalJobTypestApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(MedicalJobs.this))
        {
            try {

                showDataView();
                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseMedicalServiceCategory(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showDataView();
                            showSnackBar("Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseMedicalServiceCategory(String response){

        JSONObject jObj;
        try {
            medicalJobsTypeArrayList.clear();
            jObj = new JSONObject(response);

            /*news and events array variables*/
            if(jObj.has("medicaljobtypes")){

                /* news & events list array variables */
                String medJobsListArray, job_id_ = null, job_name_ = null;
                JSONObject jObjMedCatList  = null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObj.getJSONArray("medicaljobtypes");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        medJobsListArray = tempCatListArray.getString(i);
                        jObjMedCatList = new JSONObject(medJobsListArray);

                        if (jObjMedCatList.has("medicaljobtype_id")) {

                            job_id_ = jObjMedCatList.getString("medicaljobtype_id");
                        }

                        if (jObjMedCatList.has("name")) {

                            job_name_ = jObjMedCatList.getString("name");
                        }

                        MedicalJobsItems tempCatList = new MedicalJobsItems( job_id_, job_name_);
                        medicalJobsTypeArrayList.add(tempCatList);
                    }
                }

            }

            populateTypeArray(medicalJobsTypeArrayList);

            jobTypeArrayList= new ArrayAdapter<String>(MedicalJobs.this,android.R.layout.simple_spinner_item, jobTypeArray);
            jobTypeArrayList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            medicalJobsSpinner_sp.setAdapter(jobTypeArrayList);

            if(medicalJobsTypeArrayList.size() !=0){

                jobId = medicalJobsTypeArrayList.get(0).job_id_;
            }

            showDataView();

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    private void populateTypeArray(ArrayList<MedicalJobsItems> catArrayList) {

        jobTypeArray = new String[catArrayList.size()];

        for(int i = 0; i< catArrayList.size(); i++){
            jobTypeArray[i]= catArrayList.get(i).job_name_;
        }
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
