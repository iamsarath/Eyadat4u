package app.ens.eyadat4u.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.InsuranceAdapter;
import app.ens.eyadat4u.adapters.PromotionsAdapter;
import app.ens.eyadat4u.items.NewsAndEventsItems;
import app.ens.eyadat4u.items.PromotionItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class PromotionsList extends BaseActivity {

    Toolbar             toolbar;
    ListView            promosList_lv;
    TextView            toolbarTitle_tv;
    PromotionsAdapter   promotionsAdapter;
    LinearLayout        parentLayout_ll, progressLayout_ll, dataLayout_ll;
    ProgressBar         progressBar_pb;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;

    ArrayList<PromotionItems> promotionsArrayList   = new ArrayList<PromotionItems>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotions_list);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue                   = Volley.newRequestQueue(PromotionsList.this);
        promotionsAdapter       = new PromotionsAdapter(PromotionsList.this, promotionsArrayList);
        initViews();
        setToolbar();

        promosList_lv.setAdapter(promotionsAdapter);

        //calling promotions API
        showProgressView();
        String obj = promosDataObj(UserManager.getUserLanguage(PromotionsList.this), "0");
        promoListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_PROMOTIONS), obj);
    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        promosList_lv       = (ListView) findViewById(R.id.promos_list);
        progressLayout_ll   = (LinearLayout) findViewById(R.id.progress_view);
        dataLayout_ll       = (LinearLayout) findViewById(R.id.data_view);
        parentLayout_ll     = (LinearLayout) findViewById(R.id.parent_layout);
        progressBar_pb      = (ProgressBar) findViewById(R.id.progressBar);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(PromotionsList.this).equals("en")){

            toolbarTitle_tv.setText(R.string.menu_promotions_en);
        }else{

            toolbarTitle_tv.setText(R.string.menu_promotions_ar);
        }
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
    }

    private String promosDataObj(String language, String pageNumber) {

        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put( "language", language );
            mainObj.put( "page_number", pageNumber );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void promoListApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(PromotionsList.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseServiceList(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseServiceList(String response){

        JSONObject jObj;
        try {
            promotionsArrayList.clear();
            jObj = new JSONObject(response);

            /*clinics cat data array variables*/
            if(jObj.has("promotions")){

                /* clinic list array variables */
                String promosListArray, promo_id_ = null, start_date_= null, end_date_= null, title_= null, description_= null, news_image_= null;
                JSONObject jObjNewsEventsList= null;
                JSONArray tempPromosListArray = null;

                tempPromosListArray = jObj.getJSONArray("promotions");
                if (tempPromosListArray.length() != 0) {
                    for (int i = 0; i < tempPromosListArray.length(); i++) {


                        promosListArray = tempPromosListArray.getString(i);
                        jObjNewsEventsList = new JSONObject(promosListArray);

                        if (jObjNewsEventsList.has("pro_id")) {

                            promo_id_ = jObjNewsEventsList.getString("pro_id");
                        }
                        if (jObjNewsEventsList.has("start_date")) {

                            start_date_ = jObjNewsEventsList.getString("start_date");
                        }
                        if (jObjNewsEventsList.has("end_date")) {

                            end_date_ = jObjNewsEventsList.getString("end_date");
                        }
                        if (jObjNewsEventsList.has("title")) {

                            title_ = jObjNewsEventsList.getString("title");
                        }
                        if (jObjNewsEventsList.has("description")) {

                            description_ = jObjNewsEventsList.getString("description");
                        }
                        if (jObjNewsEventsList.has("promo_image")) {

                            news_image_ = jObjNewsEventsList.getString("promo_image");
                        }

                        PromotionItems tempCatList = new PromotionItems( promo_id_, start_date_, end_date_, title_, description_, news_image_);
                        promotionsArrayList.add(tempCatList);
                    }
                }

            }

            promotionsAdapter.notifyDataSetChanged();
            showDataView();

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }

}