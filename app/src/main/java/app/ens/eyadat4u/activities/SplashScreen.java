package app.ens.eyadat4u.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UserManager;

public class SplashScreen extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                Intent myIntent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(myIntent);

            }
        }, SPLASH_TIME_OUT);
    }

}
