package app.ens.eyadat4u.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.DetailPageServicesAdapter;
import app.ens.eyadat4u.fragments.ServiceDetailsClinicListTab;
import app.ens.eyadat4u.fragments.ServiceDetailsDoctorsListTab;
import app.ens.eyadat4u.fragments.ServiceDetailsFeedbackTab;
import app.ens.eyadat4u.fragments.ServiceDetailsGalleryTab;
import app.ens.eyadat4u.fragments.ServiceDetailsMapTab;
import app.ens.eyadat4u.items.FeedbackItems;
import app.ens.eyadat4u.items.GalleryItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.items.ServicesItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;
import app.ens.eyadat4u.views.HorizontalListView;

public class ServiceDetails extends BaseActivity {

    Toolbar         toolbar;
    TextView        toolbarTitle_tv, viewAllTimings_tv, itemName_tv, expQualif_tv, titleAvailability_tv, titleAvailabilityExp_tv,
                    about_tv, qualification_tv, specialities_tv, timeAvailableTop_tv, timeAvailableBottom_tv, fees_tv,
                    rating_tv, views_tv, visitingDocDesc_tv, feesLabel_tv;
    RatingBar       ratingBar_rb;
    ImageView       itemPic_iv;
    LinearLayout    bookAppointment_ll, expExperience_ll, expEducation_ll, expServiceList_ll, expFees_ll, expSpecialities_ll, parentSpecialityLayout_ll, parentFeesLayout_ll, parentQualificationyLayout_ll,
                    expQualifications_ll, timingDetails_ll, visitingDoc_ll, servicesLayout_ll, educationLayout_ll, experienceLayout_ll,
                    medicineLayout_ll;
    HorizontalListView          serviceList_hlv;
    DetailPageServicesAdapter   detailPageServiceAdapter;

    private TabPagerClinicsAdapter  tabPagerClinicsAdapter;
    private TabPagerDoctorsAdapter  tabPagerDoctorsAdapter;
    private TabPagerOthersAdapter   tabPagerOthersAdapter;
    private ViewPager               viewPager;
    private TabLayout               tabLayout;

    TextView            aboutText_tv, typeText_tv, availabilityText_tv, consultationFeeText_tv, specialitiesText_tv, qualificationsText_tv, services_tv,
                        bookAppointmentText_tv, allTimingsText_tv, educationText_tv, education_tv, experienceText_tv, experience_tv;

    LinearLayout        parentLayout_ll, progressLayout_ll, dataLayout_ll, headerLayout_ll;
    ProgressBar         progressBar_pb;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;
    UILHandler          uilHandler;

    ArrayList<ServicesItems>                servicesItemsArrayList_    = new ArrayList<ServicesItems>();
    public ArrayList<SearchItems>           connectedItemsArrayList_   = new ArrayList<SearchItems>();
    public ArrayList<FeedbackItems>         feedbackItemsArrayList_    = new ArrayList<FeedbackItems>();
    public ArrayList<GalleryItems>          galleryItemsArrayList_     = new ArrayList<GalleryItems>();

    ExpandableRelativeLayout education_erl, experience_erl, services_erl, qualifications_erl, specialities_erl, fees_erl;
    String tabData_en[], tabData_ar[];
    String data, title, itemId, picId, timing, itemName, clinicName, location, latitude, longitude, customSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_details);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue                       = Volley.newRequestQueue(ServiceDetails.this);
        detailPageServiceAdapter    = new DetailPageServicesAdapter(ServiceDetails.this, servicesItemsArrayList_);
        uilHandler                  = (UILHandler) getApplication();

        data            = getIntent().getExtras().getString("KEY_TYPE");
        title           = getIntent().getExtras().getString("ITEM_NAME");
        picId           = getIntent().getExtras().getString("ITEM_PIC");
        itemId          = getIntent().getExtras().getString("ITEM_ID");
        itemName        = title;
        clinicName      = getIntent().getExtras().getString("CLINIC_NAME");
        customSearch    = getIntent().getExtras().getString("CUSTOM_SEARCH");

        initViews();
        setToolbar();

        if(customSearch == null){

            medicineLayout_ll.setVisibility(View.GONE);
        }
        else{
            if(customSearch.equals("medicine_search")){

                medicineLayout_ll.setVisibility(View.VISIBLE);
            }
            else{

                medicineLayout_ll.setVisibility(View.GONE);
            }
        }

        toolbarTitle_tv.setText(title);

        SpannableString content = new SpannableString("All Timings");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        viewAllTimings_tv.setText(content);

        itemName_tv.setText(title);
        serviceList_hlv.setAdapter(detailPageServiceAdapter);

        uilHandler.loadImage(picId, itemPic_iv , Constants.imageSpecifier.NORMAL);

        //calling details API
        callDetailsApi();

        medicineLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(ServiceDetails.this, MedicineSearch.class);
                myIntent.putExtra("SERVICE_ID",  itemId);
                startActivity(myIntent);
            }
        });

        bookAppointment_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserManager.getUserLoginStatus(ServiceDetails.this)){

                    Intent myIntent = new Intent(ServiceDetails.this, TimeSlotPicker.class);
                    myIntent.putExtra("ITEM_ID",  itemId);
                    myIntent.putExtra("ITEM_NAME",  itemName);
                    myIntent.putExtra("CLINIC_NAME", clinicName);
                    myIntent.putExtra("ITEM_PIC",  picId);
                    myIntent.putExtra("LOCATION",  location);
                    startActivity(myIntent);
                }
                else{

                    showAlertDialogue("Login", "You need to login for booking appointments");
                }
            }
        });

        viewAllTimings_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(ServiceDetails.this, AvailableTimeSlots.class);
                myIntent.putExtra("TIMING_ARRAY", timing);
                myIntent.putExtra("ITEM_NAME", itemName);
                myIntent.putExtra("LOCATION", location);
                startActivity(myIntent);
            }
        });

        timingDetails_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(ServiceDetails.this, AvailableTimeSlots.class);
                myIntent.putExtra("TIMING_ARRAY", timing);
                myIntent.putExtra("ITEM_NAME", itemName);
                myIntent.putExtra("LOCATION", location);
                startActivity(myIntent);
            }
        });

        expExperience_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                experience_erl.toggle(); // toggle expand and collapse
            }
        });

        expEducation_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                education_erl.toggle(); // toggle expand and collapse
            }
        });


        expServiceList_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                services_erl.toggle(); // toggle expand and collapse
            }
        });

        expFees_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fees_erl.toggle(); // toggle expand and collapse
            }
        });

        expSpecialities_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                specialities_erl.toggle(); // toggle expand and collapse
            }
        });

        expQualifications_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qualifications_erl.toggle(); // toggle expand and collapse
            }
        });
    }

    public void setTexts(){

        if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

            aboutText_tv.setText(R.string.about_en);
            typeText_tv.setText(R.string.type_en);
            availabilityText_tv.setText(R.string.availability_en);
            consultationFeeText_tv.setText(R.string.fees_en);
            specialitiesText_tv.setText(R.string.specialities_en);
            qualificationsText_tv.setText(R.string.qualifications_en);
            educationText_tv.setText(R.string.education_en);
            experienceText_tv.setText(R.string.experience_en);
            services_tv.setText(R.string.services_en);
            bookAppointmentText_tv.setText(R.string.book_appointment_en);
            allTimingsText_tv.setText(R.string.all_timings_en);
            viewAllTimings_tv.setText(R.string.all_timings_en);
        }
        else{

            aboutText_tv.setText(R.string.about_ar);
            typeText_tv.setText(R.string.type_ar);
            availabilityText_tv.setText(R.string.availability_ar);
            consultationFeeText_tv.setText(R.string.fees_ar);
            specialitiesText_tv.setText(R.string.specialities_ar);
            qualificationsText_tv.setText(R.string.qualifications_ar);
            educationText_tv.setText(R.string.education_ar);
            experienceText_tv.setText(R.string.experience_ar);
            services_tv.setText(R.string.services_ar);
            bookAppointmentText_tv.setText(R.string.book_appointment_ar);
            allTimingsText_tv.setText(R.string.all_timings_ar);
            viewAllTimings_tv.setText(R.string.all_timings_ar);
        }
    }

    void callDetailsApi(){

        showProgressView();
        String obj = serviceDetailsObj(UserManager.getUserLanguage(ServiceDetails.this), itemId);
        serviceDetailsApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_ITEM_DETAILS), obj);
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // checking for type intent filter
            if (intent.getAction().equals(Constants.Keys.SUBMITTED_FEEDBACK)) {

//                callDetailsApi();
                Log.e("Broadcast Reciever", "Submitted feedback");
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.Keys.SUBMITTED_FEEDBACK));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(ServiceDetails.this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    private void initViews() {

        toolbar                         = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv                 = (TextView) findViewById(R.id.toolbar_title);
        itemName_tv                     = (TextView) findViewById(R.id.item_name);
        expQualif_tv                    = (TextView) findViewById(R.id.qualif_exp);
        itemPic_iv                      = (ImageView) findViewById(R.id.item_pic);
        titleAvailability_tv            = (TextView) findViewById(R.id.title_availability);
        titleAvailabilityExp_tv         = (TextView) findViewById(R.id.title_availability_exp);
        serviceList_hlv                 = (HorizontalListView) findViewById(R.id.services_list);
        about_tv                        = (TextView) findViewById(R.id.detail_page_about);
        qualification_tv                = (TextView) findViewById(R.id.qualification);
        specialities_tv                 = (TextView) findViewById(R.id.specialities);
        timeAvailableTop_tv             = (TextView) findViewById(R.id.time_available_top);
        timeAvailableBottom_tv          = (TextView) findViewById(R.id.time_available_bottom);
        fees_tv                         = (TextView) findViewById(R.id.fees);
        rating_tv                       = (TextView) findViewById(R.id.rating);
        views_tv                        = (TextView) findViewById(R.id.views);
        visitingDocDesc_tv              = (TextView) findViewById(R.id.visiting_doc_desc);
        bookAppointmentText_tv          = (TextView) findViewById(R.id.book_appointment_text);
        allTimingsText_tv               = (TextView) findViewById(R.id.all_timings_text);

        bookAppointment_ll              = (LinearLayout) findViewById(R.id.book_appointment_layout);
        ratingBar_rb                    = (RatingBar) findViewById(R.id.rating_bar);
        headerLayout_ll                 = (LinearLayout) findViewById(R.id.header_layout);
//        clinicLayout_ll                 = (LinearLayout) findViewById(R.id.clinics_layout);
//        serviceCount_tv                 = (TextView) findViewById(R.id.service_count);
        viewAllTimings_tv               = (TextView) findViewById(R.id.view_all_timings);
        viewPager                       = (ViewPager) findViewById(R.id.viewpager);
        tabLayout                       = (TabLayout) findViewById(R.id.detail_page_tabs);
        expExperience_ll                = (LinearLayout) findViewById(R.id.button_layout_experience);
        expEducation_ll                 = (LinearLayout) findViewById(R.id.button_layout_education);
        expServiceList_ll               = (LinearLayout) findViewById(R.id.button_layout_services);
        expFees_ll                      = (LinearLayout) findViewById(R.id.button_layout_fees);
        expSpecialities_ll              = (LinearLayout) findViewById(R.id.button_layout_speciality);
        expQualifications_ll            = (LinearLayout) findViewById(R.id.button_layout_qualifications);
        parentSpecialityLayout_ll       = (LinearLayout) findViewById(R.id.parent_speciality_layout);
        parentQualificationyLayout_ll   = (LinearLayout) findViewById(R.id.parent_qualification_layout);
        parentFeesLayout_ll             = (LinearLayout) findViewById(R.id.parent_fees_layout);
        timingDetails_ll                = (LinearLayout) findViewById(R.id.timing_details);
        visitingDoc_ll                  = (LinearLayout) findViewById(R.id.visiting_doc_layout);
//        feesLabel_tv                    = (TextView) findViewById(R.id.fees_label);

        aboutText_tv                    = (TextView) findViewById(R.id.about_text);
        typeText_tv                     = (TextView) findViewById(R.id.type_text);
        availabilityText_tv             = (TextView) findViewById(R.id.availability_text);
        consultationFeeText_tv          = (TextView) findViewById(R.id.fees_text);
        specialitiesText_tv             = (TextView) findViewById(R.id.speciality_text);
        qualificationsText_tv           = (TextView) findViewById(R.id.qualification_text);
        services_tv                     = (TextView) findViewById(R.id.services_text);
        education_tv                    = (TextView) findViewById(R.id.education);
        educationText_tv                = (TextView) findViewById(R.id.education_text);
        experience_tv                   = (TextView) findViewById(R.id.experience);
        experienceText_tv               = (TextView) findViewById(R.id.experience_text);

        experience_erl                  = (ExpandableRelativeLayout) findViewById(R.id.exp_experience_layout);
        education_erl                   = (ExpandableRelativeLayout) findViewById(R.id.exp_education_layout);
        services_erl                    = (ExpandableRelativeLayout) findViewById(R.id.exp_service_list_layout);
        specialities_erl                = (ExpandableRelativeLayout) findViewById(R.id.exp_speciality_layout);
        qualifications_erl              = (ExpandableRelativeLayout) findViewById(R.id.exp_qualifications_layout);
        fees_erl                        = (ExpandableRelativeLayout) findViewById(R.id.exp_fees_layout);

        educationLayout_ll              = (LinearLayout) findViewById(R.id.education_layout);
        experienceLayout_ll             = (LinearLayout) findViewById(R.id.experience_layout);
        servicesLayout_ll               = (LinearLayout) findViewById(R.id.services_layout);
        parentLayout_ll                 = (LinearLayout) findViewById(R.id.parent_fees_layout);
        dataLayout_ll                   = (LinearLayout) findViewById(R.id.data_view);
        progressLayout_ll               = (LinearLayout) findViewById(R.id.progress_view);
        progressBar_pb                  = (ProgressBar) findViewById(R.id.progressBar);
        medicineLayout_ll               = (LinearLayout) findViewById(R.id.medicine_search_layout);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);

        LayerDrawable stars = (LayerDrawable) ratingBar_rb.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.Yellow), PorterDuff.Mode.SRC_ATOP);

        parentSpecialityLayout_ll.setVisibility(View.GONE);
        parentQualificationyLayout_ll.setVisibility(View.GONE);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText("");
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
        headerLayout_ll.setVisibility(View.VISIBLE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        tabLayout.setVisibility(View.GONE);
        headerLayout_ll.setVisibility(View.INVISIBLE);
    }

    private String serviceDetailsObj(String language, String serviceId) {

        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put( "language", language );
            mainObj.put( "clinicalservice_id", serviceId );
            mainObj.put( "date", getDateTime() );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void serviceDetailsApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(ServiceDetails.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseServiceList(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseServiceList(String response){

        JSONObject jObj;
//        showDataView();
        try {
//            searchItemsArrayList.clear();
            jObj = new JSONObject(response);
            String  details;

            /*clinics cat data array variables*/
            if(jObj.has("details")){

                /* clinic list array variables */
                JSONObject jObjDetails= null;

                details= jObj.getString("details");

                jObjDetails= new JSONObject(details);

                if(jObjDetails.has("gps_lat")){

                    latitude = jObjDetails.getString("gps_lat");
                }
                if(jObjDetails.has("gps_lng")){

                    longitude = jObjDetails.getString("gps_lng");
                }

                /* Service Array */
                if(jObjDetails.has("services")){

                    servicesItemsArrayList_.clear();
                    String servicesListArray, service_id_= null, service_name_= null;
                    JSONObject jObjServicesList= null;
                    JSONArray tempServicesListArray = null;
                    tempServicesListArray = jObjDetails.getJSONArray("services");

                    if (tempServicesListArray.length() != 0) {

                        for (int i = 0; i < tempServicesListArray.length(); i++) {

                            servicesListArray = tempServicesListArray.getString(i);
                            jObjServicesList = new JSONObject(servicesListArray);

                            if (jObjServicesList.has("service_id")) {

                                service_id_ = jObjServicesList.getString("service_id");
                            }
                            if (jObjServicesList.has("name")) {

                                service_name_ = jObjServicesList.getString("name");
                            }

                            ServicesItems tempServiceItems = new ServicesItems(service_id_, service_name_);
                            servicesItemsArrayList_.add(tempServiceItems);
                        }
                    }

                    detailPageServiceAdapter.notifyDataSetChanged();
                }

                  /* Feedback Array */
                if(jObj.has("feedbacks")){

                    feedbackItemsArrayList_.clear();
                    String feedbackListArray, user_name_= null, feedback_id_= null, title_= null, message_= null, rating_= null;
                    JSONObject jObjFeedbackList= null;
                    JSONArray tempFeedbackListArray = null;
                    feedbackItemsArrayList_.clear();
                    tempFeedbackListArray = jObj.getJSONArray("feedbacks");

                    if (tempFeedbackListArray.length() != 0) {
                        for (int i = 0; i < tempFeedbackListArray.length(); i++) {

                            feedbackListArray = tempFeedbackListArray.getString(i);
                            jObjFeedbackList = new JSONObject(feedbackListArray);

                            if (jObjFeedbackList.has("name_surname")) {

                                user_name_ = jObjFeedbackList.getString("name_surname");
                            }
                            if (jObjFeedbackList.has("feedback_id")) {

                                feedback_id_ = jObjFeedbackList.getString("feedback_id");
                            }
                            if (jObjFeedbackList.has("title")) {

                                title_ = jObjFeedbackList.getString("title");
                            }
                            if (jObjFeedbackList.has("message")) {

                                message_ = jObjFeedbackList.getString("message");
                            }
                            if (jObjFeedbackList.has("rating")) {

                                rating_ = jObjFeedbackList.getString("rating");
                            }

                            FeedbackItems tempFeedback = new FeedbackItems(feedback_id_, title_, message_, user_name_, rating_);
                            feedbackItemsArrayList_.add(tempFeedback);
                        }
                    }
                }

                /* Gallery Array */
                if(jObjDetails.has("gallery")){

                    galleryItemsArrayList_.clear();
                    String galleryListArray, image_id_= null, image_url_= null;
                    JSONObject jObjGalleryList= null;
                    JSONArray tempGalleryListArray = null;
                    galleryItemsArrayList_.clear();
                    tempGalleryListArray = jObjDetails.getJSONArray("gallery");

                    if (tempGalleryListArray.length() != 0) {
                        for (int i = 0; i < tempGalleryListArray.length(); i++) {

                            galleryListArray = tempGalleryListArray.getString(i);
                            jObjGalleryList = new JSONObject(galleryListArray);

                            if (jObjGalleryList.has("id")) {

                                image_id_ = jObjGalleryList.getString("id");
                            }
                            if (jObjGalleryList.has("url")) {

                                image_url_ = jObjGalleryList.getString("url");
                            }

                            GalleryItems galleryItems = new GalleryItems(image_id_, image_url_);
                            galleryItemsArrayList_.add(galleryItems);
                        }
                    }
                }

                /*clinics connected list array variables*/
                if(jObj.has("connected_list")){

                    connectedItemsArrayList_.clear();
                    String searchListArray,  type_name_ = null, type_= null, item_id_= null, item_name_= null, item_thumb_= null, clinic_name_= null, location_= null,
                            qualification_= null,  experience_= null, rating_= null, views_= null;
                    JSONObject jObjNewConnList= null;
                    JSONArray tempCatListArray = null;

                    tempCatListArray = jObj.getJSONArray("connected_list");
                    if (tempCatListArray.length() != 0) {
                        for (int i = 0; i < tempCatListArray.length(); i++) {

                            searchListArray = tempCatListArray.getString(i);
                            jObjNewConnList = new JSONObject(searchListArray);

                            if (jObjNewConnList.has("id")) {

                                item_id_ = jObjNewConnList.getString("id");
                            }
                            if (jObjNewConnList.has("service_type_name")) {

                                type_name_ = jObjNewConnList.getString("service_type_name");
                            }
                            if (jObjNewConnList.has("service_type")) {

                                type_ = jObjNewConnList.getString("service_type");
                            }
                            if (jObjNewConnList.has("name")) {

                                item_name_ = jObjNewConnList.getString("name");
                            }
                            if (jObjNewConnList.has("thumbnail_url")) {

                                item_thumb_ = jObjNewConnList.getString("thumbnail_url");
                            }
                            if (jObjNewConnList.has("clinic_name")) {

                                clinic_name_ = jObjNewConnList.getString("clinic_name");
                            }
                            if (jObjNewConnList.has("locality_name")) {

                                location_ = jObjNewConnList.getString("locality_name");
                                location  = location_;
                            }
                            if (jObjNewConnList.has("qualifications")) {

                                qualification_ = jObjNewConnList.getString("qualifications");
                            }
                            if (jObjNewConnList.has("years_of_experience")) {

                                experience_ = jObjNewConnList.getString("years_of_experience");
                            }
                            if (jObjNewConnList.has("rating")) {

                                rating_ = jObjNewConnList.getString("rating");
                            }
                            if (jObjNewConnList.has("counter_views")) {

                                views_ = jObjNewConnList.getString("counter_views");
                            }

                            SearchItems tempCatList = new SearchItems(type_name_, type_, item_id_, item_name_, item_thumb_, clinic_name_, location_, qualification_, experience_, rating_, views_);
                            connectedItemsArrayList_.add(tempCatList);
                        }
                    }
                }

                if(data.equals("DOCTOR")) {

                    tabData_en = new String[]{"Clinic", "Feedback", "Gallery"};
                    tabData_ar = new String[]{"عيادة", "التقييم", "معرض الصور"};

                    bookAppointment_ll.setVisibility(View.VISIBLE);
                    parentSpecialityLayout_ll.setVisibility(View.VISIBLE);
                    parentQualificationyLayout_ll.setVisibility(View.VISIBLE);
                    parentFeesLayout_ll.setVisibility(View.VISIBLE);
                    expQualif_tv.setVisibility(View.VISIBLE);
                    visitingDocDesc_tv.setVisibility(View.VISIBLE);
                    visitingDoc_ll.setVisibility(View.VISIBLE);
                    servicesLayout_ll.setVisibility(View.GONE);
                    educationLayout_ll.setVisibility(View.VISIBLE);
                    experienceLayout_ll.setVisibility(View.VISIBLE);

                    if (jObjDetails.has("is_available")) {

                            if (jObjDetails.getBoolean("is_available")) {

                            timeAvailableTop_tv.setVisibility(View.VISIBLE);
                            timeAvailableBottom_tv.setVisibility(View.VISIBLE);
                            titleAvailability_tv.setTextColor(getResources().getColor(R.color.GrassGreen));
                            titleAvailabilityExp_tv.setTextColor(getResources().getColor(R.color.GrassGreen));

                            if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                                titleAvailability_tv.setText(R.string.available_today_en);
                                titleAvailabilityExp_tv.setText(R.string.available_today_en);
                            }
                            else {

                                titleAvailability_tv.setText(R.string.available_today_ar);
                                titleAvailabilityExp_tv.setText(R.string.available_today_ar);
                            }

                        } else {

                            timeAvailableTop_tv.setVisibility(View.INVISIBLE);
                            timeAvailableBottom_tv.setVisibility(View.GONE);
                            titleAvailability_tv.setTextColor(getResources().getColor(R.color.RedPepper));
                            titleAvailabilityExp_tv.setTextColor(getResources().getColor(R.color.RedPepper));

                            if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                                titleAvailability_tv.setText(R.string.not_available_today_en);
                                titleAvailabilityExp_tv.setText(R.string.not_available_today_en);
                            }
                            else {

                                titleAvailability_tv.setText(R.string.not_available_today_ar);
                                titleAvailabilityExp_tv.setText(R.string.not_available_today_ar);
                            }
                        }
                }

                    tabPagerDoctorsAdapter     =   new TabPagerDoctorsAdapter(getSupportFragmentManager(), connectedItemsArrayList_,
                            feedbackItemsArrayList_, galleryItemsArrayList_, itemName, picId, itemId);
                    viewPager.setAdapter(tabPagerDoctorsAdapter);
                    tabLayout.setTabsFromPagerAdapter(tabPagerDoctorsAdapter);
                }
                else if(data.equals("CLINIC")){

                    tabData_en = new String[]{"Doctors", "Feedback", "Gallery", "Map"};
                    tabData_ar = new String[]{"الأطباء", "التقييم", "معرض الصور", "الخريطة"};

                    bookAppointment_ll.setVisibility(View.GONE);
                    parentSpecialityLayout_ll.setVisibility(View.GONE);
                    parentQualificationyLayout_ll.setVisibility(View.GONE);
                    parentFeesLayout_ll.setVisibility(View.GONE);
                    bookAppointment_ll.setVisibility(View.GONE);
                    expQualif_tv.setVisibility(View.GONE);
                    visitingDocDesc_tv.setVisibility(View.GONE);
                    visitingDoc_ll.setVisibility(View.GONE);
                    servicesLayout_ll.setVisibility(View.VISIBLE);
                    educationLayout_ll.setVisibility(View.GONE);
                    experienceLayout_ll.setVisibility(View.GONE);

                    if (jObjDetails.has("is_available")) {

                        if (jObjDetails.getBoolean("is_available")) {

                            timeAvailableTop_tv.setVisibility(View.VISIBLE);
                            titleAvailabilityExp_tv.setVisibility(View.VISIBLE);
                            timeAvailableBottom_tv.setVisibility(View.VISIBLE);
                            titleAvailability_tv.setTextColor(getResources().getColor(R.color.GrassGreen));
                            titleAvailabilityExp_tv.setTextColor(getResources().getColor(R.color.GrassGreen));

                            if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                                titleAvailability_tv.setText(R.string.open_today_en);
                                titleAvailabilityExp_tv.setText(R.string.open_today_en);
                            }
                            else {

                                titleAvailability_tv.setText(R.string.open_today_ar);
                                titleAvailabilityExp_tv.setText(R.string.open_today_ar);
                            }

//                            titleAvailability_tv.setText("Open Today");
//                            titleAvailabilityExp_tv.setText("Open Today");
                        } else {
                            timeAvailableTop_tv.setVisibility(View.INVISIBLE);
                            titleAvailabilityExp_tv.setVisibility(View.INVISIBLE);
                            timeAvailableBottom_tv.setVisibility(View.GONE);
                            titleAvailability_tv.setTextColor(getResources().getColor(R.color.RedPepper));
                            titleAvailabilityExp_tv.setTextColor(getResources().getColor(R.color.RedPepper));
//                            titleAvailability_tv.setText("Closed Today");
//                            titleAvailabilityExp_tv.setText("Closed Today");

                            if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                                titleAvailability_tv.setText(R.string.closed_today_en);
                                titleAvailabilityExp_tv.setText(R.string.closed_today_en);
                            }
                            else {

                                titleAvailability_tv.setText(R.string.closed_today_ar);
                                titleAvailabilityExp_tv.setText(R.string.closed_today_ar);
                            }
                        }
                    }

                    tabPagerClinicsAdapter     =   new TabPagerClinicsAdapter(getSupportFragmentManager(), connectedItemsArrayList_,
                            feedbackItemsArrayList_, galleryItemsArrayList_, itemName, picId, itemId, latitude, longitude);
                    viewPager.setAdapter(tabPagerClinicsAdapter);
                    tabLayout.setTabsFromPagerAdapter(tabPagerClinicsAdapter);
                }
                else {

                    tabData_en = new String[]{"Feedback", "Gallery", "Map"};
                    tabData_ar = new String[]{"التقييم", "معرض الصور", "الخريطة"};

                    bookAppointment_ll.setVisibility(View.GONE);
                    parentSpecialityLayout_ll.setVisibility(View.GONE);
                    parentQualificationyLayout_ll.setVisibility(View.GONE);
                    parentFeesLayout_ll.setVisibility(View.GONE);
                    bookAppointment_ll.setVisibility(View.GONE);
                    expQualif_tv.setVisibility(View.GONE);
                    visitingDocDesc_tv.setVisibility(View.GONE);
                    visitingDoc_ll.setVisibility(View.GONE);
                    servicesLayout_ll.setVisibility(View.VISIBLE);
                    educationLayout_ll.setVisibility(View.GONE);
                    experienceLayout_ll.setVisibility(View.GONE);

                    if (jObjDetails.has("is_available")) {

                            if (jObjDetails.getBoolean("is_available")) {

                            timeAvailableTop_tv.setVisibility(View.VISIBLE);
                            titleAvailabilityExp_tv.setVisibility(View.VISIBLE);
                            timeAvailableBottom_tv.setVisibility(View.VISIBLE);
                            titleAvailability_tv.setTextColor(getResources().getColor(R.color.GrassGreen));
                            titleAvailabilityExp_tv.setTextColor(getResources().getColor(R.color.GrassGreen));

//                            titleAvailability_tv.setText("Open Today");
//                            titleAvailabilityExp_tv.setText("Open Today");

                                if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                                    titleAvailability_tv.setText(R.string.open_today_en);
                                    titleAvailabilityExp_tv.setText(R.string.open_today_en);
                                }
                                else {

                                    titleAvailability_tv.setText(R.string.open_today_ar);
                                    titleAvailabilityExp_tv.setText(R.string.open_today_ar);
                                }

                        } else {

                            timeAvailableTop_tv.setVisibility(View.INVISIBLE);
                            titleAvailabilityExp_tv.setVisibility(View.INVISIBLE);
                            timeAvailableBottom_tv.setVisibility(View.GONE);
                            titleAvailability_tv.setTextColor(getResources().getColor(R.color.RedPepper));
                            titleAvailabilityExp_tv.setTextColor(getResources().getColor(R.color.RedPepper));

//                            titleAvailability_tv.setText("Closed Today");
//                            titleAvailabilityExp_tv.setText("Closed Today");

                                if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                                    titleAvailability_tv.setText(R.string.closed_today_en);
                                    titleAvailabilityExp_tv.setText(R.string.closed_today_en);
                                }
                                else {

                                    titleAvailability_tv.setText(R.string.closed_today_ar);
                                    titleAvailabilityExp_tv.setText(R.string.closed_today_ar);
                                }
                        }
                }

                    tabPagerOthersAdapter     =   new TabPagerOthersAdapter(getSupportFragmentManager(), feedbackItemsArrayList_, galleryItemsArrayList_, itemName, picId, itemId, latitude, longitude);
                    viewPager.setAdapter(tabPagerOthersAdapter);
                    tabLayout.setTabsFromPagerAdapter(tabPagerOthersAdapter);
                }

                tabLayout.post(new Runnable() {
                    @Override
                    public void run() {

                        tabLayout.setupWithViewPager(viewPager);
                    }
                });


                /*setting all texts */
                setTexts();

                /* visiting doc validation*/
                if(jObjDetails.has("is_visiting_doctor")){

                    if(jObjDetails.getBoolean("is_visiting_doctor")){

                        visitingDoc_ll.setVisibility(View.VISIBLE);

                        if(jObjDetails.has("visiting_doctor_data")){
                            String tempData = jObjDetails.getString("visiting_doctor_data");
                            JSONObject jObjNew = new JSONObject(tempData);
                            visitingDocDesc_tv.setText(" From: "+jObjNew.getString("start")+"  To: "+jObjNew.getString("end"));
                        }
                    }
                    else{

                        visitingDoc_ll.setVisibility(View.GONE);
                    }
                }

                /* All Timing array*/
                if(jObjDetails.has("timing")){

                    timing = jObjDetails.getString("timing");
                }

                /* Availabile timing today*/
                if(jObjDetails.has("date_timing")){

                    if(jObjDetails.has("is_available")){

                        timeAvailableTop_tv.setText(jObjDetails.getString("date_timing"));
                        timeAvailableBottom_tv.setText(jObjDetails.getString("date_timing"));
                    }
                    else{
                    }
                }

                /* Views and Rating*/
                if(jObjDetails.has("rating")){

                    rating_tv.setText(jObjDetails.getString("rating"));

                    float ratingStarts = Float.parseFloat(jObjDetails.getString("rating"));
                    ratingBar_rb.setRating(ratingStarts);
                }
                if(jObjDetails.has("counter_views")){

                    views_tv.setText(jObjDetails.getString("counter_views"));
                }

                /* About */
                if(jObjDetails.has("about")){

                    about_tv.setText(jObjDetails.getString("about"));
                }

                 /* Fees */
                if(jObjDetails.has("exam_fee")){

                    fees_tv.setText(jObjDetails.getString("exam_fee")+" KD");
//                    if(jObjDetails.getString("exam_fee").equals("")){
//
//                        fees_tv.setText("Not mentioned, it's payable at clinic");
//                        feesLabel_tv.setVisibility(View.INVISIBLE);
//                    }
//                    else {
//
//                        fees_tv.setText(jObjDetails.getString("exam_fee")+" KD");
//                        feesLabel_tv.setVisibility(View.VISIBLE);
//                    }
                }

                /* Qualifications */
                if(jObjDetails.has("qualifications")){

                    qualification_tv.setText(jObjDetails.getString("qualifications"));

                    if(jObjDetails.has("years_of_experience")){

                        if(UserManager.getUserLanguage(ServiceDetails.this).equals("en")){

                            expQualif_tv.setText(jObjDetails.getString("qualifications")+" - "+jObjDetails.getString("years_of_experience")+" yrs exp");
                        }else{

                            expQualif_tv.setText(jObjDetails.getString("qualifications")+" - "+jObjDetails.getString("years_of_experience")+"  سنوات إكس");
                        }
                    }
                }

                 /* Specialities */
                if(jObjDetails.has("specialities")){

                    specialities_tv.setText(jObjDetails.getString("specialities"));
                }

                /*Education */
                if(jObjDetails.has("education")){

                    education_tv.setText(jObjDetails.getString("education"));
                }

                 /*Experience */
                if(jObjDetails.has("experiences")){

                    experience_tv.setText(jObjDetails.getString("experiences"));
                }
            }

            showDataView();

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*  pager adapter class for clinics in service details activity*/
    class TabPagerClinicsAdapter extends FragmentStatePagerAdapter {

        ArrayList<SearchItems>  newConnectedClinicsItemsArrayList_          = new ArrayList<SearchItems>();
        ArrayList<FeedbackItems> newFeedbackItemsArrayList_                 = new ArrayList<FeedbackItems>();
        ArrayList<GalleryItems> newgalleryItemsList_                        = new ArrayList<GalleryItems>();
        String                  itemName_                                   = "";
        String                  itemImage_                                  = "";
        String                  itemId_                                     = "";
        String                  latitude_                                   = "";
        String                  longitude_                                  = "";


        public TabPagerClinicsAdapter(FragmentManager fm, ArrayList<SearchItems> newConnectedClinicsItemsArrayList,
                                      ArrayList<FeedbackItems> feedbackItemsArrayList, ArrayList<GalleryItems> galleryItemsList, String itemName, String itemImage,
                                     String itemId, String latitude, String longitude){
            super(fm);
            newConnectedClinicsItemsArrayList_  = newConnectedClinicsItemsArrayList;
            newFeedbackItemsArrayList_          = feedbackItemsArrayList;
            newgalleryItemsList_                = galleryItemsList;
            itemName_                           = itemName;
            itemImage_                          = itemImage;
            itemId_                             = itemId;
            latitude_                           = latitude;
            longitude_                          = longitude;
        }
        @Override
        public Fragment getItem(int position) {
            if(position==0) {
                ServiceDetailsDoctorsListTab serviceDetailsDoctorsListTab = ServiceDetailsDoctorsListTab.newInstance(ServiceDetails.this, newConnectedClinicsItemsArrayList_);
                return serviceDetailsDoctorsListTab;
            }
            else if(position==1){
                ServiceDetailsFeedbackTab serviceDetailsFeedbackTab = ServiceDetailsFeedbackTab.newInstance(ServiceDetails.this, newFeedbackItemsArrayList_, itemName_, itemImage_, itemId_);
                return serviceDetailsFeedbackTab;
            }
            else if(position==2){
                ServiceDetailsGalleryTab serviceDetailsGalleryTab = ServiceDetailsGalleryTab.newInstance(ServiceDetails.this, newgalleryItemsList_);
                return serviceDetailsGalleryTab;
            }
            else if(position==3){
                ServiceDetailsMapTab serviceDetailsMapTab = ServiceDetailsMapTab.newInstance(ServiceDetails.this, latitude_, longitude_, itemName_);
                return serviceDetailsMapTab;
            }

            return  null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
//            return  tabData[position];

            if(UserManager.getUserLanguage(ServiceDetails.this).equals("ar")){

                return tabData_ar[position];
            }
            else{

                return  tabData_en[position];
            }
        }
    }

    /*  pager adapter class for doctors in service details activity*/
    class TabPagerDoctorsAdapter extends FragmentStatePagerAdapter {

        ArrayList<SearchItems> newConnectedDoctorsItemsArrayList_       = new ArrayList<SearchItems>();
        ArrayList<FeedbackItems> newFeedbackItemsArrayList_             = new ArrayList<FeedbackItems>();
        ArrayList<GalleryItems> newgalleryItemsList_                    = new ArrayList<GalleryItems>();
        String                  itemName_                               = "";
        String                  itemImage_                              = "";
        String                  itemId_                                 = "";

        public TabPagerDoctorsAdapter(FragmentManager fm, ArrayList<SearchItems> newConnectedDoctorsItemsArrayList,
                                      ArrayList<FeedbackItems> feedbackItemsArrayList, ArrayList<GalleryItems> galleryItemsList, String itemName, String itemImage,
                                      String itemId){
            super(fm);
            newConnectedDoctorsItemsArrayList_  = newConnectedDoctorsItemsArrayList;
            newFeedbackItemsArrayList_          = feedbackItemsArrayList;
            newgalleryItemsList_                = galleryItemsList;
            itemName_                           = itemName;
            itemImage_                          = itemImage;
            itemId_                             = itemId;
        }

        @Override
        public Fragment getItem(int position) {
            if(position==0) {
                ServiceDetailsClinicListTab clinListFragment_1 = ServiceDetailsClinicListTab.newInstance(ServiceDetails.this, newConnectedDoctorsItemsArrayList_);
                return clinListFragment_1;
            }
            else if(position==1){
                ServiceDetailsFeedbackTab clinicsListFragment_2 = ServiceDetailsFeedbackTab.newInstance(ServiceDetails.this, newFeedbackItemsArrayList_, itemName_, itemImage_, itemId_);
                return clinicsListFragment_2;
            }
            else if(position==2){
                ServiceDetailsGalleryTab clinicsListFragment_3 = ServiceDetailsGalleryTab.newInstance(ServiceDetails.this, newgalleryItemsList_);
                return clinicsListFragment_3;
            }

            return  null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if(UserManager.getUserLanguage(ServiceDetails.this).equals("ar")){

                return tabData_ar[position];
            }
            else{

                return  tabData_en[position];
            }
        }
    }

    /*  pager adapter class for doctors in service details activity*/
    class TabPagerOthersAdapter extends FragmentStatePagerAdapter {

        ArrayList<FeedbackItems> newFeedbackItemsArrayList_             = new ArrayList<FeedbackItems>();
        ArrayList<GalleryItems> newgalleryItemsList_                    = new ArrayList<GalleryItems>();
        String                  itemName_                               = "";
        String                  itemImage_                              = "";
        String                  itemId_                                 = "";
        String                  latitude_                               = "";
        String                  longitude_                              = "";

        public TabPagerOthersAdapter(FragmentManager fm, ArrayList<FeedbackItems> feedbackItemsArrayList,
                                     ArrayList<GalleryItems> galleryItemsList, String itemName, String itemImage,
                                     String itemId, String latitude, String longitude){
            super(fm);

            newFeedbackItemsArrayList_ = feedbackItemsArrayList;
            newgalleryItemsList_       = galleryItemsList;
            itemName_                  = itemName;
            itemImage_                 = itemImage;
            itemId_                    = itemId;
            latitude_                  = latitude;
            longitude_                 = longitude;
        }

        @Override
        public Fragment getItem(int position) {

            if(position==0){
                ServiceDetailsFeedbackTab clinicsListFragment_2 = ServiceDetailsFeedbackTab.newInstance(ServiceDetails.this, newFeedbackItemsArrayList_, itemName_, itemImage_, itemId_);
                return clinicsListFragment_2;
            }
            else if(position==1){
                ServiceDetailsGalleryTab serviceDetailsGalleryTab = ServiceDetailsGalleryTab.newInstance(ServiceDetails.this, newgalleryItemsList_);
                return serviceDetailsGalleryTab;
            }
            else if(position==2){
                ServiceDetailsMapTab serviceDetailsMapTab = ServiceDetailsMapTab.newInstance(ServiceDetails.this, latitude_, longitude_, itemName_);
                return serviceDetailsMapTab;
            }

            return  null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if(UserManager.getUserLanguage(ServiceDetails.this).equals("ar")){

                return tabData_ar[position];
            }
            else{

                return  tabData_en[position];
            }
        }
    }

    /* alert dialogue  */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(ServiceDetails.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent myIntent = new Intent(ServiceDetails.this, Login.class);
                        startActivity(myIntent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
