package app.ens.eyadat4u.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class SignUp extends BaseActivity {

    TextView                        signinText_tv, signUpText_tv, fullName_tv, email_tv,
                                    mobile_tv, password_tv, create_tv;
    ImageView                       close_iv;
    RequestQueue                    queue;
    JsonObjectRequest               jsonObjectRequest;
    EditText                        fullName_et, email_et, phone_et, username_et, password_et;
    LinearLayout                    parentLayout_ll, createLayout_ll;
    ProgressDialog                  progressdialog;
    Point                           p;
    Spinner                         countryCode_sp;
    String []                       countryCodeArray, codeTempArray;
    ArrayAdapter<String>            codeArrayAdapter;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    String fullName_, email_, phone_, username_, password_, tempUserId, countryCode;
    boolean is_popupDismissed = false;
    PopupWindow popup;

    boolean is_spinner_selected    = false;
    int     spinnerSelection       = 0;
    String org_OTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        int[] location = new int[2];
        p = new Point();
        p.x = location[0];
        p.y = location[1];

        countryCodeArray = new String[6];
        countryCodeArray[0]= "+ 965 (KWT)";
        countryCodeArray[1]= "+ 971 (ARE)";
        countryCodeArray[2]= "+ 973 (BHR)";
        countryCodeArray[3]= "+ 968 (OMN)";
        countryCodeArray[4]= "+ 974 (QAT)";
        countryCodeArray[5]= "+ 966 (SAU)";

        codeTempArray= new String[6];
        codeTempArray[0]= "965";
        codeTempArray[1]= "971";
        codeTempArray[2]= "973";
        codeTempArray[3]= "968";
        codeTempArray[4]= "974";
        codeTempArray[5]= "966";

        queue    = Volley.newRequestQueue(SignUp.this);

        initViews();

        codeArrayAdapter= new ArrayAdapter<String>(SignUp.this,android.R.layout.simple_spinner_item, countryCodeArray);
        codeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode_sp.setAdapter(codeArrayAdapter);

        countryCode =  codeTempArray[0];

        countryCode_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(is_spinner_selected){
//                    is_spinner_selected= false;
                    String selection = (String) parent.getItemAtPosition(position);
                    int pos = -1;

                    for (int i = 0; i < countryCodeArray.length; i++) {
                        if (countryCodeArray[i].equals(selection)) {

                            pos = i;
                            break;
                        }
                    }
                    spinnerSelection = pos;
                    countryCode =  codeTempArray[spinnerSelection];
                }
                else{
                    is_spinner_selected= true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        signinText_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentSignin = new Intent(SignUp.this, Login.class);
                startActivity(intentSignin);
                finish();
            }
        });

        close_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentSignin = new Intent(SignUp.this, Login.class);
                startActivity(intentSignin);
                finish();
            }
        });

        createLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fullName_   = fullName_et.getText().toString();
                email_      = email_et.getText().toString();
                phone_      = phone_et.getText().toString();
                username_   = username_et.getText().toString();
                password_   = password_et.getText().toString();

                final String email = email_et.getText().toString().trim();

                    if((fullName_ != null && fullName_.equals(""))||
                            (email_ != null && email_.equals("")) ||
                            (phone_ != null && phone_.equals(""))||
                            (password_ != null && password_.equals(""))) {

                        showSnackBar("Please fill all the fields !");
                    }
                    else{

                        if (email.matches(emailPattern))
                        {
                            progressdialog = new ProgressDialog(SignUp.this);
                            progressdialog.setMessage("Creating please wait...");
                            progressdialog.setCancelable(false);
                            progressdialog.show();

                            String obj = createSignUpObj(fullName_, email_, phone_, countryCode, email_, password_);
                            signUpApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_USER_REGISTRATION), obj);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Please enter a valid email address", Toast.LENGTH_SHORT).show();
                        }
                    }
            }
        });
    }

    private void initViews() {

        signinText_tv       = (TextView) findViewById(R.id.signin);
        close_iv            = (ImageView) findViewById(R.id.close);
        fullName_et         = (EditText) findViewById(R.id.fullname);
        email_et            = (EditText) findViewById(R.id.email);
        phone_et            = (EditText) findViewById(R.id.mobile);
        username_et         = (EditText) findViewById(R.id.username);
        password_et         = (EditText) findViewById(R.id.password);
        createLayout_ll     = (LinearLayout) findViewById(R.id.create_layout);
        parentLayout_ll     = (LinearLayout) findViewById(R.id.parent_layout);
        countryCode_sp      = (Spinner) findViewById(R.id.country_code_spinner);
        signUpText_tv       = (TextView) findViewById(R.id.sign_up_text);
        fullName_tv         = (TextView) findViewById(R.id.full_name_text);
        email_tv            = (TextView) findViewById(R.id.email_text);
        mobile_tv           = (TextView) findViewById(R.id.mobile_text);
        password_tv         = (TextView) findViewById(R.id.password_text);
        create_tv           = (TextView) findViewById(R.id.create_text);

        setTexts();
    }

    public void setTexts(){

        if(UserManager.getUserLanguage(SignUp.this).equals("en")){

            fullName_et.setHint(R.string.name_text_en);
            email_et.setHint(R.string.email_text_en);
            phone_et.setHint(R.string.phone_text_en);
            password_et.setHint(R.string.password_en);
            signUpText_tv.setText(R.string.sign_up_en);
            fullName_tv.setText(R.string.sign_up_en);
            email_tv.setText(R.string.email_text_en);
            mobile_tv.setText(R.string.phone_text_en);
            password_tv.setText(R.string.password_en);
            create_tv.setText(R.string.create_en);
        }
        else {

            fullName_et.setHint(R.string.name_text_ar);
            email_et.setHint(R.string.email_text_ar);
            phone_et.setHint(R.string.phone_text_ar);
            password_et.setHint(R.string.password_ar);
            signUpText_tv.setText(R.string.sign_up_ar);
            fullName_tv.setText(R.string.sign_up_ar);
            email_tv.setText(R.string.email_text_ar);
            mobile_tv.setText(R.string.phone_text_ar);
            password_tv.setText(R.string.password_ar);
            create_tv.setText(R.string.create_ar);
        }
    }

    private String createSignUpObj(String fullname, String email, String mobile, String countryCode, String username,
                                        String password) {

        JSONObject mainObj = new JSONObject();
        try {

            if(is_popupDismissed){

                mainObj.put( "user_id", tempUserId);
            }
            else{
                mainObj.put( "user_id", "");
            }

            String language = UserManager.getUserLanguage(SignUp.this);

            mainObj.put( "name_surname", fullname);
            mainObj.put( "email", email);
            mainObj.put( "password", password);
            mainObj.put( "phone", mobile);
            mainObj.put( "country_code", countryCode);
            mainObj.put( "username", username);
            mainObj.put( "device_platform", "android");
            mainObj.put( "language", language);
            mainObj.put( "push_reg_id", "ABC");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String verifyEmailObj(String userId, String otp) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "user_id", userId);
            mainObj.put( "code", otp );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    private String resendOtpObj(String email) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "email", email);
            mainObj.put( "language", UserManager.getUserLanguage(SignUp.this));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void signUpApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(SignUp.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        String userId_          = null;
                        String name_            = null;
                        String userName_        = null;
                        String email_           = null;
                        String phone_           = null;
                        String session_key_     = null;

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            //Validating the api
                            if(jObj.has("status")){

                                if(jObj.getBoolean("status")) {

                                    if(jObj.has("user_id")) {

                                        userId_= jObj.getString("user_id");
                                        tempUserId = userId_;
                                    }
                                    if(jObj.has("name_surname")) {

                                        name_= jObj.getString("name_surname");
                                    }
                                    if(jObj.has("username")) {

                                        userName_= jObj.getString("username");
                                    }
                                    if(jObj.has("email")) {

                                        email_= jObj.getString("email");
                                    }
                                    if(jObj.has("mobile")) {

                                        phone_= jObj.getString("mobile");
                                    }
                                    if(jObj.has("email_otp")) {

                                        org_OTP= jObj.getString("email_otp");
                                    }
                                    if(jObj.has("api_key")) {

                                        session_key_= jObj.getString("api_key");
                                    }
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                    }

//                                    UserManager.updateUserStatus(SignUp.this, userId_,  name_, userName_, email_, phone_, session_key_);

                                    progressdialog.dismiss();

                                    UserManager.updateUserLoginStatus(SignUp.this, "true");
                                    UserManager.updateUserStatus(SignUp.this, userId_,  name_, userName_, email_, phone_, session_key_);

                                    // Send registration complete broadcast message
                                    Intent registrationComplete = new Intent(Constants.Keys.REGISTRATION_COMPLETE);
                                    registrationComplete.putExtra("token", "Broadcast Message");
                                    LocalBroadcastManager.getInstance(SignUp.this).sendBroadcast(registrationComplete);

//                                    popup.dismiss();

                                    finish();
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

//                                    if(jObj.has("mail_verified")) {
//
//                                        if(jObj.getBoolean("mail_verified")) {
//
//
//                                        }
//                                        else{
//
//                                            showEmailVerifyPopup(SignUp.this, p, userId_, email_);
//                                        }
//                                    }

                                } else{

                                    progressdialog.dismiss();
                                    showSnackBar(jObj.getString("message"));
                                }
                            }
                            else{
                                progressdialog.dismiss();
                                if(jObj.has("message")){

                                    showSnackBar(jObj.getString("message"));
                                }
                                else{

                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    public void emailVerifyApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(SignUp.this))
        {
            try {
                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        String userId_          = null;
                        String name_            = null;
                        String userName_        = null;
                        String email_           = null;
                        String phone_           = null;
                        String session_key_     = null;

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            //Validating the api
                            if(jObj.has("status")){

                                if(jObj.getBoolean("status")) {

                                    if(jObj.has("user_id")) {

                                        userId_= jObj.getString("user_id");
                                        tempUserId = userId_;
                                    }

                                    if(jObj.has("name_surname")) {

                                        name_= jObj.getString("name_surname");
                                    }
                                    if(jObj.has("username")) {

                                        userName_= jObj.getString("username");
                                    }
                                    if(jObj.has("email")) {

                                        email_= jObj.getString("email");
                                    }
                                    if(jObj.has("mobile")) {

                                        phone_= jObj.getString("mobile");
                                    }
                                    if(jObj.has("api_key")) {

                                        session_key_= jObj.getString("api_key");
                                    }

                                    progressdialog.dismiss();
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                    }

                            progressdialog.dismiss();

                            UserManager.updateUserLoginStatus(SignUp.this, "true");
                            UserManager.updateUserStatus(SignUp.this, userId_,  name_, userName_, email_, phone_, session_key_);

                            // Send registration complete broadcast message
                            Intent registrationComplete = new Intent(Constants.Keys.REGISTRATION_COMPLETE);
                            registrationComplete.putExtra("token", "Broadcast Message");
                            LocalBroadcastManager.getInstance(SignUp.this).sendBroadcast(registrationComplete);

                            popup.dismiss();

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

                                } else{

                                    progressdialog.dismiss();
                                    showSnackBar(jObj.getString("message"));
                                }
                            }
                            else{

                                if(jObj.has("message")){

                                    showSnackBar(jObj.getString("message"));
                                }
                                else{
                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    public void resendOtpApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(SignUp.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        JSONObject jObj;
                        try {


                            jObj = new JSONObject(result);

                                    progressdialog.dismiss();
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                        Toast.makeText(SignUp.this,""+jObj.getString("message"),Toast.LENGTH_LONG).show();
                                    }
                                    if(jObj.has("email_otp")) {

                                        org_OTP = jObj.getString("email_otp");
                                    }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }


    // The method that displays the popup.
    private void showEmailVerifyPopup(final Activity context, Point p, final String userId, final String email) {

        final int TIME_OUT  = 1500;

        final WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        int popupWidth = width;
        int popupHeight = height;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout                                 = layoutInflater.inflate(R.layout.popup_email_otp, viewGroup);
        TextView welcomeText_tv                     = (TextView) layout.findViewById(R.id.welcome_text);
        ImageView otp_iv                            = (ImageView) layout.findViewById(R.id.otp_img);
        TextView resendOtpToEmail_tv                = (TextView) layout.findViewById(R.id.resend_otp);
        LinearLayout submitOtp_ll                   = (LinearLayout) layout.findViewById(R.id.submit_otp);

        final EditText userOtp_et                   = (EditText) layout.findViewById(R.id.user_otp);
        final TextView userEmail_tv                 = (TextView) layout.findViewById(R.id.email);

        welcomeText_tv.setText("Welcome "+ fullName_);
        userEmail_tv.setText(email_);

        resendOtpToEmail_tv.setPaintFlags(resendOtpToEmail_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        resendOtpToEmail_tv.setText("Resend OTP to your email");

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 10;
        int OFFSET_Y = 10;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                is_popupDismissed = true;

                // Send registration complete broadcast message
                Intent changeOTPEmail = new Intent(Constants.Keys.CHANGE_OTP_EMAIL);
                LocalBroadcastManager.getInstance(SignUp.this).sendBroadcast(changeOTPEmail);
            }
        });

        resendOtpToEmail_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showResendOtpDialogue("Resend OTP", "Do you want to resend OTP to your email id: "+email, email);
            }
        });

        submitOtp_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(( userOtp_et.getText().toString().equals(""))) {

                        Toast.makeText(SignUp.this,"Please enter a valid OTP from email",Toast.LENGTH_LONG).show();
                }
                else{

                    if((userOtp_et.getText().toString().equals(org_OTP))){

                        progressdialog = new ProgressDialog(SignUp.this);
                        progressdialog.setMessage(" please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = verifyEmailObj(userId, userOtp_et.getText().toString());
                        emailVerifyApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_USER_VERIFY_EMAIL), obj);
                    }
                    else{
                        Toast.makeText(SignUp.this,"Please enter a valid OTP from email",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // checking for type intent filter
            if (intent.getAction().equals(Constants.Keys.CHANGE_OTP_EMAIL)) {

                Log.e("Broadcast Reciever", "Registration completed");
                email_et.requestFocus();
//                email_et.setTextColor(getResources().getColor(R.color.ColorPrimary));
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.Keys.CHANGE_OTP_EMAIL));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            showAlertDialogue("Exit ?","Are sure want to exit from Eyadat4u Sign Up");
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }

    /* alert dialogue  */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(SignUp.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

               dialog.cancel();
               LocalBroadcastManager.getInstance(SignUp.this).unregisterReceiver(mRegistrationBroadcastReceiver);
               flushAllNetworkConnections();
               finish();
               overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        })
                .create().show();
    }

    private void showResendOtpDialogue(String title, String msg, final String email) {
        new AlertDialog.Builder(SignUp.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Resend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //calling book appointment API
                        progressdialog = new ProgressDialog(SignUp.this);
                        progressdialog.setMessage("Please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = resendOtpObj(email);
                        resendOtpApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_RESEND_OTP), obj);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        })
                .create().show();
    }
}
