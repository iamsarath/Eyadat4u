package app.ens.eyadat4u.activities;

import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.ChatAdapter;
import app.ens.eyadat4u.items.ChatMessage;
import app.ens.eyadat4u.system.UserManager;

public class OnlinePediatrician extends BaseActivity {

    Toolbar         toolbar;
    TextView        toolbarTitle_tv;
    ProgressBar     progressBar_pb;
    LinearLayout    parentLayout_ll, progressLayout_ll, dataLayout_ll, guestLayout_ll, loginLayout_ll;
    EditText        message_et;
    Button          send_btn;
    ListView        listView_lv;
    Firebase        mRef;
    ChatAdapter     chatAdapter;
    String          chatRoomId ="";
    int id_1 = 0;
    int id_2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_line_pediatrician);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        mRef.setAndroidContext(OnlinePediatrician.this);
        mRef = new Firebase("https://eyadat4uchat.firebaseio.com");
//        https://eyadat4uchat.firebaseio.com/

        initViews();
        setToolbar();
        listView_lv.setAdapter(chatAdapter);

        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendToChannel();
                message_et.setText("");
            }
        });

        loginLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(OnlinePediatrician.this, Login.class);
                startActivity(intent);
                finish();
            }
        });

        try {
            id_1 = Integer.parseInt("8");
            id_2 = Integer.parseInt(UserManager.getUserId(OnlinePediatrician.this));
            int[] numbers = {id_1, id_2};
            int a[]={id_1, id_2};
            Arrays.sort(a);

            chatRoomId = "CHATROOM_"+a[0]+"_"+a[1];

        } catch(NumberFormatException nfe) {
//            System.out.println("Could not parse " + nfe);
        }

        if (UserManager.getUserLoginStatus(OnlinePediatrician.this)){

            showProgressView();
            validateChatRoomUser();
            updateUserNode();

            mRef.child("CHAT").child("CHATROOMS").child(chatRoomId).child("messages").addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                    showDataView();
                }
                public void onCancelled(FirebaseError firebaseError) {

                    finish();
                }
            });

            mRef.child("CHAT").child("CHATROOMS").child(chatRoomId).child("messages").addChildEventListener(new com.firebase.client.ChildEventListener() {
                @Override
                public void onChildAdded(com.firebase.client.DataSnapshot dataSnapshot, String s) {

                    chatAdapter.add(new ChatMessage((String)dataSnapshot.child("to_user_id").getValue(),
                            (String)dataSnapshot.child("text").getValue()
                            ,(String)dataSnapshot.child("added_date").getValue()));
                }

                @Override
                public void onChildChanged(com.firebase.client.DataSnapshot dataSnapshot, String s) {


                }

                @Override
                public void onChildRemoved(com.firebase.client.DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(com.firebase.client.DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
        else{

            showGuestView();
        }

        //to scroll the list view to bottom on data change
        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                listView_lv.setSelection(chatAdapter.getCount() - 1);
            }
        });

    }

    private void updateUserNode() {

        final HashMap<String, String> chatElements = new HashMap<>();
        chatElements.put("email", UserManager.getUserEmail(OnlinePediatrician.this));
        chatElements.put("name", UserManager.getUserName(OnlinePediatrician.this));
        chatElements.put("profile_pic", "http://arworksdemo.com/eyadat4ulatest/files/patient.png");
        chatElements.put("user_id", UserManager.getUserId(OnlinePediatrician.this));
        chatElements.put("username",UserManager.getUserEmail(OnlinePediatrician.this));

        mRef.child("CHAT").child("USERS").child(UserManager.getUserId(OnlinePediatrician.this)).setValue(chatElements);
    }

    private void sendToChannel() {

        final HashMap<String, String> chatElements = new HashMap<>();
        chatElements.put("added_date", getCurrtime());
        chatElements.put("from_user_id", UserManager.getUserId(OnlinePediatrician.this));
        chatElements.put("from_user_status", "2");
        chatElements.put("name", UserManager.getUserName(OnlinePediatrician.this));
        chatElements.put("photoUrl","http://arworksdemo.com/eyadat4ulatest/files/patient.png" );
        chatElements.put("text", message_et.getText().toString());
        chatElements.put("to_user_id","8" );
        chatElements.put("to_user_status","8_1" );

        mRef.child("CHAT").child("CHATROOMS").child(chatRoomId).child("messages").push().setValue(chatElements);
    }

    private void validateChatRoomUser(){

        //Checking for user existance
        mRef.child("CHAT").child("USERS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if(snapshot.child(UserManager.getUserId(OnlinePediatrician.this)).getValue() == null){

                    updateChatRoomUsersNode();
                }
                else{

                }
            }
            @Override public void onCancelled(FirebaseError error) { }
        });
    }

    private void updateChatRoomUsersNode() {

        final HashMap<String, String> chatElements = new HashMap<>();

        String [] userArray = new String[2];
        userArray[0] = "8";
        userArray[1] = UserManager.getUserId(OnlinePediatrician.this);

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("chat_type", "1");
        map.put("chat_users", userArray ); // array of ints is an object
        map.put("chatroom", chatRoomId);

        mRef.child("CHAT").child("CHATROOM_USER").child(UserManager.getUserId(OnlinePediatrician.this)).push().setValue(map);
        mRef.child("CHAT").child("CHATROOM_USER").child("8").push().setValue(map);
    }

    private String getCurrtime(){

        String time = new SimpleDateFormat("hh:mm a").format(Calendar.getInstance().getTime());

        Calendar now = Calendar.getInstance();
        int month = now.get(Calendar.MONTH)+1;
        int year = now.get(Calendar.YEAR);
        int date_ =  now.get(Calendar.DATE);

        getDate(year);

        return getDate(month)+" "+date_+", "+time;
    }

    public static String getDate(int token){

        String date      = "";
        switch (token) {

            case 1:
                date = "Jan";
                break;

            case 2:
                date = "Feb";
                break;

            case 3:
                date = "Mar";
                break;

            case 4:
                date = "Apr";
                break;

            case 5:
                date = "May";
                break;

            case 6:
                date = "Jun";
                break;

            case 7:
                date = "Jul";
                break;

            case 8:
                date = "Aug";
                break;

            case 9:
                date = "Sep";
                break;

            case 10:
                date = "Oct";
                break;

            case 11:
                date = "Nov";
                break;

            case 12:
                date = "Dec";
                break;

        }
        return date;
    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        progressLayout_ll   = (LinearLayout) findViewById(R.id.progress_view);
        dataLayout_ll       = (LinearLayout) findViewById(R.id.data_view);
        parentLayout_ll     = (LinearLayout) findViewById(R.id.parent_layout);
        guestLayout_ll      = (LinearLayout) findViewById(R.id.quest_view);
        loginLayout_ll      = (LinearLayout) findViewById(R.id.login_layout);
        progressBar_pb      = (ProgressBar) findViewById(R.id.progressBar);
        listView_lv         = (ListView) findViewById(R.id.listView);
        message_et          = (EditText) findViewById(R.id.message);
        send_btn            = (Button) findViewById(R.id.send);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);

        listView_lv.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chatAdapter = new ChatAdapter(getApplicationContext(), R.layout.right);
    }

    private void showDataView() {

        dataLayout_ll.setVisibility(View.VISIBLE);
        progressLayout_ll.setVisibility(View.GONE);
        guestLayout_ll.setVisibility(View.GONE);
    }

    private void showProgressView() {

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
        guestLayout_ll.setVisibility(View.GONE);
    }

    private void showGuestView() {

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.GONE);
        guestLayout_ll.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(OnlinePediatrician.this).equals("en")){

            toolbarTitle_tv.setText(R.string.menu_online_pediatricians_en);
            message_et.setHint(R.string.chat_with_online_docs_en);
        }else{

            toolbarTitle_tv.setText(R.string.menu_online_pediatricians_ar);
            message_et.setHint(R.string.chat_with_online_docs_ar);
        }
    }
}
