package app.ens.eyadat4u.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.AvailableTimeSlotExpAdapter;

public class AvailableTimeSlots extends BaseActivity implements ExpandableListView.OnGroupExpandListener {

    Toolbar                         toolbar;
    TextView                        toolbarTitle_tv, doctorName_tv, location_tv;
    AvailableTimeSlotExpAdapter     availableTimeSlotExpAdapter;
    ExpandableListView              expListView_elv;
    List<String>                    listDaysHeader;
    HashMap<String, List<String>>   listTimeSlotChild;
    String                          timingArray, itemName, location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_time_slots);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        timingArray = getIntent().getExtras().getString("TIMING_ARRAY");
        itemName    = getIntent().getExtras().getString("ITEM_NAME");
        location    = getIntent().getExtras().getString("LOCATION");

        if(timingArray != null){

        JSONArray timingArrayList = null;
        try {
            timingArrayList = new JSONArray(timingArray);
            parseDaysList(timingArrayList);
            parseTimeSlotList(timingArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        }

        initViews();
        setToolbar();

        doctorName_tv.setText(itemName);
        location_tv.setText(location);

        expListView_elv.setOnGroupExpandListener(AvailableTimeSlots.this);
        availableTimeSlotExpAdapter = new AvailableTimeSlotExpAdapter(this, listDaysHeader, listTimeSlotChild);

        // setting list adapter
        expListView_elv.setAdapter(availableTimeSlotExpAdapter);

        int count = availableTimeSlotExpAdapter.getGroupCount();
        for ( int i = 0; i < count; i++ ) {

            expListView_elv.expandGroup(i);
        }
    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        expListView_elv     = (ExpandableListView) findViewById(R.id.lvExp);
        doctorName_tv       = (TextView) findViewById(R.id.item_name);
        location_tv         = (TextView) findViewById(R.id.location);
//        timeSlotList_lv     = (ListView) findViewById(R.id.available_time_slot_list);

    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText("All Timings");

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

    //method for parsing sub category list data
    void parseDaysList(JSONArray timeArray){

        listDaysHeader = new ArrayList<String>();
        listDaysHeader.clear();
        try {

            String timingListArray, days_ = null;
            JSONObject jObjNewSubCatList= null;

            if (timeArray.length() != 0) {
                for (int i = 0; i < timeArray.length(); i++) {

                    timingListArray = timeArray.getString(i);
                    jObjNewSubCatList = new JSONObject(timingListArray);

                    if (jObjNewSubCatList.has("days")) {

                        days_ = jObjNewSubCatList.getString("days");
                    }
                    listDaysHeader.add(days_);
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /* Parse  expandable listview time slot child */
    void parseTimeSlotList(JSONArray timingSlotArrayList) throws JSONException {

        listTimeSlotChild = new HashMap<String, List<String>>();
        listTimeSlotChild.clear();

        JSONObject  jsonObjectMain, jsonObjectInner;
        JSONArray   jsonObjectInnerTimeSlot = null;
        String      time_ = "",timeSlot_ = "";

            for (int j = 0; j < timingSlotArrayList.length(); j++) {
                String timeSlot = timingSlotArrayList.getString(j);
                jsonObjectMain = new JSONObject(timeSlot);

                if (jsonObjectMain.has("timing")) {
                    time_ = jsonObjectMain.getString("timing");
                    jsonObjectInnerTimeSlot = new JSONArray(time_);
                    List<String> childTimingArray = new ArrayList<String>();

                    for (int h = 0; h < jsonObjectInnerTimeSlot.length(); h++) {
                        String inner = jsonObjectInnerTimeSlot.getString(h);
                        jsonObjectInner = new JSONObject(inner);

                        if (jsonObjectInner.has("time_slot")) {

                            timeSlot_ = jsonObjectInner.getString("time_slot");
                            childTimingArray.add(timeSlot_);
                        }
                    }
                    putToHashMap(listDaysHeader.get(j), childTimingArray);
                }
            }
    }

    void putToHashMap(String headerText, List<String> childTimingArray){

        List<String> newChildTimingArray    = new ArrayList<String>();
        newChildTimingArray                 = childTimingArray;
        listTimeSlotChild.put(headerText, newChildTimingArray);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }

    @Override
    public void onGroupExpand(int groupPosition) {

    }
}
