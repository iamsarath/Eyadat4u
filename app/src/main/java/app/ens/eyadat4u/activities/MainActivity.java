package app.ens.eyadat4u.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.BannerPagerAdapter;
import app.ens.eyadat4u.adapters.BestDoctorsAdapter;
import app.ens.eyadat4u.fragments.ClinicsFragment;
import app.ens.eyadat4u.fragments.FitnessFragment;
import app.ens.eyadat4u.fragments.LabsFragment;
import app.ens.eyadat4u.fragments.LaserClinicFragment;
import app.ens.eyadat4u.fragments.PharmaciesFragment;
import app.ens.eyadat4u.fragments.SpaFragment;
import app.ens.eyadat4u.items.BannerItems;
import app.ens.eyadat4u.items.MainCategoryItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;
import app.ens.eyadat4u.views.HorizontalListView;
import me.relex.circleindicator.CircleIndicator;
//import app.ens.eyadat4u.views.CircleIndicator;

public class MainActivity extends BaseActivity {

    NavigationView          navigationView;
    DrawerLayout            drawerLayout;
    Handler                 mHandler;
    Toolbar                 toolbar;
    TextView                toolbarTitle_tv, name_tv, userStatus_tv;
    MenuItem                activeMenuItem;
    MenuItem                menuRightPromos_mn, menuRightNewsEvents_mn;
    ActionBarDrawerToggle   actionBarDrawerToggle;
    Menu                    menu;
    MenuItem                menuHome_mn, menuMyAccount_mn, menuPromos_mn, menuOnlinePediatrician_mn, menuVisitingDoc_mn, menuBuyInsurance_mn,
                            menuMedicalServices_mn, menuMedicalJobs_mn, menuNewsEvents_mn, menuSignIn_mn, menuSignOut_mn;

    private TabPagerAdapter tabPagerAdapter;
    private ViewPager       viewPager;
    private TabLayout       tabLayout;
    HorizontalListView      bestDoctors_hlv;
    Spinner                 languageSpinner_sp;
    LinearLayout            parentLayout_ll, search_ll, signinSignout_ll;
    LinearLayout            progressLayout_ll, dataLayout_ll, bestDocsLayout_ll;
    String                  tabData[]        = {"Specialized Clinics","Laser Clinics","Diagnostics Labs","Salons","Fitness Centers","Pharmacies"};
    String                  tabData_ar[]     = {"العيادات التخصصية","عيادات الليزر","المختبرات","الصالونات","مراكز اللياقة البدنية","الصيدليات"};
    int                     spinnerCurSelection, lanSpinnerInitPosition;
    BestDoctorsAdapter      bestDoctorsAdapter;
    ImageView               userImage_iv, chatOnlineDocs_ll;
    RequestQueue            queue;
    JsonObjectRequest       jsonObjectRequest;
    ProgressBar             progressBar_pb;
    ViewPager               bannerViewPager_vp;
    BannerPagerAdapter      bannerPagerAdapter;
    CircleIndicator         indicator;
    TextView                bestDocs_tv, searchText_tv;

    ArrayList<SearchItems>  bestDocsArrayList       = new ArrayList<SearchItems>();
    ArrayList<BannerItems>  bannerItemsArrayList    = new ArrayList<BannerItems>();

    //Declarations for banner auto-scroll
    private Handler handler;
    private int delay = 3500; //milliseconds
    private int page = 0;
    Runnable runnable = new Runnable() {
        public void run() {
            if (bannerPagerAdapter.getCount() == page) {
                page = 0;
            } else {
                page++;
            }
            bannerViewPager_vp.setCurrentItem(page, true);
            handler.postDelayed(this, delay);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue   = Volley.newRequestQueue(MainActivity.this);
        handler = new Handler();
        initViews();
        setToolbar();

        mHandler       = new Handler();
        navigationView.getMenu().getItem(0).setChecked(true);
        activeMenuItem =   navigationView.getMenu().getItem(0);

        bannerPagerAdapter  =   new BannerPagerAdapter(MainActivity.this, bannerItemsArrayList);
        bestDoctorsAdapter  =   new BestDoctorsAdapter(MainActivity.this, bestDocsArrayList);
        tabPagerAdapter     =   new TabPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabPagerAdapter);
        tabLayout.setTabsFromPagerAdapter(tabPagerAdapter);
        bestDoctors_hlv.setAdapter(bestDoctorsAdapter);
        setTexts();

        bannerViewPager_vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
//                pageIndicator_pi.setActiveDot(pos);
                page = pos;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        search_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Search.class);
                startActivity(intent);
            }
        });

        chatOnlineDocs_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, OnlinePediatrician.class);
                startActivity(intent);
            }
        });

        tabLayout.post(new Runnable() {
            @Override
            public void run() {

                tabLayout.setupWithViewPager(viewPager);
                for (int i = 0; i < tabLayout.getTabCount(); i++) {

                    if (i == 0) {
                        tabLayout.getTabAt(i).setIcon(R.drawable.ic_clinics_active);
                    }
                    else if (i == 1) {
                        tabLayout.getTabAt(i).setIcon(R.drawable.ic_laser_clinic_normal);
                    }
                    else if (i == 2) {
                        tabLayout.getTabAt(i).setIcon(R.drawable.ic_diagnostics_labs_normal);

                    }
                    else if (i == 3) {
                        tabLayout.getTabAt(i).setIcon(R.drawable.ic_spas_and_saloon_normal);

                    }
                    else if (i == 4) {
                        tabLayout.getTabAt(i).setIcon(R.drawable.ic_fitness_normal);

                    }
                    else if (i == 5) {
                        tabLayout.getTabAt(i).setIcon(R.drawable.ic_pharmacy_normal);

                    }
                }
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {

                if (position == 0) {

                    setTabSelector(position);
                }
                else if (position == 1) {

                    setTabSelector(position);
                }
                else if (position == 2) {

                    setTabSelector(position);
                }
                else if (position == 3) {

                    setTabSelector(position);
                }
                else if (position == 4) {

                    setTabSelector(position);
                }
                else if (position == 5) {

                    setTabSelector(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        loadFragment(R.id.content_frame, homeFragment, "HomeFragment");

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                navigationView.getMenu().getItem(0).setChecked(false);

                if (activeMenuItem != null)
                    activeMenuItem.setChecked(false);

                activeMenuItem = menuItem;
                menuItem.setChecked(true);

                mHandler.postDelayed(closeDrawerRunnable, 200);

                switch (menuItem.getItemId()) {

                    case R.id.home:
//                        toolbarTitle_tv.setText(R.string.app_title);
//                        loadFragment(R.id.content_frame, homeFragment, "HomeFragment");
                        return true;

                    case R.id.my_account:
                        Intent intentAppointments = new Intent(MainActivity.this, MyAccount.class);
                        startActivity(intentAppointments);
                        return true;

                    case R.id.promotions:
                        Intent intentPromos = new Intent(MainActivity.this, PromotionsList.class);
                        startActivity(intentPromos);
                        return true;

                    case R.id.online_pediatricians:
                        Intent intentOnlinePediatricians = new Intent(MainActivity.this, OnlinePediatrician.class);
                        startActivity(intentOnlinePediatricians);
                        return true;

                    case R.id.visiting_doctors:
                        Intent intent = new Intent(MainActivity.this, ServiceList.class);
                        intent.putExtra("TITLE", "Visiting Doctors");
                        intent.putExtra("SUB_TITLE", "Visiting Doctors");
                        intent.putExtra("SERVICE_ID", "0");
                        intent.putExtra("SERVICE_TYPE", "");
                        intent.putExtra("CUSTOM_SEARCH", "");
                        intent.putExtra("VISITING_DOCTOR", true);
                        startActivity(intent);
                        return true;

                    case R.id.buy_insurance:
                        Intent intentInsurance = new Intent(MainActivity.this, InsuranceList.class);
                        startActivity(intentInsurance);
                        return true;

                    case R.id.medical_services:
                        Intent intentMedServices = new Intent(MainActivity.this, MedicalServices.class);
                        startActivity(intentMedServices);
                        return true;

                    case R.id.medical_jobs:
                        Intent intentMedJobs = new Intent(MainActivity.this, MedicalJobs.class);
                        startActivity(intentMedJobs);
                        return true;

                    case R.id.menu_news_and_events:
                        Intent intentNewsEvents = new Intent(MainActivity.this, NewsAndEventsList.class);
                        startActivity(intentNewsEvents);
                        return true;

                    case R.id.menu_sign_in:
                        Intent intentSignin = new Intent(MainActivity.this, Login.class);
                        startActivity(intentSignin);

                        setDrawerHeader();
                        manageDrawerMenu();
                        actionBarDrawerToggle.syncState();
                        mHandler.postDelayed(closeDrawerRunnable, 100);

                        return true;

                    case R.id.menu_signout:
                        Intent intentSignout = new Intent(MainActivity.this, Login.class);
                        startActivity(intentSignout);
                        UserManager.updateUserLoginStatus(MainActivity.this, "false");
                        UserManager.updateUserStatus(MainActivity.this, "",  "", "", "", "", "");

                        setDrawerHeader();
                        manageDrawerMenu();
                        actionBarDrawerToggle.syncState();
                        mHandler.postDelayed(closeDrawerRunnable, 200);
                        return true;
                }
                return false;
            }
        });

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        manageDrawerMenu();

        signinSignout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHandler.postDelayed(closeDrawerRunnable, 100);
                if(UserManager.getUserLoginStatus(MainActivity.this)){

                    Intent intentSignout = new Intent(MainActivity.this, Login.class);
                    startActivity(intentSignout);
                    UserManager.updateUserLoginStatus(MainActivity.this, "false");
                }
                else{

                    Intent intentSignout = new Intent(MainActivity.this, Login.class);
                    startActivity(intentSignout);
                }

                setDrawerHeader();
                manageDrawerMenu();
                actionBarDrawerToggle.syncState();
            }
        });

        userImage_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHandler.postDelayed(closeDrawerRunnable, 200);
                if(UserManager.getUserLoginStatus(MainActivity.this)){

                    Intent intentAppointments = new Intent(MainActivity.this, MyAccount.class);
                    startActivity(intentAppointments);
                }
                else{

//                    Toast.makeText(MainActivity.this, "My Account- Signed Out", Toast.LENGTH_LONG).show();
                }

            }
        });

        languageSpinner_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                mHandler.postDelayed(closeDrawerRunnable, 100);
                if (spinnerCurSelection != position){

                    if (position==0){

                        UserManager.updateUserLanguage(MainActivity.this, "en");
                    }
                    else if (position==1){

                        UserManager.updateUserLanguage(MainActivity.this, "ar");
                    }
                }
                spinnerCurSelection= position;

                //calling initData API
                showProgressView();
                String obj = initDataObj(UserManager.getUserLanguage(MainActivity.this));
                initDataApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_INIT_DATA), obj);
            }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
            // Your code here
        }
    });

        bestDoctors_hlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, ServiceDetails.class);
                intent.putExtra("KEY_TYPE", "DOCTOR");
                intent.putExtra("ITEM_NAME", bestDocsArrayList.get(position).item_name_);
                intent.putExtra("ITEM_PIC", bestDocsArrayList.get(position).item_thumb_);
                intent.putExtra("ITEM_ID", bestDocsArrayList.get(position).item_id_);
                startActivity(intent);
            }
        });
    }

    private void initViews() {

        toolbar                     = (Toolbar) findViewById(R.id.tool_bar);
        navigationView              = (NavigationView) findViewById(R.id.navigation_view);
        drawerLayout                = (DrawerLayout) findViewById(R.id.drawer);
        toolbarTitle_tv             = (TextView) findViewById(R.id.toolbar_title);
        progressLayout_ll           = (LinearLayout) findViewById(R.id.progress_view);
        dataLayout_ll               = (LinearLayout) findViewById(R.id.data_view);

        View headerLayout           = navigationView.getHeaderView(0);
        menu                        = navigationView.getMenu();
        menuHome_mn                 = menu.findItem(R.id.home);
        menuMyAccount_mn            = menu.findItem(R.id.my_account);
        menuPromos_mn               = menu.findItem(R.id.promotions);
        menuOnlinePediatrician_mn   = menu.findItem(R.id.online_pediatricians);
        menuVisitingDoc_mn          = menu.findItem(R.id.visiting_doctors);
        menuBuyInsurance_mn         = menu.findItem(R.id.buy_insurance);
        menuMedicalServices_mn      = menu.findItem(R.id.medical_services);
        menuMedicalJobs_mn          = menu.findItem(R.id.medical_jobs);
        menuNewsEvents_mn           = menu.findItem(R.id.menu_news_and_events);
        menuSignIn_mn               = menu.findItem(R.id.menu_sign_in);
        menuSignOut_mn              = menu.findItem(R.id.menu_signout);

        languageSpinner_sp          = (Spinner) headerLayout.findViewById(R.id.language_spinner);
        name_tv                     = (TextView) headerLayout.findViewById(R.id.name);
        userStatus_tv               = (TextView) headerLayout.findViewById(R.id.user_status);
        signinSignout_ll            = (LinearLayout) headerLayout.findViewById(R.id.signin_layout);
        userImage_iv                = (ImageView) headerLayout.findViewById(R.id.user_image);
        viewPager                   = (ViewPager) findViewById(R.id.viewpager);
        tabLayout                   = (TabLayout) findViewById(R.id.detail_tabs);
        bestDoctors_hlv             = (HorizontalListView) findViewById(R.id.doctors_list);
        parentLayout_ll             = (LinearLayout) findViewById(R.id.content_frame);
        bestDocsLayout_ll           = (LinearLayout) findViewById(R.id.best_doc_layout);
        progressBar_pb              = (ProgressBar) findViewById(R.id.progressBar);
        search_ll                   = (LinearLayout) findViewById(R.id.search);
        chatOnlineDocs_ll           = (ImageView) findViewById(R.id.chat_online_doctors);
        indicator                   = (CircleIndicator) findViewById(R.id.indicator);
        bestDocs_tv                 = (TextView) findViewById(R.id.best_docs);
        searchText_tv               = (TextView) findViewById(R.id.search_text);
        bannerViewPager_vp          = (ViewPager)  findViewById(R.id.banner_view_pager);
//        pageIndicator_pi    = (PageIndicator)findViewById(R.id.page_indicator);

        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);

        viewPager.setOffscreenPageLimit(5);
        setLanguageSpinner();
        setDrawerHeader();
        languageSpinner_sp.setSelection(lanSpinnerInitPosition);
        spinnerCurSelection = languageSpinner_sp.getSelectedItemPosition();

//        setBestDocs();
    }

    public void setMenuTexts_en(){

        menuHome_mn.setTitle(R.string.menu_home_en);
        menuMyAccount_mn.setTitle(R.string.menu_my_acc_en);
        menuPromos_mn.setTitle(R.string.menu_promotions_en);
        menuOnlinePediatrician_mn.setTitle(R.string.menu_online_pediatricians_en);
        menuVisitingDoc_mn.setTitle(R.string.menu_visiting_doc_en);
        menuBuyInsurance_mn.setTitle(R.string.menu_buy_insurance_en);
        menuMedicalServices_mn.setTitle(R.string.menu_med_services_en);
        menuMedicalJobs_mn.setTitle(R.string.menu_med_jobs_en);
        menuNewsEvents_mn.setTitle(R.string.menu_news_and_events_en);
        menuSignIn_mn.setTitle(R.string.menu_sign_in_en);
        menuSignOut_mn.setTitle(R.string.menu_sign_out_en);
    }

    public void setMenuTexts_ar(){

        menuHome_mn.setTitle(R.string.menu_home_ar);
        menuMyAccount_mn.setTitle(R.string.menu_my_acc_ar);
        menuPromos_mn.setTitle(R.string.menu_promotions_ar);
        menuOnlinePediatrician_mn.setTitle(R.string.menu_online_pediatricians_ar);
        menuVisitingDoc_mn.setTitle(R.string.menu_visiting_doc_ar);
        menuBuyInsurance_mn.setTitle(R.string.menu_buy_insurance_ar);
        menuMedicalServices_mn.setTitle(R.string.menu_med_services_ar);
        menuMedicalJobs_mn.setTitle(R.string.menu_med_jobs_ar);
        menuNewsEvents_mn.setTitle(R.string.menu_news_and_events_ar);
        menuSignIn_mn.setTitle(R.string.menu_sign_in_ar);
        menuSignOut_mn.setTitle(R.string.menu_sign_out_ar);
    }

    public void setTexts(){

        if(UserManager.getUserLanguage(MainActivity.this).equals("en")){

            bestDocs_tv.setText(R.string.best_doc_en);
            searchText_tv.setText(R.string.home_search_text_en);
            setMenuTexts_en();
        }
        else {

            bestDocs_tv.setText(R.string.best_doc_ar);
            searchText_tv.setText(R.string.home_search_text_ar);
            setMenuTexts_ar();
        }
    }

    private void setLanguageSpinner() {

        if(UserManager.getUserLanguage(MainActivity.this).equals("en")){

            lanSpinnerInitPosition = 0;
        }
        else{

            lanSpinnerInitPosition = 1;
        }
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        bestDocsLayout_ll.setVisibility(View.VISIBLE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        bestDocsLayout_ll.setVisibility(View.GONE);
    }

    private String initDataObj(String language) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void initDataApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(MainActivity.this))
        {
            try {

                showSnackBar("Please check your internet connection");
                showDataView();

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String result = response.toString();
                        parseCatList(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                            showDataView();
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseCatList(String response){

        JSONObject jObj;

        try {

            jObj = new JSONObject(response);

            JSONObject jObjResult= null;
            JSONObject jObjAllItems= null;
            String result, allItems;
            result= jObj.getString("result");
            jObjResult= new JSONObject(result);
            allItems = jObjResult.getString("all_items");
            jObjAllItems= new JSONObject(allItems);

            /*clinics cat data array variables*/
            if(jObjAllItems.has("CLINIC")){
                Constants.clinicCategoryItemsArrayList.clear();

                /* clinic list array variables */
                String clinicCatListArray, service_id_ = null, service_name_ = null, icon_ = null, custom_search_ = "", service_type_="";
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjAllItems.getJSONArray("CLINIC");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        clinicCatListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(clinicCatListArray);

                        if (jObjNewCatList.has("service_id")) {

                            service_id_ = jObjNewCatList.getString("service_id");
                        }
                        if (jObjNewCatList.has("service_name")) {

                            service_name_ = jObjNewCatList.getString("service_name");
                        }
                        if (jObjNewCatList.has("icon")) {

                            icon_ = jObjNewCatList.getString("icon");
                        }
                        if (jObjNewCatList.has("custom_search")) {

                            custom_search_ = jObjNewCatList.getString("custom_search");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            service_type_ = jObjNewCatList.getString("service_type");
                        }

                        MainCategoryItems tempCatList = new MainCategoryItems(service_id_, service_name_, icon_, service_type_, custom_search_);
                        Constants.clinicCategoryItemsArrayList.add(tempCatList);
                    }
                }

            }

            /*laser clinics cat data array variables*/
            if(jObjAllItems.has("LASERCLINIC")){
                Constants.laserclinicCategoryItemsArrayList.clear();

                /* clinic list array variables */
                String laseClinicCatListArray, service_id_ = null, service_name_ = null, icon_ = null, custom_search_ = "", service_type_ = "";
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjAllItems.getJSONArray("LASERCLINIC");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        laseClinicCatListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(laseClinicCatListArray);

                        if (jObjNewCatList.has("service_id")) {

                            service_id_ = jObjNewCatList.getString("service_id");
                        }
                        if (jObjNewCatList.has("service_name")) {

                            service_name_ = jObjNewCatList.getString("service_name");
                        }
                        if (jObjNewCatList.has("icon")) {

                            icon_ = jObjNewCatList.getString("icon");
                        }
                        if (jObjNewCatList.has("custom_search")) {

                            custom_search_ = jObjNewCatList.getString("custom_search");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            service_type_ = jObjNewCatList.getString("service_type");
                        }

                        MainCategoryItems tempCatList = new MainCategoryItems(service_id_, service_name_, icon_, service_type_, custom_search_);
                        Constants.laserclinicCategoryItemsArrayList.add(tempCatList);
                    }
                }
            }

            /*diagnostic labs cat data array variables*/
            if(jObjAllItems.has("LABS")){
                Constants.diagnosticLabsCategoryItemsArrayList.clear();

                /* clinic list array variables */
                String digLabsCatListArray, service_id_ = null, service_name_ = null, icon_ = null, custom_search_ = "", service_type_ = "";
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjAllItems.getJSONArray("LABS");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        digLabsCatListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(digLabsCatListArray);

                        if (jObjNewCatList.has("service_id")) {

                            service_id_ = jObjNewCatList.getString("service_id");
                        }
                        if (jObjNewCatList.has("service_name")) {

                            service_name_ = jObjNewCatList.getString("service_name");
                        }
                        if (jObjNewCatList.has("icon")) {

                            icon_ = jObjNewCatList.getString("icon");
                        }
                        if (jObjNewCatList.has("custom_search")) {

                            custom_search_ = jObjNewCatList.getString("custom_search");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            service_type_ = jObjNewCatList.getString("service_type");
                        }

                        MainCategoryItems tempCatList = new MainCategoryItems(service_id_, service_name_, icon_, service_type_, custom_search_);
                        Constants.diagnosticLabsCategoryItemsArrayList.add(tempCatList);
                    }
                }

            }

            /*Spa & Salon cat data array variables*/
            if(jObjAllItems.has("SPA")){
                Constants.spasSalonsCategoryItemsArrayList.clear();

                /* clinic list array variables */
                String spaSalonCatListArray, service_id_ = null, service_name_ = null, icon_ = null, custom_search_ = "", service_type_ = "";
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjAllItems.getJSONArray("SPA");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        spaSalonCatListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(spaSalonCatListArray);

                        if (jObjNewCatList.has("service_id")) {

                            service_id_ = jObjNewCatList.getString("service_id");
                        }
                        if (jObjNewCatList.has("service_name")) {

                            service_name_ = jObjNewCatList.getString("service_name");
                        }
                        if (jObjNewCatList.has("icon")) {

                            icon_ = jObjNewCatList.getString("icon");
                        }
                        if (jObjNewCatList.has("custom_search")) {

                            custom_search_ = jObjNewCatList.getString("custom_search");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            service_type_ = jObjNewCatList.getString("service_type");
                        }

                        MainCategoryItems tempCatList = new MainCategoryItems(service_id_, service_name_, icon_, service_type_, custom_search_);
                        Constants.spasSalonsCategoryItemsArrayList.add(tempCatList);
                    }
                }

            }

            /*Fitness cat data array variables*/
            if(jObjAllItems.has("FITNESS")){
                Constants.fitnessCategoryItemsArrayList.clear();

                /* clinic list array variables */
                String fitnessCatListArray, service_id_ = null, service_name_ = null, icon_ = null, custom_search_ = "", service_type_ = "";
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjAllItems.getJSONArray("FITNESS");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        fitnessCatListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(fitnessCatListArray);

                        if (jObjNewCatList.has("service_id")) {

                            service_id_ = jObjNewCatList.getString("service_id");
                        }
                        if (jObjNewCatList.has("service_name")) {

                            service_name_ = jObjNewCatList.getString("service_name");
                        }
                        if (jObjNewCatList.has("icon")) {

                            icon_ = jObjNewCatList.getString("icon");
                        }
                        if (jObjNewCatList.has("custom_search")) {

                            custom_search_ = jObjNewCatList.getString("custom_search");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            service_type_ = jObjNewCatList.getString("service_type");
                        }

                        MainCategoryItems tempCatList = new MainCategoryItems(service_id_, service_name_, icon_, service_type_, custom_search_);
                        Constants.fitnessCategoryItemsArrayList.add(tempCatList);
                    }
                }

            }

            /*Fitness cat data array variables*/
            if(jObjAllItems.has("PHARMACY")){
                Constants.pharmaciesCategoryItemsArrayList.clear();

                /* clinic list array variables */
                String pharmacyCatListArray, service_id_ = null, service_name_ = null, icon_ = null, custom_search_ = "", service_type_ = "";
                JSONObject jObjNewCatList= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObjAllItems.getJSONArray("PHARMACY");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        pharmacyCatListArray = tempCatListArray.getString(i);
                        jObjNewCatList = new JSONObject(pharmacyCatListArray);

                        if (jObjNewCatList.has("service_id")) {

                            service_id_ = jObjNewCatList.getString("service_id");
                        }
                        if (jObjNewCatList.has("service_name")) {

                            service_name_ = jObjNewCatList.getString("service_name");
                        }
                        if (jObjNewCatList.has("icon")) {

                            icon_ = jObjNewCatList.getString("icon");
                        }
                        if (jObjNewCatList.has("custom_search")) {

                            custom_search_ = jObjNewCatList.getString("custom_search");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            service_type_ = jObjNewCatList.getString("service_type");
                        }

                        MainCategoryItems tempCatList = new MainCategoryItems(service_id_, service_name_, icon_, service_type_, custom_search_);
                        Constants.pharmaciesCategoryItemsArrayList.add(tempCatList);
                    }
                }
            }

            /*Best docs array variables*/
            if(jObjResult.has("best_doctors")){

                /* best doctors array variables */
                bestDocsArrayList.clear();
                String searchListArray, type_name_ = null, type_= null, item_id_= null, item_name_= null, item_thumb_= null, clinic_name_= null, location_= null,
                        qualification_= null,  experience_= null, rating_= null, views_= null;
                JSONObject jObjNewCatList= null;
                JSONArray tempBestDocsArray = null;

                tempBestDocsArray = jObjResult.getJSONArray("best_doctors");
                if (tempBestDocsArray.length() != 0) {
                    for (int i = 0; i < tempBestDocsArray.length(); i++) {

                        searchListArray = tempBestDocsArray.getString(i);
                        jObjNewCatList = new JSONObject(searchListArray);

                        if (jObjNewCatList.has("id")) {

                            item_id_ = jObjNewCatList.getString("id");
                        }
                        if (jObjNewCatList.has("service_type_name")) {

                            type_name_ = jObjNewCatList.getString("service_type_name");
                        }
                        if (jObjNewCatList.has("service_type")) {

                            type_ = jObjNewCatList.getString("service_type");
                        }
                        if (jObjNewCatList.has("name")) {

                            item_name_ = jObjNewCatList.getString("name");
                        }
                        if (jObjNewCatList.has("thumbnail_url")) {

                            item_thumb_ = jObjNewCatList.getString("thumbnail_url");
                        }
                        if (jObjNewCatList.has("clinic_name")) {

                            clinic_name_ = jObjNewCatList.getString("clinic_name");
                        }
                        if (jObjNewCatList.has("locality_name")) {

                            location_ = jObjNewCatList.getString("locality_name");
                        }
                        if (jObjNewCatList.has("qualifications")) {

                            qualification_ = jObjNewCatList.getString("qualifications");
                        }
                        if (jObjNewCatList.has("years_of_experience")) {

                            experience_ = jObjNewCatList.getString("years_of_experience");
                        }
                        if (jObjNewCatList.has("rating")) {

                            rating_ = jObjNewCatList.getString("rating");
                        }
                        if (jObjNewCatList.has("counter_views")) {

                            views_ = jObjNewCatList.getString("counter_views");
                        }

                        SearchItems tempCatList = new SearchItems(type_name_, type_, item_id_, item_name_, item_thumb_, clinic_name_, location_, qualification_, experience_, rating_, views_);
                        bestDocsArrayList.add(tempCatList);
                    }
                }

            }

             /*Best docs array variables*/
            if(jObjResult.has("banner_data")){

                /* banner data array variables */
                bannerItemsArrayList.clear();
                String bannerListArray, image_= null, text_= null, url_= null;
                JSONObject jObjNewBannerList= null;
                JSONArray tempBannerArray = null;

                tempBannerArray = jObjResult.getJSONArray("banner_data");
                if (tempBannerArray.length() != 0) {
                    for (int i = 0; i < tempBannerArray.length(); i++) {

                        bannerListArray = tempBannerArray.getString(i);
                        jObjNewBannerList = new JSONObject(bannerListArray);

                        if (jObjNewBannerList.has("banner_image")) {

                            image_ = jObjNewBannerList.getString("banner_image");
                        }
                        if (jObjNewBannerList.has("title")) {

                            text_ = jObjNewBannerList.getString("title");
                        }
                        if (jObjNewBannerList.has("url")) {

                            url_ = jObjNewBannerList.getString("url");
                        }
                        BannerItems tempBannerList = new BannerItems(image_, text_, url_);
                        bannerItemsArrayList.add(tempBannerList);
                    }
                }
            }
            showDataView();

            //setting all text
            setTexts();
            setDrawerHeader();
            invalidateOptionsMenu();

            //setting banner data
            bannerViewPager_vp.setAdapter(bannerPagerAdapter);
            indicator.setViewPager(bannerViewPager_vp);
            handler.postDelayed(runnable, delay);//starting banner auto-scroll

            viewPager.setAdapter(tabPagerAdapter);
            bestDoctorsAdapter.notifyDataSetChanged();
            tabLayout.post(new Runnable() {
                @Override
                public void run() {

                    tabLayout.setupWithViewPager(viewPager);
                    for (int i = 0; i < tabLayout.getTabCount(); i++) {

                        if (i == 0) {
                            tabLayout.getTabAt(i).setIcon(R.drawable.ic_clinics_active);
                        }
                        else if (i == 1) {
                            tabLayout.getTabAt(i).setIcon(R.drawable.ic_laser_clinic_normal);
                        }
                        else if (i == 2) {
                            tabLayout.getTabAt(i).setIcon(R.drawable.ic_diagnostics_labs_normal);

                        }
                        else if (i == 3) {
                            tabLayout.getTabAt(i).setIcon(R.drawable.ic_spas_and_saloon_normal);

                        }
                        else if (i == 4) {
                            tabLayout.getTabAt(i).setIcon(R.drawable.ic_fitness_normal);

                        }
                        else if (i == 5) {
                            tabLayout.getTabAt(i).setIcon(R.drawable.ic_pharmacy_normal);

                        }
                    }
                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void setDrawerHeader() {

        if(UserManager.getUserLoginStatus(MainActivity.this)){

            name_tv.setText(UserManager.getUserName(MainActivity.this));
//            userStatus_tv.setText("Sign Out");
            setHeaderUserStatusSignOutText();
            signinSignout_ll.setBackgroundResource(R.drawable.bg_red_round_corner);
        }
        else{

            name_tv.setText("Guest");
//            userStatus_tv.setText("Sign In");
            setHeaderUserStatusSignInText();
            signinSignout_ll.setBackgroundResource(R.drawable.bg_green_round_corner);
        }
    }

    public void setHeaderUserStatusSignInText(){

        if(UserManager.getUserLanguage(MainActivity.this).equals("en")){

            userStatus_tv.setText(R.string.menu_sign_in_en);
        }
        else{

            userStatus_tv.setText(R.string.menu_sign_in_ar);
        }
    }

    public void setHeaderUserStatusSignOutText(){

        if(UserManager.getUserLanguage(MainActivity.this).equals("en")){

            userStatus_tv.setText(R.string.menu_sign_out_en);
        }
        else{

            userStatus_tv.setText(R.string.menu_sign_out_ar);
        }

    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText(R.string.app_title);
    }

    private void manageDrawerMenu() {
        Menu nav_Menu = navigationView.getMenu();

        if(UserManager.getUserLoginStatus(MainActivity.this)){

            nav_Menu.findItem(R.id.menu_sign_in).setVisible(false);
            nav_Menu.findItem(R.id.menu_signout).setVisible(true);
        }
        else{


            nav_Menu.findItem(R.id.menu_sign_in).setVisible(true);
            nav_Menu.findItem(R.id.menu_signout).setVisible(false);
        }
        actionBarDrawerToggle.syncState();
    }

    /* Runnable thread for handling smooth drawer closing */
    Runnable closeDrawerRunnable = new Runnable() {
        @Override
        public void run() {

            drawerLayout.closeDrawers();
        }
    };

    /*  pager adapter class for categories in home fragments*/
    class TabPagerAdapter extends FragmentStatePagerAdapter {

        public TabPagerAdapter(FragmentManager fm ){
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            if(position==0) {
                ClinicsFragment clinicsFragment = ClinicsFragment.newInstance(MainActivity.this);
                return clinicsFragment;
            }
            else if(position==1){
                LaserClinicFragment laserClinicFragment = LaserClinicFragment.newInstance(MainActivity.this);
                return laserClinicFragment;
            }
            else if(position==2){
                LabsFragment labsFragment = LabsFragment.newInstance(MainActivity.this);
                return labsFragment;
            }
            else if(position==3){
                SpaFragment spaFragment = SpaFragment.newInstance(MainActivity.this);
                return spaFragment;
            }
            else if(position==4){
                FitnessFragment fitnessFragment = FitnessFragment.newInstance(MainActivity.this);
                return fitnessFragment;
            }
            else if(position==5){
                PharmaciesFragment pharmaciesFragment = PharmaciesFragment.newInstance(MainActivity.this);
                return pharmaciesFragment;
            }
            return  null;
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if(UserManager.getUserLanguage(MainActivity.this).equals("ar")){

                return tabData_ar[position];
            }
            else{

                return  tabData[position];
            }
        }
    }

    /* method for setting tab icons & toolbar title on tab switching */
    public void setTabSelector(int selectedTab) {

        switch (selectedTab) {

            case 0:

                tabLayout.getTabAt(0).setIcon(R.drawable.ic_clinics_active);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_laser_clinic_normal);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_spas_and_saloon_normal);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_fitness_normal);
                tabLayout.getTabAt(5).setIcon(R.drawable.ic_pharmacy_normal);

                break;

            case 1:

                tabLayout.getTabAt(0).setIcon(R.drawable.ic_clinics_normal);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_laser_clinic_active);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_spas_and_saloon_normal);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_fitness_normal);
                tabLayout.getTabAt(5).setIcon(R.drawable.ic_pharmacy_normal);

                break;

            case 2:

                tabLayout.getTabAt(0).setIcon(R.drawable.ic_clinics_normal);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_laser_clinic_normal);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_diagnostics_labs_active);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_spas_and_saloon_normal);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_fitness_normal);
                tabLayout.getTabAt(5).setIcon(R.drawable.ic_pharmacy_normal);

                break;

            case 3:

                tabLayout.getTabAt(0).setIcon(R.drawable.ic_clinics_normal);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_spas_and_saloon_active);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_fitness_normal);
                tabLayout.getTabAt(5).setIcon(R.drawable.ic_pharmacy_normal);

                break;

            case 4:

                tabLayout.getTabAt(0).setIcon(R.drawable.ic_clinics_normal);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_spas_and_saloon_normal);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_fitness_active);
                tabLayout.getTabAt(5).setIcon(R.drawable.ic_pharmacy_normal);

                break;

            case 5:

                tabLayout.getTabAt(0).setIcon(R.drawable.ic_clinics_normal);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_diagnostics_labs_normal);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_spas_and_saloon_normal);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_fitness_normal);
                tabLayout.getTabAt(5).setIcon(R.drawable.ic_pharmacy_active);

                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            timeOutMsg();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean mIsBackEligible = false;
    void timeOutMsg(){

        if (mIsBackEligible) {

            super.onBackPressed();
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
        else {

        }
        mIsBackEligible = true;
        new Runnable() {
            @Override
            public void run() {
                CountDownTimer cdt = new CountDownTimer(2000, 2000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mIsBackEligible = false;

                    }

                }.start();

            }
        }.run(); // End Runnable()

        String msg;
        if(UserManager.getUserLanguage(MainActivity.this).equals("en")){

            msg = getResources().getString(R.string.app_exit_alert_en);
        }
        else {

            msg = getResources().getString(R.string.app_exit_alert_ar);
        }

        showSnackBar(msg);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menuRightPromos_mn       = menu.findItem(R.id.promotions);
        menuRightNewsEvents_mn   = menu.findItem(R.id.menu_news_and_events);

        if(UserManager.getUserLanguage(MainActivity.this).equals("en")){

            setRightMenuTexts_en();
        }else{

            setRightMenuTexts_ar();
        }

        return true;
    }

    public void setRightMenuTexts_en(){

        menuRightPromos_mn.setTitle(R.string.menu_promotions_en);
        menuRightNewsEvents_mn.setTitle(R.string.menu_news_and_events_en);
    }

    public void setRightMenuTexts_ar(){

        menuRightPromos_mn.setTitle(R.string.menu_promotions_ar);
        menuRightNewsEvents_mn.setTitle(R.string.menu_news_and_events_ar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        else if (id == R.id.promotions) {

            Intent intentPromos = new Intent(MainActivity.this, PromotionsList.class);
            startActivity(intentPromos);
            return true;

        }
        else if (id == R.id.menu_news_and_events) {

            Intent intentMedJobs = new Intent(MainActivity.this, NewsAndEventsList.class);
            startActivity(intentMedJobs);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // checking for type intent filter
            if (intent.getAction().equals(Constants.Keys.REGISTRATION_COMPLETE)) {
                manageDrawerMenu();
                setDrawerHeader();
                Log.e("Broadcast Reciever", "Registration completed");
            }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.Keys.REGISTRATION_COMPLETE));

//        //starting banner auto-scroll
//        handler.postDelayed(runnable, delay);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //terminating banner auto-scroll
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
