package app.ens.eyadat4u.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;

public class PromotionDetails extends BaseActivity {

    Toolbar     toolbar;
    TextView    toolbarTitle_tv;
    TextView    desc_tv, startDate_tv, endDate_tv;
    ImageView   itemImage_iv;
    UILHandler  uilHandler;
    String      title, description, image, startDate, endDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_details);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        uilHandler      = (UILHandler) getApplication();

        title           = getIntent().getExtras().getString("ITEM_TITLE");
        description     = getIntent().getExtras().getString("ITEM_DESC");
        image           = getIntent().getExtras().getString("ITEM_IMAGE");
        startDate       = getIntent().getExtras().getString("ITEM_START_DATE");
        endDate         = getIntent().getExtras().getString("ITEM_END_DATE");

        initViews();
        setToolbar();

        desc_tv.setText(description);
        startDate_tv.setText(startDate);
        endDate_tv.setText(endDate);

        uilHandler.loadImage(image, itemImage_iv , Constants.imageSpecifier.NORMAL);
    }

    private void initViews() {

        toolbar             = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv     = (TextView) findViewById(R.id.toolbar_title);
        itemImage_iv        = (ImageView) findViewById(R.id.promo_image);
        desc_tv             = (TextView) findViewById(R.id.description);
        startDate_tv        = (TextView) findViewById(R.id.start_date);
        endDate_tv          = (TextView) findViewById(R.id.end_date);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbarTitle_tv.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }
}
