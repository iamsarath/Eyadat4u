package app.ens.eyadat4u.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

public class BookAppointment extends BaseActivity {

    Toolbar             toolbar;
    ListView            list_lv;
    TextView            toolbarTitle_tv, itemName_tv, itemLocation_tv, bookedClinic_tv, bookedDoctor_tv,
                        bookingTime_tv, bookingDate_tv, userName_tv, bookAppointmentText_tv,
                        userEmail_tv, userPhone_tv, terms_tv, patientText_tv, emailText_tv, phoneText_tv, appointmentOnText_tv, byBooking_tv;
    LinearLayout        parentLayout_ll, bookAppointment_ll;
    String              itemId, itemName, itemPic, clinicName, location, bookingTime, bookingDate, timeSlotId;
    ImageView           itemImage_iv;
    UILHandler          uilHandler;
    RequestQueue        queue;
    JsonObjectRequest   jsonObjectRequest;
    ProgressDialog      progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        queue                   = Volley.newRequestQueue(BookAppointment.this);
        uilHandler              = (UILHandler) getApplication();

        itemId                  = getIntent().getExtras().getString("ITEM_ID");
        itemName                = getIntent().getExtras().getString("ITEM_NAME");
        clinicName              = getIntent().getExtras().getString("CLINIC_NAME");
        itemPic                 = getIntent().getExtras().getString("ITEM_PIC");
        location                = getIntent().getExtras().getString("LOCATION");
        bookingDate             = getIntent().getExtras().getString("APPOINTMENT_TIME");
        bookingTime             = getIntent().getExtras().getString("TIME_SLOT");
        timeSlotId              = getIntent().getExtras().getString("TIME_SLOT_ID");

        initViews();
        setToolbar();

        itemName_tv.setText(itemName);
        itemLocation_tv.setText(clinicName);
        uilHandler.loadImage(itemPic, itemImage_iv , Constants.imageSpecifier.ROUND_EDGE_MEDIUM);
        bookedClinic_tv.setText(clinicName);
        bookedDoctor_tv.setText(itemName);
        bookingTime_tv.setText(bookingTime);
        bookingDate_tv.setText(bookingDate);
        userName_tv.setText(" "+UserManager.getUserName(BookAppointment.this));
        userEmail_tv.setText(" "+UserManager.getUserEmail(BookAppointment.this));
        userPhone_tv.setText(" "+UserManager.getUserPhone(BookAppointment.this));

        bookAppointment_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserManager.getUserLanguage(BookAppointment.this).equals("en")){

                    showAppointmentConfirmDialogue("", getResources().getString(R.string.you_are_about_to_fix_appointment_text_en)+
                            itemName+" "+getResources().getString(R.string.on_text_en)+" "+bookingDate, getResources().getString(R.string.proceed_en), getResources().getString(R.string.pick_another_time_en));
                }else{

                    showAppointmentConfirmDialogue("", getResources().getString(R.string.you_are_about_to_fix_appointment_text_ar)+
                            itemName+" "+getResources().getString(R.string.on_text_ar)+" "+bookingDate, getResources().getString(R.string.proceed_ar), getResources().getString(R.string.pick_another_time_ar));
                }
            }
        });
    }

    private void initViews() {

        toolbar                 = (Toolbar) findViewById(R.id.tool_bar);
        toolbarTitle_tv         = (TextView) findViewById(R.id.toolbar_title);
        list_lv                 = (ListView) findViewById(R.id.list);
        itemName_tv             = (TextView) findViewById(R.id.item_name);
        itemLocation_tv         = (TextView) findViewById(R.id.item_location);
        itemImage_iv            = (ImageView) findViewById(R.id.item_image);
        bookingTime_tv          = (TextView) findViewById(R.id.booking_time);
        bookingDate_tv          = (TextView) findViewById(R.id.booking_date);
        bookedClinic_tv         = (TextView) findViewById(R.id.booked_clinic);
        bookedDoctor_tv         = (TextView) findViewById(R.id.booked_doctor);
        userName_tv             = (TextView) findViewById(R.id.user_name);
        userEmail_tv            = (TextView) findViewById(R.id.user_email);
        userPhone_tv            = (TextView) findViewById(R.id.user_phone);
        bookAppointment_ll      = (LinearLayout) findViewById(R.id.book_layout);
        parentLayout_ll         = (LinearLayout) findViewById(R.id.parent_layout);
        terms_tv                = (TextView) findViewById(R.id.terms);
        bookAppointmentText_tv  = (TextView) findViewById(R.id.book_appointment_text);
        patientText_tv          = (TextView) findViewById(R.id.patient_text);
        emailText_tv            = (TextView) findViewById(R.id.email_text);
        phoneText_tv            = (TextView) findViewById(R.id.phone_text);
        appointmentOnText_tv    = (TextView) findViewById(R.id.appointment_on_text);
        byBooking_tv            = (TextView) findViewById(R.id.by_booking_text);


        terms_tv.setPaintFlags(terms_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void setToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(BookAppointment.this).equals("en")){

            toolbarTitle_tv.setText(R.string.book_appointment_en);
            bookAppointmentText_tv.setText(R.string.book_appointment_en);
            patientText_tv.setText(R.string.patient_en);
            emailText_tv.setText(R.string.email_en);
            phoneText_tv.setText(R.string.phone_en);
            appointmentOnText_tv.setText(R.string.appointment_on_en);
            byBooking_tv.setText(R.string.by_booking_text_en);
            terms_tv.setText(R.string.terms_and_conditions_en);
        }
        else {

            toolbarTitle_tv.setText(R.string.book_appointment_ar);
            bookAppointmentText_tv.setText(R.string.book_appointment_ar);
            patientText_tv.setText(R.string.patient_ar);
            emailText_tv.setText(R.string.email_ar);
            phoneText_tv.setText(R.string.phone_ar);
            appointmentOnText_tv.setText(R.string.appointment_on_ar);
            byBooking_tv.setText(R.string.by_booking_text_ar);
            terms_tv.setText(R.string.terms_and_conditions_ar);
        }
    }

    private String bookAppointemntObj(String language, String userId, String apiKey, String time_slot_id) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "language", language);
            mainObj.put( "user_id", userId );
            mainObj.put( "api_key", apiKey );
            mainObj.put( "schedule_id", time_slot_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void bookAppointemntApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(BookAppointment.this))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        progressdialog.dismiss();

                        JSONObject jObj;

                        jObj = new JSONObject(result);
                        if(jObj.has("status")){

                            if(jObj.getBoolean("status")){

                                // Send registration complete broadcast message
                                Intent registrationComplete = new Intent(Constants.Keys.APPOINTMENT_COMPLETE);
                                registrationComplete.putExtra("token", "Broadcast Message");
                                LocalBroadcastManager.getInstance(BookAppointment.this).sendBroadcast(registrationComplete);

                                Intent intent = new Intent(BookAppointment.this, AppointmentComplete.class);
                                startActivity(intent);
                                finish();
                            }
                            else{

                                showLoginAlert("", "Something went wrong. Please login again and continue !");
                            }
                        }

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");

                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_empty, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            flushAllNetworkConnections();
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* alert dialogues  */
    private void showLoginAlert(String title, String msg) {
        new AlertDialog.Builder(BookAppointment.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent myIntent = new Intent(BookAppointment.this, Login.class);
                        startActivity(myIntent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }

    private void showAppointmentConfirmDialogue(String title, String msg, String proceed, String pick_another) {
        new AlertDialog.Builder(BookAppointment.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(proceed, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //calling book appointment API
                        progressdialog = new ProgressDialog(BookAppointment.this);
                        progressdialog.setMessage("Booking appointment please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = bookAppointemntObj(UserManager.getUserLanguage(BookAppointment.this), UserManager.getUserId(BookAppointment.this),
                                UserManager.getUserApiKey(BookAppointment.this), timeSlotId);
                        bookAppointemntApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_BOOK_APPOINTMENT), obj);

                    }
                }).setNegativeButton(pick_another, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        })
                .create().show();
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(parentLayout_ll, ""+msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /* method used for flushing all network calls that are executing now and that were added to the queue */
    public void flushAllNetworkConnections(){

        if(jsonObjectRequest != null){

            queue.cancelAll(jsonObjectRequest);
        }
    }
}
