package app.ens.eyadat4u.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UserManager;

public class AppointmentComplete extends BaseActivity {

    LinearLayout myAppointments_ll;
    TextView     bookingCompleteText_tv, viewAllAppointments_tv, myAppointments_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_complete);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        initViews();

        myAppointments_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AppointmentComplete.this, MyAccount.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initViews() {

        myAppointments_ll           = (LinearLayout) findViewById(R.id.my_appointments_layout);
        bookingCompleteText_tv      = (TextView) findViewById(R.id.booking_completed_text);
        viewAllAppointments_tv      = (TextView) findViewById(R.id.view_all_timings_appointment_text);
        myAppointments_tv           = (TextView) findViewById(R.id.my_appointments_text);

        setTexts();
    }

    private void setTexts() {

        if(UserManager.getUserLanguage(AppointmentComplete.this).equals("en")){

            bookingCompleteText_tv.setText(R.string.booking_completed_text_en);
            viewAllAppointments_tv.setText(R.string.view_your_appointments_text_en);
            myAppointments_tv.setText(R.string.my_appointments_en);
        }else{

            bookingCompleteText_tv.setText(R.string.booking_completed_text_ar);
            viewAllAppointments_tv.setText(R.string.view_your_appointments_text_ar);
            myAppointments_tv.setText(R.string.my_appointments_ar);
        }
    }
}
