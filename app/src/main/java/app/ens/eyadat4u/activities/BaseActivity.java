package app.ens.eyadat4u.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.system.UserManager;

/**
 * Created by Sarath on 1/27/2017. Eyadat4u App developed by ENS Consultants.
 */

public abstract class BaseActivity extends AppCompatActivity {

    MenuItem    menuRightPromos_mn, menuRightNewsEvents_mn, menuOnlinePediatrician_mn, menuVisitingDocs_mn, menuBuyInsurance_mn,
                menuMedicalServices_mn, menuMediacalJobs_mn;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);

        menuRightPromos_mn          = menu.findItem(R.id.promotions);
        menuRightNewsEvents_mn      = menu.findItem(R.id.menu_news_and_events);
        menuOnlinePediatrician_mn   = menu.findItem(R.id.online_pediatricians);
//        menuVisitingDocs_mn         = menu.findItem(R.id.visiting_doctors);
        menuBuyInsurance_mn         = menu.findItem(R.id.buy_insurance);
        menuMedicalServices_mn      = menu.findItem(R.id.medical_services);
        menuMediacalJobs_mn         = menu.findItem(R.id.medical_jobs);

        if(UserManager.getUserLanguage(BaseActivity.this).equals("en")){

            setRightMenuTexts_en();
        }else{

            setRightMenuTexts_ar();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        else if (id == R.id.promotions) {

            Intent intentPromos = new Intent(BaseActivity.this, PromotionsList.class);
            startActivity(intentPromos);
            return true;

        }
//        else if (id == R.id.visiting_doctors) {
//
//            Intent intent = new Intent(BaseActivity.this, ServiceList.class);
//            intent.putExtra("TITLE", "Visiting Doctors");
//            intent.putExtra("SUB_TITLE", "Visiting Doctors");
//            intent.putExtra("SERVICE_ID", "0");
//            intent.putExtra("VISITING_DOCTOR", true);
//            startActivity(intent);
//            return true;
//
//        }
        else if (id == R.id.online_pediatricians) {

            Intent intentOnlinePediatricians = new Intent(BaseActivity.this, OnlinePediatrician.class);
            startActivity(intentOnlinePediatricians);
            return true;

        }
        else if (id == R.id.buy_insurance) {

            Intent intentInsurance = new Intent(BaseActivity.this, InsuranceList.class);
            startActivity(intentInsurance);
            return true;
        }
        else if (id == R.id.medical_services) {


            Intent intentMedServices = new Intent(BaseActivity.this, MedicalServices.class);
            startActivity(intentMedServices);
            return true;
        }
        else if (id == R.id.medical_jobs) {

            Intent intentMedJobs = new Intent(BaseActivity.this, MedicalJobs.class);
            startActivity(intentMedJobs);
            return true;
        }
        else if (id == R.id.menu_news_and_events) {

            Intent intentMedJobs = new Intent(BaseActivity.this, NewsAndEventsList.class);
            startActivity(intentMedJobs);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setRightMenuTexts_en(){

        menuRightPromos_mn.setTitle(R.string.menu_promotions_en);
        menuRightNewsEvents_mn.setTitle(R.string.menu_news_and_events_en);
        menuOnlinePediatrician_mn.setTitle(R.string.menu_online_pediatricians_en);
//        menuVisitingDocs_mn.setTitle(R.string.menu_visiting_doc_en);
        menuBuyInsurance_mn.setTitle(R.string.menu_buy_insurance_en);
        menuMedicalServices_mn.setTitle(R.string.menu_med_services_en);
        menuMediacalJobs_mn.setTitle(R.string.menu_med_jobs_en);
    }

    public void setRightMenuTexts_ar(){

        menuRightPromos_mn.setTitle(R.string.menu_promotions_ar);
        menuRightNewsEvents_mn.setTitle(R.string.menu_news_and_events_ar);
        menuOnlinePediatrician_mn.setTitle(R.string.menu_online_pediatricians_ar);
//        menuVisitingDocs_mn.setTitle(R.string.menu_visiting_doc_ar);
        menuBuyInsurance_mn.setTitle(R.string.menu_buy_insurance_ar);
        menuMedicalServices_mn.setTitle(R.string.menu_med_services_ar);
        menuMediacalJobs_mn.setTitle(R.string.menu_med_jobs_ar);
    }
}