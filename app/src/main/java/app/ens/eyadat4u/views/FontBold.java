package app.ens.eyadat4u.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import app.ens.eyadat4u.utils.Fonts;


/**
 * Created by Sarath on 1/24/2017.
 */

public class FontBold extends TextView {

    public FontBold(Context context) {
        super(context);
        Typeface face=Typeface.createFromAsset(context.getAssets(), Fonts.FONT_BOLD);
        this.setTypeface(face);
    }

    public FontBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face=Typeface.createFromAsset(context.getAssets(), Fonts.FONT_BOLD);
        this.setTypeface(face);
    }

    public FontBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face=Typeface.createFromAsset(context.getAssets(), Fonts.FONT_BOLD);
        this.setTypeface(face);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);

    }
}