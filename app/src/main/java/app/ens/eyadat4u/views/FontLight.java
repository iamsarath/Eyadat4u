package app.ens.eyadat4u.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import app.ens.eyadat4u.utils.Fonts;

/**
 * Created by Sarath on 1/24/2017.
 */
public class FontLight extends TextView {

    public FontLight(Context context) {
        super(context);
        Typeface face=Typeface.createFromAsset(context.getAssets(), Fonts.FONT_LIGHT);
        this.setTypeface(face);
    }

    public FontLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face=Typeface.createFromAsset(context.getAssets(), Fonts.FONT_LIGHT);
        this.setTypeface(face);
    }

    public FontLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face=Typeface.createFromAsset(context.getAssets(), Fonts.FONT_LIGHT);
        this.setTypeface(face);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}