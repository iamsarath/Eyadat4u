package app.ens.eyadat4u.utils;

/**
 * Created by Sarath on 1/29/2016.
 */
public class UrlDispatcher {

    public static String getUrl(String token){

        String url      = "";
//        String baseUrl  = "http://arworksdemo.com/eyadat4ulatest/index.php/api/";
        String baseUrl  = "http://eyadat4u.com/api/";

        switch (token) {

            case Constants.UrlSpecifier.BASE_URL :
                url = baseUrl;
                break;

            case Constants.UrlSpecifier.URL_USER_REGISTRATION :
                url = baseUrl+"registration";
                break;

            case Constants.UrlSpecifier.URL_USER_VERIFY_EMAIL :
                url = baseUrl+"email_verify";
                break;

            case Constants.UrlSpecifier.URL_USER_LOGIN :
                url = baseUrl+"login";
                break;

            case Constants.UrlSpecifier.URL_INIT_DATA :
                url = baseUrl+"init_data";
                break;

            case Constants.UrlSpecifier.URL_SEARCH_LIST :
                url = baseUrl+"search";
                break;

            case Constants.UrlSpecifier.URL_ITEM_DETAILS :
                url = baseUrl+"detail";
                break;

            case Constants.UrlSpecifier.URL_APPOINTMENT_TIME_SLOT_PICKER :
                url = baseUrl+"appointment_check";
                break;

            case Constants.UrlSpecifier.URL_BOOK_APPOINTMENT :
                url = baseUrl+"appointment_process";
                break;

            case Constants.UrlSpecifier.URL_MY_APPOINTMENT :
                url = baseUrl+"appointments";
                break;

            case Constants.UrlSpecifier.URL_NEWS_AND_EVENTS :
                url = baseUrl+"news_events";
                break;

            case Constants.UrlSpecifier.URL_PROMOTIONS :
                url = baseUrl+"promotions";
                break;

            case Constants.UrlSpecifier.URL_INSURANCES :
                url = baseUrl+"insurances";
                break;

            case Constants.UrlSpecifier.URL_MEDICAL_SERVICE_CATEGORY :
                url = baseUrl+"medicalservicecategory";
                break;

            case Constants.UrlSpecifier.URL_MEDICAL_SERVICE_REQUEST :
                url = baseUrl+"medicalservice_request";
                break;

            case Constants.UrlSpecifier.URL_MEDICAL_JOB_TYPES :
                url = baseUrl+"medicaljobtypes";
                break;

            case Constants.UrlSpecifier.URL_MEDICAL_JOB_REQUEST :
                url = baseUrl+"medicaljob_request";
                break;

            case Constants.UrlSpecifier.URL_FILE_UPLOAD :
                url = baseUrl+"file_upload";
                break;

            case Constants.UrlSpecifier.URL_UPDATE_PROFILE :
                url = baseUrl+"profile_update";
                break;

            case Constants.UrlSpecifier.URL_CANCEL_APPOINTMENT :
                url = baseUrl+"appointment_cancel";
                break;

            case Constants.UrlSpecifier.URL_COMMON_SEARCH :
                url = baseUrl+"search_keyword/";
                break;

            case Constants.UrlSpecifier.URL_CHANGE_PASSWORD :
                url = baseUrl+"change_password";
                break;

            case Constants.UrlSpecifier.URL_GIVE_FEEDBACK :
                url = baseUrl+"submit_feedback";
                break;

            case Constants.UrlSpecifier.URL_FORGET_PASSWORD :
                url = baseUrl+"forgot_password";
                break;

            case Constants.UrlSpecifier.URL_RESEND_OTP :
                url = baseUrl+"resend_otp";
                break;

            case Constants.UrlSpecifier.URL_MEDICINE_SEARCH :
                url = baseUrl+"pharamcy_medicines";
                break;
        }
        System.out.println("url--> "+url);
            return url;
    }
}
