package app.ens.eyadat4u.utils;

/**
 * Created by Sarath on 2/18/2017.
 */
public class MonthPicker {

    public static String getMonth(String month){

        String date= "";
        switch (month) {

            case "01":
                date= "Jan";
                break;

            case "02":
                date= "Feb";
                break;

            case "03":
                date= "Mar";
                break;

            case "04":
                date= "Apr";
                break;

            case "05":
                date= "May";
                break;

            case "06":
                date= "Jun";
                break;

            case "07":
                date= "Jul";
                break;

            case "08":
                date= "Aug";
                break;

            case "09":
                date= "Sep";
                break;

            case "10":
                date= "Oct";
                break;

            case "11":
                date= "Nov";
                break;

            case "12":
                date= "Dec";
                break;
        }

        return date;

    }

}
