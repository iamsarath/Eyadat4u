package app.ens.eyadat4u.utils;

/**
 * Created by Sarath on 2/8/2017.
 */
import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


public class CheckNetwork {

    private static Context mycon;

    public CheckNetwork(Context context) {
        mycon = context;
    }

    private static final String TAG = CheckNetwork.class.getSimpleName();

    @SuppressLint("ShowToast")
    public static boolean isInternetAvailable(Context context)
    {

        NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();



        if (info == null)
        {
            Log.d(TAG,"No internet connection");
            return false;
        }
        else
        {
            if(info.isConnected())
            {
                Log.d(TAG," Internet connection available");
                return true;
            }
            else
            {
                Log.d(TAG," Internet connection available");
                return true;
            }

        }

    }

}

