package app.ens.eyadat4u.utils;

import java.util.ArrayList;

import app.ens.eyadat4u.items.FilterDayCheckItems;
import app.ens.eyadat4u.items.FilterFeesCheckItems;
import app.ens.eyadat4u.items.FilterTimeCheckItems;
import app.ens.eyadat4u.items.MainCategoryItems;
import app.ens.eyadat4u.items.TimeSlotItems;

/**
 * Created by Sarath on 2/8/2017.
 */

public class Constants {

    public interface UrlSpecifier{

        /* 1) */     public static String BASE_URL                              =   "BASE_URL";
        /* 2) */     public static String URL_USER_REGISTRATION                 =   "URL_USER_REGISTRATION";
        /* 3) */     public static String URL_USER_VERIFY_EMAIL                 =   "URL_USER_VERIFY_EMAIL";
        /* 4) */     public static String URL_USER_LOGIN                        =   "URL_USER_LOGIN";
        /* 5) */     public static String URL_INIT_DATA                         =   "URL_INIT_DATA";
        /* 6) */     public static String URL_SEARCH_LIST                       =   "URL_SEARCH_LIST";
        /* 7) */     public static String URL_ITEM_DETAILS                      =   "URL_ITEM_DETAILS";
        /* 8) */     public static String URL_APPOINTMENT_TIME_SLOT_PICKER      =   "URL_APPOINTMENT_TIME_SLOT_PICKER";
        /* 9) */     public static String URL_BOOK_APPOINTMENT                  =   "URL_BOOK_APPOINTMENT";
        /* 10) */    public static String URL_MY_APPOINTMENT                    =   "URL_MY_APPOINTMENT";
        /* 11) */    public static String URL_NEWS_AND_EVENTS                   =   "URL_NEWS_AND_EVENTS";
        /* 12) */    public static String URL_PROMOTIONS                        =   "URL_PROMOTIONS";
        /* 13) */    public static String URL_INSURANCES                        =   "URL_INSURANCES";
        /* 14) */    public static String URL_MEDICAL_SERVICE_CATEGORY          =   "URL_MEDICAL_SERVICE_CATEGORY";
        /* 15) */    public static String URL_MEDICAL_SERVICE_REQUEST           =   "URL_MEDICAL_SERVICE_REQUEST";
        /* 16) */    public static String URL_MEDICAL_JOB_TYPES                 =   "URL_MEDICAL_JOB_TYPES";
        /* 17) */    public static String URL_MEDICAL_JOB_REQUEST               =   "URL_MEDICAL_JOB_REQUEST";
        /* 18) */    public static String URL_UPDATE_PROFILE                    =   "URL_UPDATE_PROFILE";
        /* 19) */    public static String URL_CANCEL_APPOINTMENT                =   "URL_CANCEL_APPOINTMENT";
        /* 20) */    public static String URL_COMMON_SEARCH                     =   "URL_COMMON_SEARCH";
        /* 21) */    public static String URL_FILE_UPLOAD                       =   "URL_FILE_UPLOAD";
        /* 22) */    public static String URL_CHANGE_PASSWORD                   =   "URL_CHANGE_PASSWORD";
        /* 23) */    public static String URL_GIVE_FEEDBACK                     =   "URL_GIVE_FEEDBACK";
        /* 24) */    public static String URL_FORGET_PASSWORD                   =   "URL_FORGET_PASSWORD";
        /* 25) */    public static String URL_RESEND_OTP                        =   "URL_RESEND_OTP";
        /* 26) */    public static String URL_MEDICINE_SEARCH                   =   "URL_MEDICINE_SEARCH";
    }

    public interface imageSpecifier{

        String NORMAL                       = "NORMAL";
        String ROUND_EDGE_SMALL             = "ROUND_EDGE_SMALL";
        String ROUND_EDGE_MEDIUM            = "ROUND_EDGE_MEDIUM";
        String ROUND_EDGE_LARGE             = "ROUND_EDGE_LARGE";
        String ROUND_EDGE_EXTRA_LARGE       = "ROUND_EDGE_EXTRA_LARGE";
        String LAYOUT_NORMAL                = "LAYOUT_NORMAL";
    }

    public interface Keys{

        // broadcast receiver intent filters
        public static final String REGISTRATION_COMPLETE    = "registrationComplete";
        public static final String CHANGE_OTP_EMAIL         = "changeOTPEmail";
        public static final String APPOINTMENT_COMPLETE     = "appointmentComplete";
        public static final String SUBMITTED_FEEDBACK       = "submittedFeedBack";
    }

    //arraylists
    public static ArrayList<MainCategoryItems> clinicCategoryItemsArrayList                 = new ArrayList<MainCategoryItems>();
    public static ArrayList<MainCategoryItems> laserclinicCategoryItemsArrayList            = new ArrayList<MainCategoryItems>();
    public static ArrayList<MainCategoryItems> diagnosticLabsCategoryItemsArrayList         = new ArrayList<MainCategoryItems>();
    public static ArrayList<MainCategoryItems> spasSalonsCategoryItemsArrayList             = new ArrayList<MainCategoryItems>();
    public static ArrayList<MainCategoryItems> fitnessCategoryItemsArrayList                = new ArrayList<MainCategoryItems>();
    public static ArrayList<MainCategoryItems> pharmaciesCategoryItemsArrayList             = new ArrayList<MainCategoryItems>();

    //arraylists- filter
    public static ArrayList<FilterDayCheckItems> filterDayCheckItemsArrayList               = new ArrayList<FilterDayCheckItems>();
    public static ArrayList<FilterTimeCheckItems> filterTimeCheckItemsArrayList             = new ArrayList<FilterTimeCheckItems>();
    public static ArrayList<FilterFeesCheckItems> filterFeesCheckItemsArrayList             = new ArrayList<FilterFeesCheckItems>();


//    public static boolean is_filter_selected = false;

}
