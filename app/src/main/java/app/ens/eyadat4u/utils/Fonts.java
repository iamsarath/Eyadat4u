package app.ens.eyadat4u.utils;

/**
 * Created by Sarath on 1/24/2017.
 */
public class Fonts {

    public static String FONT_REGULAR="fonts/Roboto-Regular.ttf";
    public static String FONT_BOLD="fonts/Roboto-Bold.ttf";
    public static String FONT_LIGHT="fonts/Roboto-Italic.ttf";

}
