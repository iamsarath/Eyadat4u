package app.ens.eyadat4u.utils;

/**
 * Created by Sarath on 2/23/2017.
 */

public class MonthPickerCustomized {

    public static String getMonth(String month){

        String date= "";
        switch (month) {

            case "1":
                date= "Jan";
                break;

            case "2":
                date= "Feb";
                break;

            case "3":
                date= "Mar";
                break;

            case "4":
                date= "Apr";
                break;

            case "5":
                date= "May";
                break;

            case "6":
                date= "Jun";
                break;

            case "7":
                date= "Jul";
                break;

            case "8":
                date= "Aug";
                break;

            case "9":
                date= "Sep";
                break;

            case "10":
                date= "Oct";
                break;

            case "11":
                date= "Nov";
                break;

            case "12":
                date= "Dec";
                break;
        }

        return date;

    }
}
