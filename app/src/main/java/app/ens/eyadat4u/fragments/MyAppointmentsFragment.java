package app.ens.eyadat4u.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.MyAccount;
import app.ens.eyadat4u.adapters.MyAppointmentsAdapter;
import app.ens.eyadat4u.adapters.ServiceCategoriesAdapter;
import app.ens.eyadat4u.items.MyAppointmentItems;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

/**
 * Created by Sarath on 2/21/2017.
 */

public class MyAppointmentsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String          mParam1;
    private TextView        tabText;
    static Context          mContext;
    ListView                appointmentList_lv;
    public static MyAppointmentsAdapter   myAppointmentsAdapter;
    ProgressBar             progressBar_pb;
    RequestQueue            queue;
    JsonObjectRequest       jsonObjectRequest;
    public static LinearLayout            progressLayout_ll, dataLayout_ll, noDataLayout_ll, guestLayout_ll;
    public static ArrayList<MyAppointmentItems> appointmentsArrayList   = new ArrayList<MyAppointmentItems>();

    public MyAppointmentsFragment() {
        // Required empty public constructor
    }

    public static MyAppointmentsFragment newInstance(Context context) {
        MyAppointmentsFragment fragment = new MyAppointmentsFragment();

        mContext= context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_my_appointments, container, false);

        queue                   = Volley.newRequestQueue(getActivity());
        myAppointmentsAdapter   = new MyAppointmentsAdapter(getActivity(), appointmentsArrayList);

        initViews(convertView);

        appointmentList_lv.setAdapter(myAppointmentsAdapter);

        if(UserManager.getUserLoginStatus(getActivity())){


            //calling search API
            showProgressView();
            String obj = myAppointmentsDataObj(UserManager.getUserLanguage(getActivity()), UserManager.getUserId(getActivity()),
                    UserManager.getUserApiKey(getActivity()),"0");
            System.out.println("obj ---> "+obj);
            myAppointmentsListApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_MY_APPOINTMENT), obj);
        }
        else{

            showGuestView();
        }

        return convertView;
    }

    private void initViews(View convertView) {

        appointmentList_lv      = (ListView) convertView.findViewById(R.id.list);
        progressLayout_ll       = (LinearLayout) convertView.findViewById(R.id.progress_view);
        dataLayout_ll           = (LinearLayout) convertView.findViewById(R.id.data_view);
        noDataLayout_ll         = (LinearLayout) convertView.findViewById(R.id.no_data_view);
        guestLayout_ll          = (LinearLayout) convertView.findViewById(R.id.quest_view);
        progressBar_pb          = (ProgressBar) convertView.findViewById(R.id.progressBar);
        progressBar_pb.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.ColorPrimary),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public void showDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.VISIBLE);
        noDataLayout_ll.setVisibility(View.GONE);
        guestLayout_ll.setVisibility(View.GONE);
    }

    public void showProgressView(){

        progressLayout_ll.setVisibility(View.VISIBLE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.GONE);
        guestLayout_ll.setVisibility(View.GONE);
    }

    public static void showNoDataView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.VISIBLE);
        guestLayout_ll.setVisibility(View.GONE);
    }

    public void showGuestView(){

        progressLayout_ll.setVisibility(View.GONE);
        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.GONE);
        guestLayout_ll.setVisibility(View.VISIBLE);
    }


    private String myAppointmentsDataObj(String language, String userId, String apiKey, String pageNumber) {

        JSONObject mainObj = new JSONObject();
        try {
            mainObj.put( "language", language );
            mainObj.put( "user_id", userId );
            mainObj.put( "api_key", apiKey );
            mainObj.put( "page_number", pageNumber );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void myAppointmentsListApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(getActivity()))
        {
            try {

                MyAccount.showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result = response.toString();
                        parseServiceList(result);

                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            MyAccount.showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    //method for parsing category list data
    void parseServiceList(String response){

        JSONObject jObj;
        try {
            appointmentsArrayList.clear();
            jObj = new JSONObject(response);
            /*clinics cat data array variables*/
            if(jObj.has("appoint_data")){

                /* doctors list array variables */

                String appointmentListArray, type_= null, appointment_id_= null, appointment_date_= null, appointment_from_time_= null, appointment_to_time_ = null
                        , item_id_= null, item_name_= null, item_thumb_= null, clinic_name_= null, location_= null,
                        qualification_= null,  experience_= null, rating_= null, views_= null;
                JSONObject jObjDocDetailsList, jsonNewObj= null;
                JSONArray tempCatListArray = null;

                tempCatListArray = jObj.getJSONArray("appoint_data");
                if (tempCatListArray.length() != 0) {
                    for (int i = 0; i < tempCatListArray.length(); i++) {

                        appointmentListArray = tempCatListArray.getString(i);
                        jObjDocDetailsList = new JSONObject(appointmentListArray);

                        if (jObjDocDetailsList.has("app_id")) {

                            appointment_id_ = jObjDocDetailsList.getString("app_id");
                        }
                        if (jObjDocDetailsList.has("date")) {

                            appointment_date_ = jObjDocDetailsList.getString("date");

                        }
                        if (jObjDocDetailsList.has("from")) {

                            appointment_from_time_ = jObjDocDetailsList.getString("from");

                        }
                        if (jObjDocDetailsList.has("to")) {

                            appointment_to_time_ = jObjDocDetailsList.getString("to");

                        }

                        if (jObjDocDetailsList.has("doctor_details")) {

                            jsonNewObj= new JSONObject(jObjDocDetailsList.getString("doctor_details"));

                            if (jsonNewObj.has("id")) {

                                item_id_ = jsonNewObj.getString("id");
                            }
                            if (jsonNewObj.has("service_type")) {

                                type_ = jsonNewObj.getString("service_type");
                            }
                            if (jsonNewObj.has("name")) {

                                item_name_ = jsonNewObj.getString("name");
                            }
                            if (jsonNewObj.has("thumbnail_url")) {

                                item_thumb_ = jsonNewObj.getString("thumbnail_url");
                            }
                            if (jsonNewObj.has("clinic_name")) {

                                clinic_name_ = jsonNewObj.getString("clinic_name");
                            }
                            if (jsonNewObj.has("locality_name")) {

                                location_ = jsonNewObj.getString("locality_name");
                            }
                            if (jsonNewObj.has("qualifications")) {

                                qualification_ = jsonNewObj.getString("qualifications");
                            }
                            if (jsonNewObj.has("years_of_experience")) {

                                experience_ = jsonNewObj.getString("years_of_experience");
                            }
                            if (jsonNewObj.has("rating")) {

                                rating_ = jsonNewObj.getString("rating");
                            }
                            if (jsonNewObj.has("counter_views")) {

                                views_ = jsonNewObj.getString("counter_views");
                            }
                        }
                        else {

                        }

                        MyAppointmentItems tempCatList = new MyAppointmentItems(appointment_id_, appointment_date_, appointment_from_time_, appointment_to_time_,
                                type_, item_id_, item_name_, item_thumb_, clinic_name_, location_, qualification_, experience_, rating_, views_);
                        appointmentsArrayList.add(tempCatList);
                    }
                }
            }
            else {
            }
            if(appointmentsArrayList.size() !=0){

                myAppointmentsAdapter.notifyDataSetChanged();
                showDataView();
            }
            else{

                showNoDataView();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}