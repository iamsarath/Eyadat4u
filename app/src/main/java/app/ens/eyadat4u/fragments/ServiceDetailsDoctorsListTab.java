package app.ens.eyadat4u.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.adapters.ConnectedListAdapter;
import app.ens.eyadat4u.items.SearchItems;

/**
 * Created by Sarath on 1/31/2017.
 */

public class ServiceDetailsDoctorsListTab extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String                          mParam1;
    static Context                          mContext;
    RecyclerView                            clinicsList_rv;
    ConnectedListAdapter                    connectedListAdapter;
    public static ArrayList<SearchItems>    connectedItemsArrayList_       = new ArrayList<SearchItems>();

    public ServiceDetailsDoctorsListTab() {
        // Required empty public constructor
    }

    public static ServiceDetailsDoctorsListTab newInstance(Context context, ArrayList<SearchItems> connectedItemsArrayList) {
        ServiceDetailsDoctorsListTab fragment = new ServiceDetailsDoctorsListTab();

        mContext= context;
        connectedItemsArrayList_= connectedItemsArrayList;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_service_details_doctors_list_tab, container, false);
        clinicsList_rv   =(RecyclerView)convertView.findViewById(R.id.connected_list);

        try {
            connectedListAdapter = new ConnectedListAdapter (getActivity(), connectedItemsArrayList_);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LinearLayoutManager llm = new LinearLayoutManager(mContext);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        clinicsList_rv.setLayoutManager(llm);
        clinicsList_rv.setItemAnimator(new DefaultItemAnimator());
        clinicsList_rv.setAdapter(connectedListAdapter);

        return convertView;
    }

}