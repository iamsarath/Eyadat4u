package app.ens.eyadat4u.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.adapters.ConnectedListAdapter;
import app.ens.eyadat4u.items.GalleryItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.items.ServicesItems;

/**
 * Created by Sarath on 1/30/2017.
 */

public class ServiceDetailsClinicListTab extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String                          mParam1;
    static Context                          mContext;
    RecyclerView                            clinicsList_rv;
    ConnectedListAdapter                    connectedListAdapter;
    public static ArrayList<SearchItems>    connectedItemsArrayList_       = new ArrayList<SearchItems>();


    public ServiceDetailsClinicListTab() {
        // Required empty public constructor
    }

    public static ServiceDetailsClinicListTab newInstance(Context context, ArrayList<SearchItems> connectedItemsArrayList) {
        ServiceDetailsClinicListTab fragment = new ServiceDetailsClinicListTab();

        mContext= context;
        connectedItemsArrayList_ = connectedItemsArrayList;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_service_details_clinic_detail_tab, container, false);
        clinicsList_rv   =(RecyclerView)convertView.findViewById(R.id.connected_list);

        try {
            connectedListAdapter = new ConnectedListAdapter (getActivity(), connectedItemsArrayList_);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LinearLayoutManager llm = new LinearLayoutManager(mContext);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        clinicsList_rv.setLayoutManager(llm);
        clinicsList_rv.setItemAnimator(new DefaultItemAnimator());
        clinicsList_rv.setAdapter(connectedListAdapter);
        return convertView;
    }

}