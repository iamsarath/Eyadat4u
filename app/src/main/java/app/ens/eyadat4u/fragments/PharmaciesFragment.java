package app.ens.eyadat4u.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.adapters.ServiceCategoriesAdapter;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 2/13/2017.
 */

public class PharmaciesFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String                  mParam1;
    static Context                  mContext;
    RecyclerView                    clinicsList_rv;
    ServiceCategoriesAdapter        clinicsListAdapter;

    public PharmaciesFragment() {
        // Required empty public constructor
    }

    public static PharmaciesFragment newInstance(Context context) {
        PharmaciesFragment fragment = new PharmaciesFragment();

        mContext= context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_home_tabs, container, false);

        try {
            clinicsListAdapter = new ServiceCategoriesAdapter(getActivity(), Constants.pharmaciesCategoryItemsArrayList, "Pharmacies");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        clinicsList_rv   =(RecyclerView)convertView.findViewById(R.id.clinics_list);

        GridLayoutManager lLayout;

        lLayout = new GridLayoutManager(getActivity(), 3);
        clinicsList_rv.setLayoutManager(lLayout);
        clinicsList_rv.setItemAnimator(new DefaultItemAnimator());
        clinicsList_rv.setAdapter(clinicsListAdapter);

        return convertView;
    }
}