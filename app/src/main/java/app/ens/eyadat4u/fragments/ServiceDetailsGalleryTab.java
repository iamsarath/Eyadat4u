package app.ens.eyadat4u.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.adapters.GalleryAdapter;
import app.ens.eyadat4u.adapters.ServiceCategoriesAdapter;
import app.ens.eyadat4u.items.FeedbackItems;
import app.ens.eyadat4u.items.GalleryItems;
import app.ens.eyadat4u.system.UserManager;

/**
 * Created by Sarath on 1/30/2017.
 */

public class ServiceDetailsGalleryTab extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String              mParam1;
    static Context              mContext;
    RecyclerView                gallery_rv;
    GalleryAdapter              galleryAdapter;
    public static LinearLayout  dataLayout_ll, noDataLayout_ll;
    public static ArrayList<GalleryItems> galleryItemsArrayList_    = new ArrayList<GalleryItems>();
    TextView                    noImageText_tv;

    public ServiceDetailsGalleryTab() {
        // Required empty public constructor
    }

    public static ServiceDetailsGalleryTab newInstance(Context context, ArrayList<GalleryItems> galleryItemsArrayList) {
        ServiceDetailsGalleryTab fragment = new ServiceDetailsGalleryTab();

        mContext= context;
        galleryItemsArrayList_ = galleryItemsArrayList;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_service_details_gallery_tab, container, false);


        try {

            galleryAdapter = new GalleryAdapter(getActivity(), galleryItemsArrayList_);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initViews(convertView);

        GridLayoutManager lLayout;

        lLayout = new GridLayoutManager(getActivity(), 1);
        gallery_rv.setLayoutManager(lLayout);
        gallery_rv.setItemAnimator(new DefaultItemAnimator());
        gallery_rv.setAdapter(galleryAdapter);

        if (galleryItemsArrayList_.size()==0){

            showNoDataView();
        }
        else{

            gallery_rv.setAdapter(galleryAdapter);
            showDataView();
        }


        return convertView;
    }

    private void initViews(View convertView) {

        gallery_rv              = (RecyclerView)convertView.findViewById(R.id.gallery_list);
        dataLayout_ll           = (LinearLayout) convertView.findViewById(R.id.data_view);
        noDataLayout_ll         = (LinearLayout) convertView.findViewById(R.id.no_data_view);
        noImageText_tv          = (TextView) convertView.findViewById(R.id.no_image_text);

        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(getActivity()).equals("en")){

            noImageText_tv.setText(R.string.no_image_text_en);
        }else{

            noImageText_tv.setText(R.string.no_image_text_ar);
        }
    }

    public void showDataView(){

        dataLayout_ll.setVisibility(View.VISIBLE);
        noDataLayout_ll.setVisibility(View.GONE);
    }

    public static void showNoDataView(){

        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.VISIBLE);
    }
}