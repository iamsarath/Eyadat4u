package app.ens.eyadat4u.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.GiveFeedback;
import app.ens.eyadat4u.activities.Login;
import app.ens.eyadat4u.activities.OnlinePediatrician;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.activities.ServiceList;
import app.ens.eyadat4u.adapters.FeedbackAdapter;
import app.ens.eyadat4u.adapters.GalleryAdapter;
import app.ens.eyadat4u.items.FeedbackItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UserManager;

/**
 * Created by Sarath on 1/30/2017.
 */

public class ServiceDetailsFeedbackTab extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String              mParam1;
    static Context              mContext;
    RecyclerView                feedbackList_rv;
    FeedbackAdapter             feedbackAdapter;
    TextView                    noFeedbackText_tv, beTheFirstText_tv, giveFeedbackText_tv;
    public static LinearLayout  dataLayout_ll, noDataLayout_ll, giveFeedbackLayout_1_ll, giveFeedbackLayout_2_ll;
    public static ArrayList<FeedbackItems> feedbackItemsArrayList_    = new ArrayList<FeedbackItems>();
    static String itemName_     = "";
    static String itemImage_    = "";
    static String itemId_       = "";

    public ServiceDetailsFeedbackTab() {

    }

    public static ServiceDetailsFeedbackTab newInstance(Context context, ArrayList<FeedbackItems> feedbackItemsArrayList, String itemName, String itemImage,
                                                        String itemId) {

        ServiceDetailsFeedbackTab fragment  = new ServiceDetailsFeedbackTab();
        mContext                            = context;
        feedbackItemsArrayList_             = feedbackItemsArrayList;
        itemName_                           = itemName;
        itemImage_                          = itemImage;
        itemId_                             = itemId;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_service_details_feedback_tab, container, false);

        try {

            feedbackAdapter = new FeedbackAdapter(getActivity(), feedbackItemsArrayList_);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initViews(convertView);

        //recyclerview
        LinearLayoutManager llm = new LinearLayoutManager(mContext);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        feedbackList_rv.setLayoutManager(llm);
        feedbackList_rv.setItemAnimator(new DefaultItemAnimator());

        if (feedbackItemsArrayList_.size()==0){

            showNoDataView();
        }
        else{

            feedbackList_rv.setAdapter(feedbackAdapter);
            showDataView();
        }

        giveFeedbackLayout_1_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserManager.getUserLoginStatus(getActivity())){

                    Intent intent = new Intent(getActivity(), GiveFeedback.class);
                    intent.putExtra("ITEM_NAME", itemName_);
                    intent.putExtra("ITEM_IMAGE", itemImage_);
                    intent.putExtra("ITEM_ID", itemId_);
                    mContext.startActivity(intent);
                }
                else{

                    showAlertDialogue("Login", "You need to login for giving feedbacks");
                }

            }
        });

        giveFeedbackLayout_2_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserManager.getUserLoginStatus(getActivity())){

                    Intent intent = new Intent(getActivity(), GiveFeedback.class);
                    intent.putExtra("ITEM_NAME", itemName_);
                    intent.putExtra("ITEM_IMAGE", itemImage_);
                    intent.putExtra("ITEM_ID", itemId_);
                    mContext.startActivity(intent);
                }
                else{

                    showAlertDialogue("Login", "You need to login for giving feedbacks");
                }
            }
        });

        return convertView;
    }

    private void initViews(View convertView) {

        feedbackList_rv         = (RecyclerView)convertView.findViewById(R.id.feedback_list);
        dataLayout_ll           = (LinearLayout) convertView.findViewById(R.id.data_view);
        noDataLayout_ll         = (LinearLayout) convertView.findViewById(R.id.no_data_view);
        giveFeedbackLayout_1_ll = (LinearLayout) convertView.findViewById(R.id.give_feedback_1);
        giveFeedbackLayout_2_ll = (LinearLayout) convertView.findViewById(R.id.give_feedback_2);
        noFeedbackText_tv       = (TextView) convertView.findViewById(R.id.no_feedback_text);
        beTheFirstText_tv       = (TextView) convertView.findViewById(R.id.be_the_first_to_text);
        giveFeedbackText_tv     = (TextView) convertView.findViewById(R.id.give_feedback_text);


        setTexts();
    }

    void setTexts(){

        if(UserManager.getUserLanguage(getActivity()).equals("en")){

            noFeedbackText_tv.setText(R.string.no_feedback_en);
            beTheFirstText_tv.setText(R.string.be_the_first_to_give_fb_en);
            giveFeedbackText_tv.setText(R.string.give_feedback_en);
        }else{

            noFeedbackText_tv.setText(R.string.no_feedback_ar);
            beTheFirstText_tv.setText(R.string.be_the_first_to_give_fb_ar);
            giveFeedbackText_tv.setText(R.string.give_feedback_ar);
        }
    }

    public void showDataView(){

        dataLayout_ll.setVisibility(View.VISIBLE);
        noDataLayout_ll.setVisibility(View.GONE);
    }

    public static void showNoDataView(){

        dataLayout_ll.setVisibility(View.GONE);
        noDataLayout_ll.setVisibility(View.VISIBLE);
    }

    /* alert dialogue  */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent myIntent = new Intent(getActivity(), Login.class);
                        startActivity(myIntent);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }
}