package app.ens.eyadat4u.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.Login;
import app.ens.eyadat4u.activities.MainActivity;
import app.ens.eyadat4u.activities.MyAccount;
import app.ens.eyadat4u.activities.SignUp;
import app.ens.eyadat4u.adapters.ServiceCategoriesAdapter;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

/**
 * Created by Sarath on 2/21/2017.
 */
public class MyProfileFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String          mParam1;
    static Context          mContext;
    RequestQueue            queue;
    JsonObjectRequest       jsonObjectRequest;
    EditText                fullName_et, phone_et, password_et, rePassword_et;
    TextView                name_tv, email_tv, fullnameText_tv, mobileText_tv, chanagePassword_tv, updateProfileText_tv;
    LinearLayout            userLayout_ll, guestLayout_ll, signIn_ll, update_ll, bottomLayout_ll, chagePwd_ll,
                            passwordLayout_ll, rePasswordLayout_ll, editLayout_ll;
    ProgressDialog          progressdialog;

    Point p;
    PopupWindow popup;
    String fullName_, phone_, password_, rePassword_, tempUserId;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    public static MyProfileFragment newInstance(Context context) {
        MyProfileFragment fragment = new MyProfileFragment();

        mContext= context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        int[] location = new int[2];
        p = new Point();
        p.x = location[0];
        p.y = location[1];

        queue    = Volley.newRequestQueue(getActivity());

        initViews(convertView);
        validateLoginStatus();

        editLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                passwordLayout_ll.setVisibility(View.VISIBLE);
//                rePasswordLayout_ll.setVisibility(View.VISIBLE);
                bottomLayout_ll.setVisibility(View.VISIBLE);
                editLayout_ll.setVisibility(View.GONE);
                chanagePassword_tv.setVisibility(View.VISIBLE);
            }
        });

        chagePwd_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showChngPwdPopup(getActivity(), p, UserManager.getUserId(getActivity()));
            }
        });

        signIn_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentSignout = new Intent(getActivity(), Login.class);
                startActivity(intentSignout);
            }
        });

        update_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fullName_   = fullName_et.getText().toString();
                phone_      = phone_et.getText().toString();
//                password_   = password_et.getText().toString();
//                rePassword_ = rePassword_et.getText().toString();

                if ((fullName_ != null && fullName_.equals("")) ||
                        (phone_ != null && phone_.equals(""))) {

                        showSnackBar("Please fill all the fields !");

                } else {

                        progressdialog = new ProgressDialog(getActivity());
                        progressdialog.setMessage("Creating please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = updateProfileObj(UserManager.getUserApiKey(getActivity()), UserManager.getUserId(getActivity()), fullName_, password_, phone_, UserManager.getUserLanguage(getActivity()));

                        updateProfileApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_UPDATE_PROFILE), obj);
                }
            }
        });

                return convertView;
    }

    private void validateLoginStatus() {

        if(UserManager.getUserLoginStatus(getActivity())){

            guestLayout_ll.setVisibility(View.GONE);
            userLayout_ll.setVisibility(View.VISIBLE);
            bottomLayout_ll.setVisibility(View.VISIBLE);
            chanagePassword_tv.setVisibility(View.VISIBLE);
        }
        else{

            guestLayout_ll.setVisibility(View.VISIBLE);
            userLayout_ll.setVisibility(View.GONE);
            bottomLayout_ll.setVisibility(View.GONE);
            chanagePassword_tv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        validateLoginStatus();
        updateViews();
    }

    private void initViews(View view) {

        fullName_et             = (EditText) view.findViewById(R.id.fullname);
        phone_et                = (EditText) view.findViewById(R.id.mobile);
        password_et             = (EditText) view.findViewById(R.id.password);
        rePassword_et           = (EditText) view.findViewById(R.id.re_password);
        email_tv                = (TextView) view.findViewById(R.id.email);
        name_tv                 = (TextView) view.findViewById(R.id.name);

        fullnameText_tv         = (TextView) view.findViewById(R.id.name_text);
        mobileText_tv           = (TextView) view.findViewById(R.id.mobile_text);
        updateProfileText_tv    = (TextView) view.findViewById(R.id.update_profile_text);

        userLayout_ll           = (LinearLayout) view.findViewById(R.id.user_layout);
        guestLayout_ll          = (LinearLayout) view.findViewById(R.id.guest_layout);
        signIn_ll               = (LinearLayout) view.findViewById(R.id.signin_layout);
        update_ll               = (LinearLayout) view.findViewById(R.id.update_layout);
        bottomLayout_ll         = (LinearLayout) view.findViewById(R.id.bottom_layout);
        passwordLayout_ll       = (LinearLayout) view.findViewById(R.id.password_layout);
        rePasswordLayout_ll     = (LinearLayout) view.findViewById(R.id.re_password_layout);
        editLayout_ll           = (LinearLayout) view.findViewById(R.id.edit_layout);
        chanagePassword_tv      = (TextView) view.findViewById(R.id.change_password);
        chagePwd_ll             = (LinearLayout) view.findViewById(R.id.change_password_layout);

        chanagePassword_tv.setPaintFlags(chanagePassword_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        updateViews();
    }

    void updateViews(){

        fullName_et.setText(UserManager.getUserName(getActivity()));
        phone_et.setText(UserManager.getUserPhone(getActivity()));
        name_tv.setText(UserManager.getUserName(getActivity()));
        email_tv.setText(UserManager.getUserEmail(getActivity()));
        password_et.setText("");
        rePassword_et.setText("");
        passwordLayout_ll.setVisibility(View.GONE);
        rePasswordLayout_ll.setVisibility(View.GONE);
        editLayout_ll.setVisibility(View.GONE);
        chanagePassword_tv.setVisibility(View.VISIBLE);

        if(UserManager.getUserLanguage(getActivity()).equals("en")){

            fullnameText_tv.setText(getResources().getString(R.string.name_text_en));
            mobileText_tv.setText(getResources().getString(R.string.phone_text_en));
            chanagePassword_tv.setText(getResources().getString(R.string.change_pwd_en));
            updateProfileText_tv.setText(getResources().getString(R.string.update_profile_en));
        }
        else {

            fullnameText_tv.setText(getResources().getString(R.string.name_text_ar));
            mobileText_tv.setText(getResources().getString(R.string.phone_text_ar));
            chanagePassword_tv.setText(getResources().getString(R.string.change_pwd_ar));
            updateProfileText_tv.setText(getResources().getString(R.string.update_profile_ar));
        }
    }

//    showChngPwdPopup(SignUp.this, p, userId_, email_otp_);

    // The method that displays the popup.
    private void showChngPwdPopup(final Activity context, Point p, final String userId) {

        final int TIME_OUT  = 1500;

        String userId_ = userId;

        final WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        int popupWidth = width;
        int popupHeight = height;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout                                 = layoutInflater.inflate(R.layout.popup_change_password, viewGroup);
        ImageView closePopup_iv                     = (ImageView) layout.findViewById(R.id.close_popup);
        final EditText pwdNew_et                    = (EditText) layout.findViewById(R.id.password_new);
        LinearLayout submitNewPwd_ll                = (LinearLayout) layout.findViewById(R.id.update_pwd_layout);
        TextView changePwdText_tv                   = (TextView) layout.findViewById(R.id.change_pwd_text);
        TextView submitText_tv                      = (TextView) layout.findViewById(R.id.submit_text);

        if(UserManager.getUserLanguage(getActivity()).equals("en")){

            changePwdText_tv.setText(R.string.change_pwd_en);
            submitText_tv.setText(R.string.submit_en);
            pwdNew_et.setHint(R.string.new_pwd_en);
        }
        else {

            changePwdText_tv.setText(R.string.change_pwd_ar);
            submitText_tv.setText(R.string.submit_ar);
            pwdNew_et.setHint(R.string.new_pwd_ar);
        }

        submitNewPwd_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((pwdNew_et.getText().toString() != null && pwdNew_et.getText().toString().equals(""))) {

                    showSnackBar("Please enter your new password.");

                } else {

                        progressdialog = new ProgressDialog(getActivity());
                        progressdialog.setMessage("Creating please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = changePswdObj(UserManager.getUserId(getActivity()), UserManager.getUserApiKey(getActivity()), pwdNew_et.getText().toString());
                        changePwdApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_CHANGE_PASSWORD), obj);                }

            }
        });

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 10;
        int OFFSET_Y = 10;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        closePopup_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.dismiss();
            }
        });

    }

    private String changePswdObj(String userId, String apiKey, String pwdNew) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "user_id", userId);
            mainObj.put( "api_key", apiKey);
            mainObj.put( "password", pwdNew);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void changePwdApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(getActivity()))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            if(jObj.has("message")){
                                    showSnackBar(jObj.getString("message"));
                                }
                                else{
                                }
                            progressdialog.dismiss();
                            popup.dismiss();

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    private String updateProfileObj(String apiKey, String userId, String fullname,  String password, String mobile, String language) {

        JSONObject mainObj = new JSONObject();
        try {

            mainObj.put( "api_key", apiKey);
            mainObj.put( "user_id", userId);
            mainObj.put( "name_surname", fullname);
            mainObj.put( "phone", mobile);
            mainObj.put( "language", language );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }

    public void updateProfileApi(String url, String obj){

        if(!CheckNetwork.isInternetAvailable(getActivity()))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        String userId_          = null;
                        String name_            = null;
                        String userName_        = null;
                        String email_           = null;
                        String phone_           = null;

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            //Validating the api
                            if(jObj.has("status")){

                                if(jObj.getBoolean("status")) {

                                    if(jObj.has("user_id")) {

                                        userId_= jObj.getString("user_id");
                                        tempUserId = userId_;
                                    }
                                    if(jObj.has("name_surname")) {

                                        name_= jObj.getString("name_surname");
                                    }
                                    if(jObj.has("username")) {

                                        userName_= jObj.getString("username");
                                    }
                                    if(jObj.has("email")) {

                                        email_= jObj.getString("email");
                                    }
                                    if(jObj.has("mobile")) {

                                        phone_= jObj.getString("mobile");
                                    }

                                    progressdialog.dismiss();
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                    }

                                    UserManager.updateUserStatus(getActivity(), userId_,  name_, userName_, email_, phone_, UserManager.getUserApiKey(getActivity()));
                                    updateViews();
                                    progressdialog.dismiss();

                                } else{

                                    progressdialog.dismiss();
//                                    showSnackBar(jObj.getString("message"));
                                }
                            }
                            else{

                                if(jObj.has("message")){

                                    showSnackBar(jObj.getString("message"));
                                }
                                else{
                                }

                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(MyAccount.parentLayout_ll, ""+msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}