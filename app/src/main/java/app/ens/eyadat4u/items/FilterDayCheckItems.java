package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 3/3/2017.
 */

public class FilterDayCheckItems {

    public String   day_;
    public boolean  is_checked_;

    public FilterDayCheckItems(String day, boolean is_checked) {

        day_            = day;
        is_checked_     = is_checked;
    }
}