package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 03/03/2017.
 */

public class ChatMessage {

    public String is_sender_;
    public String message_;
    public String time_;

    public ChatMessage(String is_sender, String message, String time) {
        super();

        this.is_sender_ = is_sender;
        this.message_   = message;
        this.time_      = time;
    }
}