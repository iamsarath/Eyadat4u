package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/22/2017.
 */

public class NewsAndEventsItems {

    public String   news_id_;
    public String   date_;
    public String   title_;
    public String   description_;
    public String   news_image_;

    public NewsAndEventsItems(String news_id, String date, String title, String description, String news_image) {

        news_id_        = news_id;
        date_           = date;
        title_          = title;
        description_    = description;
        news_image_     = news_image;
    }
}
