package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/15/2017.
 */

public class GalleryItems {

    public String   image_id_;
    public String   image_url_;

    public GalleryItems(String image_id, String image_url) {

        image_id_       = image_id;
        image_url_      = image_url;
    }
}
