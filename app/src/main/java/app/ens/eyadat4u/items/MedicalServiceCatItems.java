package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/27/2017.
 */
public class MedicalServiceCatItems {

    public String   cat_id_;
    public String   cat_name_;

    public MedicalServiceCatItems(String cat_id, String cat_name) {

        cat_id_       = cat_id;
        cat_name_     = cat_name;
    }
}
