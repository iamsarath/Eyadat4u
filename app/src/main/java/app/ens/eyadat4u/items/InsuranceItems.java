package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/22/2017.
 */

public class InsuranceItems {

    public String   insurance_id_;
    public String   title_;
    public String   description_;
    public String   amount_;
    public String   eligibility_;
    public String   benefit_;
    public String   renewal_policy_;
    public String   exclusion_;
    public String   company_name_;
    public String   image_;

    public InsuranceItems(String insurance_id, String title, String description, String amount, String eligibility, String benefit,
                          String renewal_policy, String exclusion, String company_name, String image) {

        insurance_id_   = insurance_id;
        title_          = title;
        description_    = description;
        amount_         = amount;
        eligibility_    = eligibility;
        benefit_        = benefit;
        renewal_policy_ = renewal_policy;
        exclusion_      = exclusion;
        company_name_   = company_name;
        image_          = image;

    }
}