package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/15/2017.
 */

public class DetailItems {

    public String   item_id_;
    public String   item_name_;
    public String   item_thumb_;
    public String   location_;
    public String   qualification_;
    public String   experience_;
    public String   views_;
//    public String   clinic_name_;

    public DetailItems(String item_id, String item_name, String item_thumb, String location, String experience, String views) {

        item_id_        = item_id;
        item_name_      = item_name;
        item_thumb_     = item_thumb;
        location_       = location;
        experience_     = experience;
        views_          = views;
    }
}