package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 1/6/2018.
 */

public class MedicineItems {

    public String   id_;
    public String   clinicalservice_id_;
    public String   added_date_;
    public String   name_;
    public String   price_;
    public String   desc_;

    public MedicineItems(String id, String clinicalservice_id, String added_date, String name, String price, String desc) {

        id_                     = id;
        clinicalservice_id_     = clinicalservice_id;
        added_date_             = added_date;
        name_                   = name;
        price_                  = price;
        desc_                   = desc;
    }
}