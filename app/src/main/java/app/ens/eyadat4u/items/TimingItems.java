package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/15/2017.
 */

public class TimingItems {

    public String   days_;
    public String   timing_;
    public String   item_name_;

    public TimingItems(String days, String timing) {

        days_           = days;
        timing_         = timing;
    }
}
