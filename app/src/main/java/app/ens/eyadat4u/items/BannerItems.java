package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 5/11/2017.
 */

public class BannerItems {

    public String   banner_image_;
    public String   banner_text_;
    public String   banner_url_;

    public BannerItems(String banner_image, String banner_text, String banner_url) {

        banner_image_       = banner_image;
        banner_text_        = banner_text;
        banner_url_         = banner_url;
    }
}
