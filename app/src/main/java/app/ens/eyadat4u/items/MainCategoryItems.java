package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/13/2017.
 */

public class MainCategoryItems {

    public String   service_id_;
    public String   service_name_;
    public String   icon_;
    public String   service_type_;
    public String   custom_search_;

    public MainCategoryItems(String service_id, String service_name, String icon, String service_type, String custom_search) {

        service_id_         = service_id;
        service_name_       = service_name;
        icon_               = icon;
        service_type_       = service_type;
        custom_search_      = custom_search;
    }
}
