package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 3/2/2017.
 */

public class CommonSearchItems {

    public String   item_id_;
    public String   item_name_;
    public String   item_type_;
    public String   service_type_;
    public String   custom_search_;

    public CommonSearchItems(String item_id, String item_name, String item_type, String service_type, String custom_search) {

        item_id_        = item_id;
        item_name_      = item_name;
        item_type_      = item_type;
        service_type_   = service_type;
        custom_search_  = custom_search;
    }
}

