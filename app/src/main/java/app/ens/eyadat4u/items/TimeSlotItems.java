package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/16/2017.
 */
public class TimeSlotItems {

    public String   time_slot_id_;
    public String   time_slot_;

    public TimeSlotItems(String time_slot_id, String time_slot) {

        time_slot_id_   = time_slot_id;
        time_slot_      = time_slot;
    }
}
