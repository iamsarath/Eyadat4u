package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/20/2017.
 */

public class MyAppointmentItems {


    public String   appointment_id_;
    public String   appointment_date_;
    public String   appointment_from_time_;
    public String   appointment_to_time_;
    public String   type_;
    public String   item_id_;
    public String   item_name_;
    public String   item_thumb_;
    public String   clinic_name_;
    public String   location_;
    public String   qualification_;
    public String   experience_;
    public String   rating_;
    public String   views_;
//    public String   clinic_name_;

    public MyAppointmentItems(String appointment_id, String appointment_date, String appointment_from_time, String appointment_to_time, String type, String item_id, String item_name, String item_thumb, String clinic_name, String location, String qualification, String experience, String rating, String views) {

        appointment_id_         = appointment_id;
        appointment_date_       = appointment_date;
        appointment_from_time_  = appointment_from_time;
        appointment_to_time_    = appointment_to_time;
        type_                   = type;
        item_id_                = item_id;
        item_name_              = item_name;
        item_thumb_             = item_thumb;
        clinic_name_            = clinic_name;
        location_               = location;
        qualification_          = qualification;
        experience_             = experience;
        rating_                 = rating;
        views_                  = views;
    }
}

