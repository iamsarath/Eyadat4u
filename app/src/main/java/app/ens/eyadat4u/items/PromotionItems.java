package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/22/2017.
 */

public class PromotionItems {

    public String   promo_id_;
    public String   start_date_;
    public String   end_date_;
    public String   title_;
    public String   description_;
    public String   promo_image_;

    public PromotionItems(String promo_id, String start_date, String end_date, String title, String description, String promo_image) {

        promo_id_        = promo_id;
        start_date_      = start_date;
        end_date_        = end_date;
        title_           = title;
        description_     = description;
        promo_image_     = promo_image;
    }
}
