package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/27/2017.
 */

public class MedicalJobsItems {

    public String   job_id_;
    public String   job_name_;

    public MedicalJobsItems(String job_id, String job_name) {

        job_id_       = job_id;
        job_name_     = job_name;
    }
}
