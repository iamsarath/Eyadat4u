package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 3/4/2017.
 */

public class FilterTimeCheckItems {

    public String   time_;
    public boolean  is_checked_;

    public FilterTimeCheckItems(String time, boolean is_checked) {

        time_           = time;
        is_checked_     = is_checked;
    }
}