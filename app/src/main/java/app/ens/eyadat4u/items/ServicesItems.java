package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/15/2017.
 */

public class ServicesItems {

    public String   service_id_;
    public String   service_name_;

    public ServicesItems(String service_id, String service_name) {

        service_id_             = service_id;
        service_name_           = service_name;
    }
}
