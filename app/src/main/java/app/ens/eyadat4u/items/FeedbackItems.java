package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 3/10/2017.
 */

public class FeedbackItems {

    public String   feedback_id_;
    public String   title_;
    public String   message_;
    public String   user_name_;
    public String   rating_;

    public FeedbackItems(String feedback_id, String title, String message, String user_name, String rating) {

        feedback_id_    = feedback_id;
        title_          = title;
        message_        = message;
        user_name_      = user_name;
        rating_         = rating;
    }
}