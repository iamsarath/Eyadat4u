package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 2/13/2017.
 */

public class SearchItems {

    public String   type_name_;
    public String   type_;
    public String   item_id_;
    public String   item_name_;
    public String   item_thumb_;
    public String   clinic_name_;
    public String   location_;
    public String   qualification_;
    public String   experience_;
    public String   rating_;
    public String   views_;
//    public String   clinic_name_;

    public SearchItems(String type_name, String type, String item_id, String item_name, String item_thumb, String clinic_name, String location, String qualification, String experience,  String rating, String views) {

        type_name_      = type_name;
        type_           = type;
        item_id_        = item_id;
        item_name_      = item_name;
        item_thumb_     = item_thumb;
        clinic_name_    = clinic_name;
        location_       = location;
        qualification_  = qualification;
        experience_     = experience;
        rating_         = rating;
        views_          = views;
    }
}
