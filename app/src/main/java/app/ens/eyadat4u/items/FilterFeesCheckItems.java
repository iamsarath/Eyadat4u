package app.ens.eyadat4u.items;

/**
 * Created by Sarath on 3/4/2017.
 */

public class FilterFeesCheckItems {

    public String   fees_;
    public boolean  is_checked_;

    public FilterFeesCheckItems(String fees, boolean is_checked) {

        fees_           = fees;
        is_checked_     = is_checked;
    }
}