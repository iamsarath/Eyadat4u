package app.ens.eyadat4u.system;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 11/13/2016.
 */

public class UILHandler extends Application {
    DisplayImageOptions optionsNormal, optionsRoundEdgeSmall,optionsRoundEdgeMedium, optionsRoundEdgeLarge, optionsRoundEdgeExtraLarge;
    ImageLoaderConfiguration config;
    ImageLoader imageLoader;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
//                .writeDebugLogs() // Remove for release app
                .build();

        optionsNormal = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .displayer(new SimpleBitmapDisplayer()).build();

        optionsRoundEdgeSmall = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .displayer(new RoundedBitmapDisplayer(35)).build();

        optionsRoundEdgeMedium= new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .displayer(new RoundedBitmapDisplayer(50)).build();

        optionsRoundEdgeLarge = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .displayer(new RoundedBitmapDisplayer(110)).build();

        optionsRoundEdgeExtraLarge = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .displayer(new RoundedBitmapDisplayer(125)).build();

        imageLoader.getInstance().init(config); //Initialize ImageLoader with configuration.
    }

    @Override
    public void onLowMemory() {
        // TODO Auto-generated method stub
        super.onLowMemory();
        imageLoader.getInstance().clearMemoryCache();
        imageLoader.getInstance().clearDiscCache();
    }


    public void loadImage(String url, ImageView imageView, String imageSpecifier){

        imageLoader = ImageLoader.getInstance();

        if(imageSpecifier.equals(Constants.imageSpecifier.NORMAL))
            imageLoader.displayImage(url, imageView, optionsNormal);

        else if(imageSpecifier.equals(Constants.imageSpecifier.ROUND_EDGE_SMALL))
            imageLoader.displayImage(url, imageView, optionsRoundEdgeSmall);

        else if(imageSpecifier.equals(Constants.imageSpecifier.ROUND_EDGE_MEDIUM))
            imageLoader.displayImage(url, imageView, optionsRoundEdgeMedium);

        else if(imageSpecifier.equals(Constants.imageSpecifier.ROUND_EDGE_LARGE))
            imageLoader.displayImage(url, imageView, optionsRoundEdgeLarge);

        else if(imageSpecifier.equals(Constants.imageSpecifier.ROUND_EDGE_EXTRA_LARGE))
            imageLoader.displayImage(url, imageView, optionsRoundEdgeExtraLarge);
    }

    public void loadImage(String url, final LinearLayout layout, String imageSpecifier){

        imageLoader = ImageLoader.getInstance();

        if(imageSpecifier.equals(Constants.imageSpecifier.LAYOUT_NORMAL)){

            imageLoader.loadImage(url, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    layout.setBackgroundDrawable(new BitmapDrawable(loadedImage));
                }
            });
        }
    }

}
