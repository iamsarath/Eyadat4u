package app.ens.eyadat4u.system;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sarath on 08/02/2017.
 */
public class UserManager {

    static SharedPreferences pref;
    static SharedPreferences.Editor editor;

    public static boolean getUserLoginStatus(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserLoginStatus", 0);

        if(pref.getString("login_status", "false").equals("false")){

            return false;
        }
        else{
            return true;
        }
    }

    public static String getUserId(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserData", 0);

        return pref.getString("user_id","");
    }

    public static String getUserName(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserData", 0);

        return pref.getString("name","");
    }

    public static String getUserEmail(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserData", 0);

        return pref.getString("email","");
    }

    public static String getUserPhone(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserData", 0);

        return pref.getString("phone","");
    }

    public static String getUserApiKey(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserData", 0);

        return pref.getString("session_key","");
    }


    public static void updateUserStatus(Context context, String userId, String name, String userName,
                                       String userEmail, String userPhone, String sessionKey){

       pref = context.getApplicationContext().getSharedPreferences("UserData", 0);
       editor = pref.edit();
       editor.putString("user_id", userId); // Storing user id
       editor.putString("name", name); // Storing user name
       editor.putString("user_name", userName); // Storing user name
       editor.putString("email", userEmail); // Storing user email
       editor.putString("phone", userPhone); // Storing user phone
       editor.putString("session_key", sessionKey); // Storing user id
       editor.commit();
    }

    public static void updateUserLoginStatus(Context context, String status){

        pref = context.getApplicationContext().getSharedPreferences("UserLoginStatus", 0);
        editor = pref.edit();
        editor.putString("login_status", status); // Storing user login status
        editor.commit();
    }

    /****************************User Language***************************/

    public static String getUserLanguage(Context context){

        pref = context.getApplicationContext().getSharedPreferences("UserLanguage", 0);

        if(pref.getString("language", "en").equals("en")){

            return "en";
        }
        else{

            return "ar";
        }
    }

    public static void updateUserLanguage(Context context, String status){

        pref = context.getApplicationContext().getSharedPreferences("UserLanguage", 0);
        editor = pref.edit();
        editor.putString("language", status); // Storing lang preference
        editor.commit();
    }

    /****************************User Email***************************/

//    public static boolean getUserEmailStatus(Context context){
//
//        pref = context.getApplicationContext().getSharedPreferences("UserEmail", 0);
//
//        if(pref.getBoolean("email_status", false)){
//
//            return false;
//        }
//        else{
//            return true;
//        }
//    }
//
//    public static void updateUserEailStatus(Context context, boolean email_status){
//
//        pref = context.getApplicationContext().getSharedPreferences("UserEmail", 0);
//        editor = pref.edit();
//        editor.putBoolean("email_status", email_status); // Storing user login status
//        editor.commit();
//    }

    /****************************User Phone***************************/

    public static void updateUserPhoneStatus(Context context, String email_status){

        pref = context.getApplicationContext().getSharedPreferences("UserPhone", 0);
        editor = pref.edit();
        editor.putBoolean("email_status", true); // Storing user login status
        editor.commit();
    }


}
