package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.ServiceList;
import app.ens.eyadat4u.items.CommonSearchItems;
import app.ens.eyadat4u.system.UILHandler;

/**
 * Created by Sarath on 3/2/2017.
 */
public class SearchAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater layoutInflater;
    ArrayList<CommonSearchItems> searchItemsListArrayList_  = new ArrayList<CommonSearchItems>();

    public SearchAdapter(Context context, ArrayList<CommonSearchItems> searchItemsListArrayList){
        mContext                    = context;
        layoutInflater              = LayoutInflater.from(context);
        searchItemsListArrayList_   = searchItemsListArrayList;

    }

    @Override
    public int getCount() {
        return searchItemsListArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_search, null);

            lastPosition = position;

            holder = new ViewHolder();
            holder.mHandler = new Handler();

            holder.name_tv  = (TextView) convertView.findViewById(R.id.name);
            holder.type_tv  = (TextView) convertView.findViewById(R.id.type);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {


                holder.name_tv.setText(searchItemsListArrayList_.get(pos).item_name_);
                holder.type_tv.setText(searchItemsListArrayList_.get(pos).item_type_);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ServiceList.class);
                intent.putExtra("TITLE", "Search");
                intent.putExtra("SUB_TITLE", searchItemsListArrayList_.get(pos).item_name_);
                intent.putExtra("SERVICE_ID", searchItemsListArrayList_.get(pos).item_id_);
                intent.putExtra("SERVICE_TYPE", searchItemsListArrayList_.get(pos).service_type_);
                intent.putExtra("CUSTOM_SEARCH", searchItemsListArrayList_.get(pos).custom_search_);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }
    static class ViewHolder {

        TextView    name_tv, type_tv;
        Handler     mHandler;
    }
}