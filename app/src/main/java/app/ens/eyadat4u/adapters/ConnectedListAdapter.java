package app.ens.eyadat4u.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.Login;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.activities.ServiceList;
import app.ens.eyadat4u.activities.TimeSlotPicker;
import app.ens.eyadat4u.items.MainCategoryItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 2/15/2017.
 */

public class ConnectedListAdapter extends RecyclerView.Adapter<ConnectedListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<SearchItems> connectedItemsItemsArrayList_  = new ArrayList<SearchItems>();

    public ConnectedListAdapter(Context context, ArrayList<SearchItems> connectedItemsItemsArrayList) throws JSONException {
        this.mContext                   = context;
        connectedItemsItemsArrayList_   = connectedItemsItemsArrayList;
    }

    @Override
    public int getItemCount() {

        return connectedItemsItemsArrayList_.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service_list, parent, false);

        return new ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {



        holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();
        holder.name_tv.setVisibility(View.GONE);
        holder.qualification_tv.setVisibility(View.GONE);
        holder.clinicName_tv.setVisibility(View.GONE);
        holder.detailslayout_ll.setVisibility(View.GONE);
        holder.bookAppointment_ll.setVisibility(View.GONE);
        holder.servicesLayout_ll.setVisibility(View.GONE);

        String docText, clinicText;

        if(UserManager.getUserLanguage(mContext).equals("en")){

            docText     = "DOCTOR";
            clinicText  = "CLINIC";
        }else{

            docText     = "طبيب";
            clinicText  = "عيادة";
        }


        if(UserManager.getUserLanguage(mContext).equals("en")){

            holder.viewDetailsText_tv.setText(R.string.view_details_en);
            holder.bookAppointment_tv.setText(R.string.book_appointment_en);

        }else{

            holder.viewDetailsText_tv.setText(R.string.view_details_ar);
            holder.bookAppointment_tv.setText(R.string.book_appointment_ar);
        }

        if(connectedItemsItemsArrayList_.get(position).type_.equals(docText)){

            holder.name_tv.setVisibility(View.VISIBLE);
            holder.qualification_tv.setVisibility(View.VISIBLE);
            holder.clinicName_tv.setVisibility(View.VISIBLE);
            holder.bookAppointment_ll.setVisibility(View.VISIBLE);
            holder.detailslayout_ll.setVisibility(View.VISIBLE);
            holder.servicesLayout_ll.setVisibility(View.GONE);
        }
        else{

            holder.name_tv.setVisibility(View.VISIBLE);
            holder.qualification_tv.setVisibility(View.GONE);
            holder.clinicName_tv.setVisibility(View.GONE);
            holder.bookAppointment_ll.setVisibility(View.GONE);
            holder.detailslayout_ll.setVisibility(View.VISIBLE);
            holder.servicesLayout_ll.setVisibility(View.GONE);
        }

        if(UserManager.getUserLanguage(mContext).equals("en")){
            holder.qualification_tv.setText(connectedItemsItemsArrayList_.get(position).qualification_+" - "+
                    connectedItemsItemsArrayList_.get(position).experience_+" yrs exp");
        }else{
            holder.qualification_tv.setText(connectedItemsItemsArrayList_.get(position).qualification_+" - "+
                    connectedItemsItemsArrayList_.get(position).experience_+" سنوات إكس");
        }

        holder.specialityOrService_tv.setText(connectedItemsItemsArrayList_.get(position).type_name_);
        holder.name_tv.setText(trimItemName(connectedItemsItemsArrayList_.get(position).item_name_));

        holder.services_tv.setText("");
        holder.clinicName_tv.setText(connectedItemsItemsArrayList_.get(position).clinic_name_);
        holder.location_tv.setText(connectedItemsItemsArrayList_.get(position).location_);
        holder.rating_tv.setText(connectedItemsItemsArrayList_.get(position).rating_);
        holder.views_tv.setText(connectedItemsItemsArrayList_.get(position).views_);

        if(connectedItemsItemsArrayList_.get(position).rating_ != null){

            float ratingStarts = Float.parseFloat(connectedItemsItemsArrayList_.get(position).rating_);
            holder.ratingBar_rb.setRating(ratingStarts);
        }

//                holder.itemImage_iv.setBackgroundResource(imageList[pos]);
        holder.uilHandler.loadImage(connectedItemsItemsArrayList_.get(position).item_thumb_,
                holder.itemImage_iv , Constants.imageSpecifier.NORMAL);

        holder.detailslayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( connectedItemsItemsArrayList_.get(position).type_.equals("DOCTOR")){

                    Intent intent = new Intent(mContext, ServiceDetails.class);
                    intent.putExtra("KEY_TYPE", "DOCTOR");
                    intent.putExtra("ITEM_NAME", connectedItemsItemsArrayList_.get(position).item_name_);
                    intent.putExtra("CLINIC_NAME", connectedItemsItemsArrayList_.get(position).clinic_name_);
                    intent.putExtra("ITEM_PIC", connectedItemsItemsArrayList_.get(position).item_thumb_);
                    intent.putExtra("ITEM_ID", connectedItemsItemsArrayList_.get(position).item_id_);
                    mContext.startActivity(intent);
                }
                else{

                    Intent intent = new Intent(mContext, ServiceDetails.class);
                    intent.putExtra("KEY_TYPE", "CLINIC");
                    intent.putExtra("ITEM_NAME", connectedItemsItemsArrayList_.get(position).item_name_);
                    intent.putExtra("CLINIC_NAME", connectedItemsItemsArrayList_.get(position).clinic_name_);
                    intent.putExtra("ITEM_PIC", connectedItemsItemsArrayList_.get(position).item_thumb_);
                    intent.putExtra("ITEM_ID", connectedItemsItemsArrayList_.get(position).item_id_);
                    mContext.startActivity(intent);
                }
            }
        });

        holder.bookAppointment_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserManager.getUserLoginStatus(mContext)){

                    Intent myIntent = new Intent(mContext, TimeSlotPicker.class);
                    myIntent.putExtra("ITEM_ID",  connectedItemsItemsArrayList_.get(position).item_id_);
                    myIntent.putExtra("ITEM_NAME",  connectedItemsItemsArrayList_.get(position).item_name_);
                    myIntent.putExtra("CLINIC_NAME",  connectedItemsItemsArrayList_.get(position).clinic_name_);
                    myIntent.putExtra("ITEM_PIC",   connectedItemsItemsArrayList_.get(position).item_thumb_);
                    myIntent.putExtra("LOCATION",  connectedItemsItemsArrayList_.get(position).location_);
                    mContext.startActivity(myIntent);
                }
                else{

                    showAlertDialogue("Login", "You need to login for booking appointments");
                }

            }
        });

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout    detailslayout_ll, bookAppointment_ll, servicesLayout_ll;
        TextView        name_tv, qualification_tv, services_tv, clinicName_tv, location_tv, rating_tv, views_tv,
                        specialityOrService_tv, viewDetailsText_tv, bookAppointment_tv;
        ImageView       itemImage_iv;
        RatingBar       ratingBar_rb;
        UILHandler      uilHandler;

        public ViewHolder(View convertView) {
            super(convertView);

            detailslayout_ll         = (LinearLayout) convertView.findViewById(R.id.details);
            bookAppointment_ll       = (LinearLayout) convertView.findViewById(R.id.book_appointment);
            servicesLayout_ll        = (LinearLayout) convertView.findViewById(R.id.specialisation_layout);
            name_tv                  = (TextView) convertView.findViewById(R.id.name);
            qualification_tv         = (TextView) convertView.findViewById(R.id.qualif_exp);
            services_tv              = (TextView) convertView.findViewById(R.id.clinic_services);
            clinicName_tv            = (TextView) convertView.findViewById(R.id.clinic_name);
            location_tv              = (TextView) convertView.findViewById(R.id.location);
            itemImage_iv             = (ImageView) convertView.findViewById(R.id.item_image);
            rating_tv                = (TextView) convertView.findViewById(R.id.rating);
            views_tv                 = (TextView) convertView.findViewById(R.id.views);
            specialityOrService_tv   = (TextView) convertView.findViewById(R.id.speciality_service);
            ratingBar_rb             = (RatingBar) convertView.findViewById(R.id.rating_bar);
            viewDetailsText_tv       = (TextView) convertView.findViewById(R.id.view_details_text);
            bookAppointment_tv       = (TextView) convertView.findViewById(R.id.book_appointment_text);

            LayerDrawable stars = (LayerDrawable) ratingBar_rb .getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.Yellow), PorterDuff.Mode.SRC_ATOP);
        }
    }

    public String trimItemName(String text){

        if(text.length() >= 15){
            String s = text.substring(0,15);
            return s+"..";
        }
        else{

            return text;
        }
    }

    /* alert dialogue for cart item removal confirmation */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent myIntent = new Intent(mContext, Login.class);
                        mContext.startActivity(myIntent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }

}


