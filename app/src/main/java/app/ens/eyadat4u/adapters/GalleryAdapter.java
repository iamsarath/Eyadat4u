package app.ens.eyadat4u.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.ServiceList;
import app.ens.eyadat4u.items.GalleryItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 2/2/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GalleryItems> galleryItemsArrayList_        = new ArrayList<GalleryItems>();

    public GalleryAdapter(Context context, ArrayList<GalleryItems> galleryItemsArrayList) throws JSONException {
        this.mContext           = context;
        galleryItemsArrayList_  = galleryItemsArrayList;
    }

    @Override
    public int getItemCount() {
        return galleryItemsArrayList_.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gallery, parent, false);

        return new ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();
        holder.uilHandler.loadImage(galleryItemsArrayList_.get(position).image_url_, holder.galleryImg_iv,
                Constants.imageSpecifier.NORMAL);
//

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView   galleryImg_iv;
        UILHandler  uilHandler;

        public ViewHolder(View itemView) {
            super(itemView);

            galleryImg_iv = (ImageView) itemView.findViewById(R.id.gallery_image);
        }
    }

}