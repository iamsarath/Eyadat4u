package app.ens.eyadat4u.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.ServiceList;
import app.ens.eyadat4u.items.MainCategoryItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 1/25/2017.
 */

public class ServiceCategoriesAdapter extends RecyclerView.Adapter<ServiceCategoriesAdapter.ViewHolder> {

    Context                         mContext;
    ArrayList<MainCategoryItems>    mainCategoryItemsArrayList_  = new ArrayList<MainCategoryItems>();
    String                          category_;

    public ServiceCategoriesAdapter(Context context, ArrayList<MainCategoryItems> mainCategoryItemsArrayList, String category) throws JSONException {

        this.mContext               = context;
        mainCategoryItemsArrayList_ = mainCategoryItemsArrayList;
        category_                   = category;
    }

    @Override
    public int getItemCount() {

        return mainCategoryItemsArrayList_.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service_categories_list, parent, false);

        return new ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();
        holder.itemName_tv.setText(trimItemName(mainCategoryItemsArrayList_.get(position).service_name_));
        holder.uilHandler.loadImage(mainCategoryItemsArrayList_.get(position).icon_,
                holder.itemImage_iv , Constants.imageSpecifier.NORMAL);

        holder.parentLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ServiceList.class);
                intent.putExtra("TITLE", category_);
                intent.putExtra("SUB_TITLE", mainCategoryItemsArrayList_.get(position).service_name_);
                intent.putExtra("SERVICE_ID", mainCategoryItemsArrayList_.get(position).service_id_);
                intent.putExtra("SERVICE_TYPE", mainCategoryItemsArrayList_.get(position).service_type_);
                intent.putExtra("CUSTOM_SEARCH", mainCategoryItemsArrayList_.get(position).custom_search_);
                intent.putExtra("VISITING_DOCTOR", false);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout    parentLayout_ll;
        TextView        itemName_tv;
        ImageView       itemImage_iv;
        UILHandler      uilHandler;

        public ViewHolder(View itemView) {
            super(itemView);

            parentLayout_ll = (LinearLayout) itemView.findViewById(R.id.parent);
            itemName_tv     = (TextView) itemView.findViewById(R.id.item_name);
            itemImage_iv    = (ImageView) itemView.findViewById(R.id.item_icon);
        }
    }

    public String trimItemName(String text){

        if(text.length() >= 15){
            String s = text.substring(0,15);
            return s+"..";
        }
        else{

            return text;
        }
    }

}


