package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.TimeSlotItems;

/**
 * Created by Sarath on 1/28/2017.
 */

public class TimeSlotPickerAdapter extends BaseAdapter {

    Context                     mContext;
    LayoutInflater              layoutInflater;
    ArrayList<TimeSlotItems>    timeSlotItemsArrayList_ = new ArrayList<TimeSlotItems>();

    public TimeSlotPickerAdapter(Context context, ArrayList<TimeSlotItems> timeSlotItemsArrayList){
        mContext                = context;
        timeSlotItemsArrayList_ = timeSlotItemsArrayList;
        layoutInflater          = LayoutInflater.from(context);

    }
    @Override
    public int getCount() {
        return timeSlotItemsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_time_slot, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
//
            holder.timeSlot_tv  = (TextView) convertView.findViewById(R.id.time_slot);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                holder.timeSlot_tv.setText(timeSlotItemsArrayList_.get(pos).time_slot_);
            }
        });

        return convertView;
    }

      static class ViewHolder {

        TextView    timeSlot_tv;
        Handler     mHandler;
    }
}