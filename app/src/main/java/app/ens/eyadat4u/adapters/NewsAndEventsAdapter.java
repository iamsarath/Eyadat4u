package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.InsuranceDetails;
import app.ens.eyadat4u.activities.NewsAndEventsDetails;
import app.ens.eyadat4u.items.NewsAndEventsItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.MonthPicker;

/**
 * Created by Sarath on 2/4/2017.
 */

public class NewsAndEventsAdapter extends BaseAdapter {

    Context             mContext;
    LayoutInflater      layoutInflater;
    String[]            itemNameList;
    ArrayList<NewsAndEventsItems> newsEventsItemsArrayList_   = new ArrayList<NewsAndEventsItems>();

    public NewsAndEventsAdapter(Context context, ArrayList<NewsAndEventsItems> newsEventsItemsArrayList){

        mContext                    = context;
        layoutInflater              = LayoutInflater.from(context);
        newsEventsItemsArrayList_   = newsEventsItemsArrayList;
    }

    @Override
    public int getCount() {
        return newsEventsItemsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_news_and_events, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
            holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();

            holder.detailslayout_ll     = (LinearLayout) convertView.findViewById(R.id.read_more);
            holder.itemImage_iv         = (ImageView) convertView.findViewById(R.id.item_image);
            holder.title_tv             = (TextView) convertView.findViewById(R.id.title);
            holder.desc_tv              = (TextView) convertView.findViewById(R.id.description);
            holder.day_tv               = (TextView) convertView.findViewById(R.id.day);
            holder.month_tv             = (TextView) convertView.findViewById(R.id.month);
            holder.year_tv              = (TextView) convertView.findViewById(R.id.year);
            holder.readMore_tv          = (TextView) convertView.findViewById(R.id.read_more_text);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                final String year, month, day;
                String dayString    = newsEventsItemsArrayList_.get(pos).date_;
                String[] separated  = dayString.split("-");
                year                = separated[0];
                month               = separated[1];
                day                 = separated[2];

                holder.title_tv.setText(newsEventsItemsArrayList_.get(pos).title_);
                holder.desc_tv.setText(trimDescription(newsEventsItemsArrayList_.get(pos).description_));
                holder.day_tv.setText(day);
                holder.month_tv.setText(MonthPicker.getMonth(month));
                holder.year_tv.setText(year);

                holder.uilHandler.loadImage(newsEventsItemsArrayList_.get(pos).news_image_,
                        holder.itemImage_iv , Constants.imageSpecifier.NORMAL);

                if(UserManager.getUserLanguage(mContext).equals("en")){

                    holder.readMore_tv.setText(R.string.read_more_en);
                }
                else {

                    holder.readMore_tv.setText(R.string.read_more_ar);
                }

                holder.detailslayout_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(mContext, NewsAndEventsDetails.class);
                        intent.putExtra("ITEM_TITLE", newsEventsItemsArrayList_.get(pos).title_);
                        intent.putExtra("ITEM_DESC", newsEventsItemsArrayList_.get(pos).description_);
                        intent.putExtra("ITEM_IMAGE", newsEventsItemsArrayList_.get(pos).news_image_);
                        intent.putExtra("ITEM_DAY", day);
                        intent.putExtra("ITEM_MONTH", MonthPicker.getMonth(month));
                        intent.putExtra("ITEM_YEAR", year);
                        mContext.startActivity(intent);
                    }
                });
            }
        });

        return convertView;
    }

    public String trimDescription(String text){

        if(text.length() >= 125){
            String s = text.substring(0,125);
            return s+"...";
        }
        else{

            return text;
        }
    }

    static class ViewHolder {

        LinearLayout    detailslayout_ll;
        Handler         mHandler;
        TextView        title_tv, desc_tv, day_tv, month_tv, year_tv, readMore_tv;
        ImageView       itemImage_iv;
        UILHandler      uilHandler;
    }
}