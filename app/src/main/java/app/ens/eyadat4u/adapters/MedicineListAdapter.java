package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.NewsAndEventsDetails;
import app.ens.eyadat4u.items.MedicineItems;
import app.ens.eyadat4u.items.NewsAndEventsItems;

/**
 * Created by Sarath on 1/6/2018.
 */

public class MedicineListAdapter extends BaseAdapter {

    Context                     mContext;
    LayoutInflater              layoutInflater;
    ArrayList<MedicineItems>    medicineItemsArrayList_   = new ArrayList<MedicineItems>();

    public MedicineListAdapter(Context context, ArrayList<MedicineItems> medicineItemsArrayList){

        mContext                    = context;
        layoutInflater              = LayoutInflater.from(context);
        medicineItemsArrayList_     = medicineItemsArrayList;
    }

    @Override
    public int getCount() {
        return medicineItemsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_medicine_list, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();

            holder.name_tv      = (TextView) convertView.findViewById(R.id.medicine_name);
            holder.price_tv     = (TextView) convertView.findViewById(R.id.medicine_price);
            holder.desc_tv      = (TextView) convertView.findViewById(R.id.medicine_description);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                holder.name_tv.setText(medicineItemsArrayList_.get(pos).name_);
                holder.price_tv.setText(medicineItemsArrayList_.get(pos).price_+" KD");
                holder.desc_tv.setText(trimDescription(medicineItemsArrayList_.get(pos).desc_));
            }
        });

        return convertView;
    }

    public String trimDescription(String text){

        if(text.length() >= 125){
            String s = text.substring(0,125);
            return s+"...";
        }
        else{

            return text;
        }
    }

    static class ViewHolder {

        LinearLayout    detailslayout_ll;
        Handler         mHandler;
        TextView        name_tv, desc_tv, price_tv;
    }
}
