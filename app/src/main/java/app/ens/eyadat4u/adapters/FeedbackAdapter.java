package app.ens.eyadat4u.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.Login;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.activities.TimeSlotPicker;
import app.ens.eyadat4u.items.FeedbackItems;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 3/10/2017.
 */

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {

    Context mContext;
    ArrayList<FeedbackItems> feedbackItemsArrayList_  = new ArrayList<FeedbackItems>();

    public FeedbackAdapter(Context context, ArrayList<FeedbackItems> feedbackItemsArrayList) throws JSONException {
        this.mContext             = context;
        feedbackItemsArrayList_   = feedbackItemsArrayList;
    }

    @Override
    public int getItemCount() {

        return feedbackItemsArrayList_.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_feedback, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title_tv.setText(feedbackItemsArrayList_.get(position).title_);
        holder.message_tv.setText(feedbackItemsArrayList_.get(position).message_);
        holder.name_tv.setText(feedbackItemsArrayList_.get(position).user_name_);

        float ratingStarts = Float.parseFloat(feedbackItemsArrayList_.get(position).rating_);
        holder.ratingBar_rb.setRating(ratingStarts);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView    name_tv, title_tv, message_tv;
        RatingBar   ratingBar_rb;

        public ViewHolder(View convertView) {
            super(convertView);

            name_tv          = (TextView) convertView.findViewById(R.id.user_name);
            title_tv         = (TextView) convertView.findViewById(R.id.title);
            message_tv       = (TextView) convertView.findViewById(R.id.message);
            ratingBar_rb     = (RatingBar) convertView.findViewById(R.id.rating_bar);

            LayerDrawable stars = (LayerDrawable) ratingBar_rb .getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.Yellow), PorterDuff.Mode.SRC_ATOP);
        }
    }

}