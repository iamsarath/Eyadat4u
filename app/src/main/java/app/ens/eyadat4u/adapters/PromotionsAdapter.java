package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.InsuranceDetails;
import app.ens.eyadat4u.activities.PromotionDetails;
import app.ens.eyadat4u.items.PromotionItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.MonthPicker;

/**
 * Created by Sarath on 2/4/2017.
 */

public class PromotionsAdapter extends BaseAdapter {

    Context                     mContext;
    LayoutInflater              layoutInflater;
    ArrayList<PromotionItems>   promotionsArrayList_   = new ArrayList<PromotionItems>();

    public PromotionsAdapter(Context context, ArrayList<PromotionItems> promotionsArrayList){
        mContext                = context;
        layoutInflater          = LayoutInflater.from(context);
        promotionsArrayList_    = promotionsArrayList;
    }

    @Override
    public int getCount() {
        return promotionsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView     = layoutInflater.inflate(R.layout.adapter_promotions, null);

            lastPosition    = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
            holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();

            holder.detailslayout_ll     = (LinearLayout) convertView.findViewById(R.id.read_more);
            holder.promoImage_iv        = (ImageView) convertView.findViewById(R.id.promo_image);
            holder.title_tv             = (TextView) convertView.findViewById(R.id.title);
            holder.desc_tv              = (TextView) convertView.findViewById(R.id.description);
            holder.startDate_tv         = (TextView) convertView.findViewById(R.id.start_date);
            holder.endDate_tv           = (TextView) convertView.findViewById(R.id.end_date);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                holder.title_tv.setText(promotionsArrayList_.get(pos).title_);
                holder.desc_tv.setText(trimDescription(promotionsArrayList_.get(pos).description_));
                holder.startDate_tv.setText(promotionsArrayList_.get(pos).start_date_);
                holder.endDate_tv.setText(promotionsArrayList_.get(pos).end_date_);

                holder.uilHandler.loadImage(promotionsArrayList_.get(pos).promo_image_,
                        holder.promoImage_iv , Constants.imageSpecifier.NORMAL);

            }
        });

        holder.detailslayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, PromotionDetails.class);
                intent.putExtra("ITEM_TITLE", promotionsArrayList_.get(pos).title_);
                intent.putExtra("ITEM_DESC", promotionsArrayList_.get(pos).description_);
                intent.putExtra("ITEM_IMAGE", promotionsArrayList_.get(pos).promo_image_);
                intent.putExtra("ITEM_START_DATE", promotionsArrayList_.get(pos).start_date_);
                intent.putExtra("ITEM_END_DATE", promotionsArrayList_.get(pos).end_date_);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    public String trimDescription(String text){

        if(text.length() >= 75){
            String s = text.substring(0,75);
            return s+"...";
        }
        else{

            return text;
        }
    }

    /* alert dialogue for cart item removal confirmation */
    private void showAlertDialogue(String title, String msg, final int position) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }

    static class ViewHolder {

        LinearLayout    detailslayout_ll;
        Handler         mHandler;
        TextView        title_tv, desc_tv, startDate_tv, endDate_tv;
        ImageView       promoImage_iv;
        UILHandler      uilHandler;
    }
}