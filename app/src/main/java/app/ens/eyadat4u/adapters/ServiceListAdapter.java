package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.Login;
import app.ens.eyadat4u.activities.MainActivity;
import app.ens.eyadat4u.activities.MedicalServices;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.activities.TimeSlotPicker;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 1/27/2017.
 */

public class ServiceListAdapter extends BaseAdapter {

    Context                 mContext;
    LayoutInflater          layoutInflater;
    ArrayList<SearchItems>  searchItemsArrayList_   = new ArrayList<SearchItems>();
    String                  customSearch_;

    public ServiceListAdapter(Context context, ArrayList<SearchItems> searchItemsArrayList, String customSearch){
        mContext                = context;
        searchItemsArrayList_   = searchItemsArrayList;
        layoutInflater          = LayoutInflater.from(context);
        customSearch_           = customSearch;
    }

    @Override
    public int getCount() {
        return searchItemsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_service_list, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
            holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();
//
            holder.detailslayout_ll         = (LinearLayout) convertView.findViewById(R.id.details);
            holder.bookAppointment_ll       = (LinearLayout) convertView.findViewById(R.id.book_appointment);
            holder.servicesLayout_ll        = (LinearLayout) convertView.findViewById(R.id.specialisation_layout);
            holder.name_tv                  = (TextView) convertView.findViewById(R.id.name);
            holder.qualification_tv         = (TextView) convertView.findViewById(R.id.qualif_exp);
            holder.services_tv              = (TextView) convertView.findViewById(R.id.clinic_services);
            holder.clinicName_tv            = (TextView) convertView.findViewById(R.id.clinic_name);
            holder.location_tv              = (TextView) convertView.findViewById(R.id.location);
            holder.itemImage_iv             = (ImageView) convertView.findViewById(R.id.item_image);
            holder.rating_tv                = (TextView) convertView.findViewById(R.id.rating);
            holder.views_tv                 = (TextView) convertView.findViewById(R.id.views);
            holder.specialityOrService_tv   = (TextView) convertView.findViewById(R.id.speciality_service);
            holder.viewDetailsText_tv       = (TextView) convertView.findViewById(R.id.view_details_text);
            holder.bookAppointment_tv       = (TextView) convertView.findViewById(R.id.book_appointment_text);
            holder.ratingBar_rb             = (RatingBar) convertView.findViewById(R.id.rating_bar);

            LayerDrawable stars = (LayerDrawable) holder.ratingBar_rb .getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.Yellow), PorterDuff.Mode.SRC_ATOP);

            holder.name_tv.setVisibility(View.GONE);
            holder.qualification_tv.setVisibility(View.GONE);
            holder.clinicName_tv.setVisibility(View.GONE);
            holder.detailslayout_ll.setVisibility(View.GONE);
            holder.bookAppointment_ll.setVisibility(View.GONE);
            holder.servicesLayout_ll.setVisibility(View.GONE);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                String docText, clinicText;

                if(UserManager.getUserLanguage(mContext).equals("en")){

                    docText     = "DOCTOR";
                    clinicText  = "CLINIC";
                }else{

                    docText     = "طبيب";
                    clinicText  = "عيادة";
                }

                holder.detailslayout_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String docText, clinicText;

                        if(UserManager.getUserLanguage(mContext).equals("en")){

                            docText     = "DOCTOR";
                            clinicText  = "CLINIC";
                        }else{

                            docText     = "طبيب";
                            clinicText  = "عيادة";
                        }

                        if(searchItemsArrayList_.get(pos).type_.equals(docText)){

                            Intent intent = new Intent(mContext, ServiceDetails.class);
                            intent.putExtra("KEY_TYPE", "DOCTOR");
                            intent.putExtra("ITEM_NAME", searchItemsArrayList_.get(pos).item_name_);
                            intent.putExtra("CLINIC_NAME", searchItemsArrayList_.get(pos).clinic_name_);
                            intent.putExtra("ITEM_PIC", searchItemsArrayList_.get(pos).item_thumb_);
                            intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                            intent.putExtra("CUSTOM_SEARCH", customSearch_);
                            mContext.startActivity(intent);
                        }
                        else if(searchItemsArrayList_.get(pos).type_.equals(clinicText)){

                            Intent intent = new Intent(mContext, ServiceDetails.class);
                            intent.putExtra("KEY_TYPE", "CLINIC");
                            intent.putExtra("ITEM_NAME", searchItemsArrayList_.get(pos).item_name_);
                            intent.putExtra("CLINIC_NAME", searchItemsArrayList_.get(pos).clinic_name_);
                            intent.putExtra("ITEM_PIC", searchItemsArrayList_.get(pos).item_thumb_);
                            intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                            intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                            intent.putExtra("CUSTOM_SEARCH", customSearch_);
                            mContext.startActivity(intent);
                        }
                        else {

                            Intent intent = new Intent(mContext, ServiceDetails.class);
                            intent.putExtra("KEY_TYPE", "OTHERS");
                            intent.putExtra("ITEM_NAME", searchItemsArrayList_.get(pos).item_name_);
                            intent.putExtra("CLINIC_NAME", searchItemsArrayList_.get(pos).clinic_name_);
                            intent.putExtra("ITEM_PIC", searchItemsArrayList_.get(pos).item_thumb_);
                            intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                            intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                            intent.putExtra("CUSTOM_SEARCH", customSearch_);
                            mContext.startActivity(intent);
                        }

                    }
                });

                if(searchItemsArrayList_.get(pos).type_.equals(docText)){

                    holder.name_tv.setVisibility(View.VISIBLE);
                    holder.qualification_tv.setVisibility(View.VISIBLE);
                    holder.clinicName_tv.setVisibility(View.VISIBLE);
                    holder.bookAppointment_ll.setVisibility(View.VISIBLE);
                    holder.detailslayout_ll.setVisibility(View.VISIBLE);
                    holder.servicesLayout_ll.setVisibility(View.GONE);
//                    holder.specialityOrService_tv.setText("Speciality");
                }
                else{

                    holder.name_tv.setVisibility(View.VISIBLE);
                    holder.qualification_tv.setVisibility(View.GONE);
                    holder.clinicName_tv.setVisibility(View.GONE);
                    holder.bookAppointment_ll.setVisibility(View.GONE);
                    holder.detailslayout_ll.setVisibility(View.VISIBLE);
                    holder.servicesLayout_ll.setVisibility(View.GONE);
                }

                if(UserManager.getUserLanguage(mContext).equals("en")){

                    holder.viewDetailsText_tv.setText(R.string.view_details_en);
                    holder.bookAppointment_tv.setText(R.string.book_appointment_en);
                }else{

                    holder.viewDetailsText_tv.setText(R.string.view_details_ar);
                    holder.bookAppointment_tv.setText(R.string.book_appointment_ar);
                }

                if(UserManager.getUserLanguage(mContext).equals("en")){
                    holder.qualification_tv.setText(searchItemsArrayList_.get(position).qualification_+" - "+
                            searchItemsArrayList_.get(position).experience_+" yrs exp");
                }else{
                    holder.qualification_tv.setText(searchItemsArrayList_.get(position).qualification_+" - "+
                            searchItemsArrayList_.get(position).experience_+" سنوات إكس");
                }

                holder.specialityOrService_tv.setText(searchItemsArrayList_.get(pos).type_name_);
                holder.name_tv.setText(trimItemName(searchItemsArrayList_.get(pos).item_name_));
//                holder.qualification_tv.setText(trimQualifications(searchItemsArrayList_.get(pos).qualification_)+" - "+searchItemsArrayList_.get(pos).experience_+" yrs exp");
                holder.services_tv.setText("");
                holder.clinicName_tv.setText(trimClinicName(searchItemsArrayList_.get(pos).clinic_name_));
                holder.location_tv.setText(searchItemsArrayList_.get(pos).location_);
                holder.rating_tv.setText(searchItemsArrayList_.get(pos).rating_);
                holder.views_tv.setText(searchItemsArrayList_.get(pos).views_);

                float ratingStarts = Float.parseFloat(searchItemsArrayList_.get(pos).rating_);
                holder.ratingBar_rb.setRating(ratingStarts);

                holder.uilHandler.loadImage(searchItemsArrayList_.get(pos).item_thumb_,
                        holder.itemImage_iv , Constants.imageSpecifier.NORMAL);
            }


        });



        holder.bookAppointment_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UserManager.getUserLoginStatus(mContext)){

                    Intent myIntent = new Intent(mContext, TimeSlotPicker.class);
                    myIntent.putExtra("ITEM_ID",  searchItemsArrayList_.get(pos).item_id_);
                    myIntent.putExtra("ITEM_NAME",  searchItemsArrayList_.get(pos).item_name_);
                    myIntent.putExtra("CLINIC_NAME",  searchItemsArrayList_.get(pos).clinic_name_);
                    myIntent.putExtra("ITEM_PIC",   searchItemsArrayList_.get(pos).item_thumb_);
                    myIntent.putExtra("LOCATION",  searchItemsArrayList_.get(pos).location_);
                    mContext.startActivity(myIntent);
                }
                else{

                    showAlertDialogue("Login", "You need to login for booking appointments");
                }

            }
        });

        return convertView;
    }

    public String trimItemName(String text){

        if(text.length() >= 35){
            String s = text.substring(0,35);
            return s+"...";
        }
        else{

            return text;
        }
    }

    public String trimClinicName(String text){

        if(text.length() >= 18){
            String s = text.substring(0,18);
            return s+"...";
        }
        else{

            return text;
        }
    }

    public String trimQualifications(String text){

        if(text.length() >= 15){
            String s = text.substring(0,15);
            return s+"...";
        }
        else{

            return text;
        }
    }

    /* alert dialogue  */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent myIntent = new Intent(mContext, Login.class);
                        mContext.startActivity(myIntent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }

    static class ViewHolder {

        LinearLayout    detailslayout_ll, bookAppointment_ll, servicesLayout_ll;
        Handler         mHandler;
        TextView        name_tv, qualification_tv, services_tv, clinicName_tv, location_tv, rating_tv, views_tv,
                        specialityOrService_tv, viewDetailsText_tv, bookAppointment_tv;
        ImageView       itemImage_iv;
        RatingBar       ratingBar_rb;
        UILHandler      uilHandler;
    }
}