package app.ens.eyadat4u.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.ChatMessage;
import app.ens.eyadat4u.system.UserManager;

/**
 * Created by Sarath on 3/6/2017.
 */
public class ChatAdapter extends ArrayAdapter<ChatMessage> {

    private TextView chatText, chatTime;
    private List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();
    private Context context;

    @Override
    public void add(ChatMessage object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public ChatAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public ChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ChatMessage chatMessageObj = getItem(position);
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        int myNum = 0;

        try {
            myNum = Integer.parseInt(UserManager.getUserId(context));
        } catch(NumberFormatException nfe) {
        }

        if (chatMessageObj.is_sender_.equals("8")) {
//        if (chatMessageObj.is_sender_ == myNum) {

            row = inflater.inflate(R.layout.right, parent, false);
        }else{

            row = inflater.inflate(R.layout.left, parent, false);
        }
        chatText = (TextView) row.findViewById(R.id.text);
        chatTime  = (TextView) row.findViewById(R.id.time_stamp);
        chatText.setText(chatMessageObj.message_);
        chatTime.setText(chatMessageObj.time_);
        return row;
    }
}