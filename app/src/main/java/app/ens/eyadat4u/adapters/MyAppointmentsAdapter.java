package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.BookAppointment;
import app.ens.eyadat4u.activities.GiveFeedback;
import app.ens.eyadat4u.activities.Login;
import app.ens.eyadat4u.activities.MainActivity;
import app.ens.eyadat4u.activities.MyAccount;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.activities.TimeSlotPicker;
import app.ens.eyadat4u.fragments.MyAppointmentsFragment;
import app.ens.eyadat4u.items.MyAppointmentItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.system.UserManager;
import app.ens.eyadat4u.utils.CheckNetwork;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;

/**
 * Created by Sarath on 2/20/2017.
 */
public class MyAppointmentsAdapter extends BaseAdapter {

    Context                         mContext;
    LayoutInflater                  layoutInflater;
    ArrayList<MyAppointmentItems>   searchItemsArrayList_   = new ArrayList<MyAppointmentItems>();
    RequestQueue                    queue;
    JsonObjectRequest               jsonObjectRequest;
    ProgressDialog                  progressdialog;

    public MyAppointmentsAdapter(Context context, ArrayList<MyAppointmentItems> searchItemsArrayList){

        mContext                = context;
        searchItemsArrayList_   = searchItemsArrayList;
        layoutInflater          = LayoutInflater.from(context);
        queue                   = Volley.newRequestQueue(mContext);
    }

    @Override
    public int getCount() {
        return searchItemsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_my_appointments, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
            holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();
//
            holder.detailslayout_ll             = (LinearLayout) convertView.findViewById(R.id.details);
            holder.cancelAppointment_ll         = (LinearLayout) convertView.findViewById(R.id.cancel_appointment);
            holder.giveFeedback_ll              = (LinearLayout) convertView.findViewById(R.id.give_feedback);
            holder.servicesLayout_ll            = (LinearLayout) convertView.findViewById(R.id.specialisation_layout);
            holder.name_tv                      = (TextView) convertView.findViewById(R.id.name);
            holder.qualification_tv             = (TextView) convertView.findViewById(R.id.qualif_exp);
            holder.clinicName_tv                = (TextView) convertView.findViewById(R.id.clinic_name);
            holder.itemImage_iv                 = (ImageView) convertView.findViewById(R.id.item_image);
            holder.appointDate_tv               = (TextView) convertView.findViewById(R.id.appoint_date);
            holder.fromTime_tv                  = (TextView) convertView.findViewById(R.id.from_time);
            holder.toTime_tv                    = (TextView) convertView.findViewById(R.id.to_time);
            holder.cancelAppointmentsText_tv    = (TextView) convertView.findViewById(R.id.cancel_appointments_text);
            holder.appointmentsOnText_tv        = (TextView) convertView.findViewById(R.id.appointments_on_text);
            holder.feedbackText_tv              = (TextView) convertView.findViewById(R.id.feedback_text);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                if(UserManager.getUserLanguage(mContext).equals("en")){

                    holder.cancelAppointmentsText_tv.setText(R.string.cancel_appointment_en);
                    holder.feedbackText_tv.setText(R.string.give_feedback_en);
                    holder.appointmentsOnText_tv.setText(R.string.appointment_on_en);
                    holder.qualification_tv.setText(searchItemsArrayList_.get(position).qualification_+" - "+
                            searchItemsArrayList_.get(position).experience_+" yrs exp");
                }
                else {

                    holder.cancelAppointmentsText_tv.setText(R.string.cancel_appointment_ar);
                    holder.feedbackText_tv.setText(R.string.give_feedback_ar);
                    holder.appointmentsOnText_tv.setText(R.string.appointment_on_ar);
                    holder.qualification_tv.setText(searchItemsArrayList_.get(position).qualification_+" - "+
                            searchItemsArrayList_.get(position).experience_+" سنوات إكس");
                }

                holder.appointDate_tv.setText(searchItemsArrayList_.get(pos).appointment_date_);

                if(searchItemsArrayList_.get(pos).item_name_ != null){

                    holder.name_tv.setText(trimItemName(searchItemsArrayList_.get(pos).item_name_));
                }

//                holder.qualification_tv.setText(searchItemsArrayList_.get(pos).qualification_+" - "+searchItemsArrayList_.get(pos).experience_+" yrs exp");
                holder.clinicName_tv.setText(searchItemsArrayList_.get(pos).clinic_name_);
                holder.fromTime_tv.setText(searchItemsArrayList_.get(pos).appointment_from_time_);
                holder.toTime_tv.setText(searchItemsArrayList_.get(pos).appointment_to_time_);

                holder.uilHandler.loadImage(searchItemsArrayList_.get(pos).item_thumb_,
                        holder.itemImage_iv , Constants.imageSpecifier.NORMAL);
            }
        });

        holder.detailslayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intent = new Intent(mContext, ServiceDetails.class);
                    intent.putExtra("KEY_TYPE", "DOCTOR");
                    intent.putExtra("ITEM_NAME", searchItemsArrayList_.get(pos).item_name_);
                    intent.putExtra("CLINIC_NAME", searchItemsArrayList_.get(pos).clinic_name_);
                    intent.putExtra("ITEM_PIC", searchItemsArrayList_.get(pos).item_thumb_);
                    intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                    mContext.startActivity(intent);
            }
        });

        holder.cancelAppointment_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cancelAppointmentConfirmDialogue("Cancel Appointment ?","Are you sure want to cancel the appoinment of "
                        +searchItemsArrayList_.get(pos).item_name_+" on "+searchItemsArrayList_.get(pos).appointment_date_, position);
            }
        });

        holder.giveFeedback_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, GiveFeedback.class);
                intent.putExtra("ITEM_NAME", searchItemsArrayList_.get(pos).item_name_);
                intent.putExtra("ITEM_IMAGE",  searchItemsArrayList_.get(pos).item_thumb_);
                intent.putExtra("ITEM_ID", searchItemsArrayList_.get(pos).item_id_);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    private void cancelAppointmentConfirmDialogue(String title, String msg, final int position) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        progressdialog = new ProgressDialog(mContext);
                        progressdialog.setMessage("Canceling please wait...");
                        progressdialog.setCancelable(false);
                        progressdialog.show();

                        String obj = cancelAppointmentObj( UserManager.getUserLanguage(mContext), UserManager.getUserId(mContext),
                                UserManager.getUserApiKey(mContext),searchItemsArrayList_.get(position).appointment_id_);
                        cancelAppointmentApi(UrlDispatcher.getUrl(Constants.UrlSpecifier.URL_CANCEL_APPOINTMENT), obj, position);

                    }
                }).setNegativeButton("Don't Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        })
                .create().show();
    }

    private String cancelAppointmentObj(String language, String userId, String apiKey, String appointmentId) {

        JSONObject mainObj = new JSONObject();

        try {

            mainObj.put( "language", language);
            mainObj.put( "user_id", userId);
            mainObj.put( "api_key", apiKey);
            mainObj.put( "appointment_id", appointmentId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mainObj.toString();
    }


    public String trimItemName(String text){

        if(text.length() >= 35){
            String s = text.substring(0,35);
            return s+"...";
        }
        else{

            return text;
        }
    }

    public void cancelAppointmentApi(String url, String obj, final int position){

        if(!CheckNetwork.isInternetAvailable(mContext))
        {
            try {

                showSnackBar("Bad Network !, Please check your internet connection");

            } catch(Exception e) {

                e.printStackTrace();
            }
        }
        else {

            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        String result =response.toString();

                        JSONObject jObj;
                        try {

                            jObj = new JSONObject(result);

                            //Validating the api
                            if(jObj.has("status")){

                                if(jObj.getBoolean("status")) {

                                    progressdialog.dismiss();
                                    if(jObj.has("message")) {

                                        showSnackBar(jObj.getString("message"));
                                    }

                                    searchItemsArrayList_.remove(position);
                                    MyAppointmentsFragment.myAppointmentsAdapter.notifyDataSetChanged();

                                    if(searchItemsArrayList_.size() ==0){

                                        MyAppointmentsFragment.showNoDataView();
                                    }
                                    progressdialog.dismiss();

                                } else{

                                    progressdialog.dismiss();
                                }
                            }
                            else{

                                if(jObj.has("message")){

                                    showSnackBar(jObj.getString("message"));
                                }
                                else{
                                }

                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } catch (Exception b) {
                        b.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showSnackBar("Retry !, Can't reach our server right now, please retry");
                        }
                    }
                    error.printStackTrace();
                }
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    150000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonObjectRequest);

        }
    }
    /* alert dialogue  */
    private void showAlertDialogue(String title, String msg) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent myIntent = new Intent(mContext, Login.class);
                        mContext.startActivity(myIntent);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        })
                .create().show();
    }

    public void showSnackBar(String msg){

        Snackbar snackbar = Snackbar
                .make(MyAccount.parentLayout_ll, ""+msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundResource(R.color.ColorPrimary);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    static class ViewHolder {

        LinearLayout    detailslayout_ll, cancelAppointment_ll, servicesLayout_ll, giveFeedback_ll;
        Handler         mHandler;
        TextView        name_tv, qualification_tv, clinicName_tv, appointmentsOnText_tv, cancelAppointmentsText_tv,
                        fromTime_tv, toTime_tv, appointDate_tv, feedbackText_tv;
        ImageView       itemImage_iv;
        UILHandler      uilHandler;
    }
}