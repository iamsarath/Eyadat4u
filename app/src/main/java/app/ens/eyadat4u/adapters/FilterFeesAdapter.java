package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.Filter;
import app.ens.eyadat4u.items.FilterFeesCheckItems;
import app.ens.eyadat4u.items.FilterTimeCheckItems;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 3/3/2017.
 */

public class FilterFeesAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater layoutInflater;
    String [] feesArray;

    public FilterFeesAdapter(Context context){

        mContext                = context;
        layoutInflater          = LayoutInflater.from(context);
        setFeesArrayList();
    }

    void setFeesArrayList() {

        feesArray = new String[4];
        feesArray [0] = "Below 50KD";
        feesArray [1] = "B/W 50KD & 100KD";
        feesArray [2] = "B/W 100KD & 500KD";
        feesArray [3] = "Above 500KD";
    }

    @Override
    public int getCount() {
        return Constants.filterFeesCheckItemsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_filter_time_fees, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();

            holder.fees_tv              = (TextView) convertView.findViewById(R.id.day_name);
            holder.feesLayout_ll        = (LinearLayout) convertView.findViewById(R.id.days_layout);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                if(Constants.filterFeesCheckItemsArrayList.get(pos).is_checked_){

                    holder.fees_tv.setTextColor(mContext.getResources().getColor(R.color.ColorPrimary));
                }
                else{

                    holder.fees_tv.setTextColor(mContext.getResources().getColor(R.color.TextMain));
                }

                holder.fees_tv.setText(Constants.filterFeesCheckItemsArrayList.get(pos).fees_);

            }
        });

        holder.feesLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateTimeArrayList(position);
                Filter.filterFeesAdapter.notifyDataSetChanged();
                Filter.validateFilterSelection();

            }
        });

        return convertView;
    }

    void updateTimeArrayList(int position) {

        for (int i=0; i< 4; i++){

            if(i==position){

                FilterFeesCheckItems tempCheckList = new FilterFeesCheckItems(feesArray[i], true);
                Constants.filterFeesCheckItemsArrayList.set(i, tempCheckList);
            }
            else{

                FilterFeesCheckItems tempCheckList = new FilterFeesCheckItems(feesArray[i], false);
                Constants.filterFeesCheckItemsArrayList.set(i, tempCheckList);
            }
        }
    }

    static class ViewHolder {

        TextView        fees_tv;
        LinearLayout    feesLayout_ll;
        Handler         mHandler;
    }
}