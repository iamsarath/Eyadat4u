package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.InsuranceDetails;
import app.ens.eyadat4u.activities.ServiceDetails;
import app.ens.eyadat4u.items.InsuranceItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 2/4/2017.
 */

public class InsuranceAdapter extends BaseAdapter {

    Context                     mContext;
    LayoutInflater              layoutInflater;
    ArrayList<InsuranceItems>   insuranceItemsList_   = new ArrayList<InsuranceItems>();

    public InsuranceAdapter(Context context, ArrayList<InsuranceItems> insuranceItemsList){
        mContext            = context;
        layoutInflater      = LayoutInflater.from(context);
        insuranceItemsList_ = insuranceItemsList;
    }

    @Override
    public int getCount() {
        return insuranceItemsList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_insurance, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
            holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();

            holder.detailslayout_ll     = (LinearLayout) convertView.findViewById(R.id.read_more);
            holder.promoImage_iv        = (ImageView) convertView.findViewById(R.id.promo_image);
            holder.title_tv             = (TextView) convertView.findViewById(R.id.title);
            holder.desc_tv              = (TextView) convertView.findViewById(R.id.description);
            holder.price_tv             = (TextView) convertView.findViewById(R.id.price);
            holder.companyName_tv       = (TextView) convertView.findViewById(R.id.company_name);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                holder.title_tv.setText(insuranceItemsList_.get(pos).title_);
                holder.desc_tv.setText(trimDescription(insuranceItemsList_.get(pos).description_));
                holder.companyName_tv.setText(insuranceItemsList_.get(pos).company_name_);
                holder.price_tv.setText(insuranceItemsList_.get(pos).amount_+" KD");

                holder.uilHandler.loadImage(insuranceItemsList_.get(pos).image_,
                        holder.promoImage_iv , Constants.imageSpecifier.NORMAL);
            }
        });

        holder.detailslayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, InsuranceDetails.class);
                intent.putExtra("ITEM_TITLE", insuranceItemsList_.get(pos).title_);
                intent.putExtra("ITEM_DESC", insuranceItemsList_.get(pos).description_);
                intent.putExtra("COMPANY_NAME", insuranceItemsList_.get(pos).company_name_);
                intent.putExtra("PRICING", insuranceItemsList_.get(pos).amount_);
                intent.putExtra("ITEM_IMAGE", insuranceItemsList_.get(pos).image_);
                intent.putExtra("ITEM_ELIGIBILITY", insuranceItemsList_.get(pos).eligibility_);
                intent.putExtra("ITEM_BENEFITS", insuranceItemsList_.get(pos).benefit_);
                intent.putExtra("ITEM_RENEWAL_POLICY", insuranceItemsList_.get(pos).renewal_policy_);
                intent.putExtra("ITEM_EXCLUSION", insuranceItemsList_.get(pos).exclusion_);
                mContext.startActivity(intent);
            }
        });


        return convertView;
    }

    public String trimDescription(String text){

        if(text.length() >= 125){
            String s = text.substring(0,125);
            return s+"...";
        }
        else{

            return text;
        }
    }

    static class ViewHolder {
        LinearLayout    detailslayout_ll;
        Handler         mHandler;
        TextView        title_tv, desc_tv, companyName_tv, price_tv;
        ImageView       promoImage_iv;
        UILHandler      uilHandler;
    }
}