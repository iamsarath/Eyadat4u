package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.activities.Filter;
import app.ens.eyadat4u.items.FilterFeesCheckItems;
import app.ens.eyadat4u.items.FilterTimeCheckItems;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 3/3/2017.
 */

public class FilterTimeAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater layoutInflater;
    String [] timeArray;

    public FilterTimeAdapter(Context context){
        mContext                = context;
        layoutInflater          = LayoutInflater.from(context);
        setTimeArrayList();
    }

    void setTimeArrayList() {

        timeArray = new String[4];
        timeArray [0] = "Before 12:00 PM";
        timeArray [1] = "12:00 PM To 4:00 PM";
        timeArray [2] = "4:00 PM To 8:00 PM";
        timeArray [3] = "After 8:00 PM";
    }

    @Override
    public int getCount() {
        return  Constants.filterTimeCheckItemsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_filter_time_fees, null);

            lastPosition = position;

            holder                  = new ViewHolder();
            holder.mHandler         = new Handler();
//
            holder.time_tv          = (TextView) convertView.findViewById(R.id.day_name);
            holder.timeLayout_ll    = (LinearLayout) convertView.findViewById(R.id.days_layout);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                if(Constants.filterTimeCheckItemsArrayList.get(pos).is_checked_){

                    holder.time_tv.setTextColor(mContext.getResources().getColor(R.color.ColorPrimary));
                }
                else{

                    holder.time_tv.setTextColor(mContext.getResources().getColor(R.color.TextMain));
                }

                holder.time_tv.setText(Constants.filterTimeCheckItemsArrayList.get(pos).time_);

            }
        });

        holder.timeLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateFeesArrayList(position);
                Filter.filterTimeAdapter.notifyDataSetChanged();
                Filter.validateFilterSelection();

            }
        });

        return convertView;
    }

    void updateFeesArrayList(int position) {

        for (int i=0; i< 4; i++){

            if(i==position){

                FilterTimeCheckItems tempCheckList = new FilterTimeCheckItems(timeArray[i], true);
                Constants.filterTimeCheckItemsArrayList.set(i, tempCheckList);
            }
            else{

                FilterTimeCheckItems tempCheckList = new FilterTimeCheckItems(timeArray[i], false);
                Constants.filterTimeCheckItemsArrayList.set(i, tempCheckList);
            }
        }
    }

    static class ViewHolder {

        TextView        time_tv;
        LinearLayout    timeLayout_ll;
        Handler         mHandler;
    }
}