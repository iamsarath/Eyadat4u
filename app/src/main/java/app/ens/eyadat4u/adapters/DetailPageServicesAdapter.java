package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.ServicesItems;

/**
 * Created by Sarath on 1/28/2017.
 */

public class DetailPageServicesAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater layoutInflater;
    ArrayList<ServicesItems> servicesItemsArrayList_   = new ArrayList<ServicesItems>();

    public DetailPageServicesAdapter(Context context, ArrayList<ServicesItems> servicesItemsArrayList){
        mContext                = context;
        servicesItemsArrayList_ = servicesItemsArrayList;
        layoutInflater          = LayoutInflater.from(context);

    }
    @Override
    public int getCount() {
        return servicesItemsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_detailpage_services, null);

            lastPosition = position;

            holder              = new ViewHolder();
            holder.mHandler     = new Handler();
//
            holder.serviceName_tv    = (TextView) convertView.findViewById(R.id.service_name);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                holder.serviceName_tv.setText(servicesItemsArrayList_.get(pos).service_name_);
            }
        });


        return convertView;
    }

    static class ViewHolder {

        TextView serviceName_tv;
        Handler         mHandler;
    }
}