package app.ens.eyadat4u.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.BannerItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;
import app.ens.eyadat4u.utils.UrlDispatcher;


/**
 * Created by Sarath on 2/13/2016.
 */

public class BannerPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context_;
    ArrayList<BannerItems> bannerList_;
    LayoutInflater inflater;
    UILHandler uilHandler;

    public BannerPagerAdapter(Context context, ArrayList<BannerItems> bannerList) {
//        public BannerPagerAdapter(Context context, ArrayList<BannerItems> bannerItemsArrayList) {
        context_        = context;
        bannerList_     = bannerList;
        uilHandler      = (UILHandler) ((Activity)context).getApplication();
    }

    @Override
    public int getCount() {
        return bannerList_.size();
    }

//    @Override
//    public int getCount() {
//        return 3;
//    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        ImageView       bannerImage_iv;
        TextView        bannerText_tv;
        LinearLayout    bannerTextLayout_ll;

        inflater = (LayoutInflater) context_
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.adapter_banner_images, container,
                false);

        // Locate the ImageView in viewpager_item.xml
        bannerImage_iv      = (ImageView) itemView.findViewById(R.id.banner_image);
        bannerText_tv       = (TextView) itemView.findViewById(R.id.banner_text);
        bannerTextLayout_ll = (LinearLayout) itemView.findViewById(R.id.banner_text_layout);

//        bannerText_tv.setText(bannerText[position]);
//        uilHandler.loadImage(bannerImg[position], bannerImage_iv,
//                Constants.imageSpecifier.NORMAL);

        bannerTextLayout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bannerList_.get(position).banner_url_));
                context_.startActivity(browserIntent);
            }
        });

        uilHandler.loadImage(bannerList_.get(position).banner_image_, bannerImage_iv,
                Constants.imageSpecifier.NORMAL);
        bannerText_tv.setText(bannerList_.get(position).banner_text_);

        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}