package app.ens.eyadat4u.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.ens.eyadat4u.R;
import app.ens.eyadat4u.items.SearchItems;
import app.ens.eyadat4u.system.UILHandler;
import app.ens.eyadat4u.utils.Constants;

/**
 * Created by Sarath on 1/30/2017.
 */

public class BestDoctorsAdapter extends BaseAdapter {

    Context                 mContext;
    LayoutInflater          layoutInflater;
    ArrayList<SearchItems>  bestDocsArrayList_    = new ArrayList<SearchItems>();

    public BestDoctorsAdapter(Context context, ArrayList<SearchItems> bestDocsArrayList) {
        mContext             = context;
        layoutInflater       = LayoutInflater.from(context);
        bestDocsArrayList_   = bestDocsArrayList;

    }

    @Override
    public int getCount() {
        return bestDocsArrayList_.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int lastPosition = -1;

    @SuppressLint("NewApi")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.adapter_best_doctors, null);

            lastPosition = position;

            holder = new ViewHolder();
            holder.mHandler = new Handler();
            holder.uilHandler   = (UILHandler) ((Activity)mContext).getApplication();

            holder.itemImage_iv    = (ImageView) convertView.findViewById(R.id.item_image);
            holder.itemName_tv  = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemExpQual_tv  = (TextView) convertView.findViewById(R.id.item_qual_exp);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final int pos = position;
        holder.mHandler.post(new Runnable() {
            @Override
            public void run() {

                holder.itemName_tv.setText(trimItemName(bestDocsArrayList_.get(pos).item_name_));
                holder.itemExpQual_tv.setText(bestDocsArrayList_.get(pos).qualification_+" - "+
                        bestDocsArrayList_.get(pos).experience_+" yrs exp");

                holder.uilHandler.loadImage(bestDocsArrayList_.get(pos).item_thumb_,
                        holder.itemImage_iv , Constants.imageSpecifier.NORMAL);

            }
        });


        return convertView;
    }

    public String trimItemName(String text){

        if(text.length() >= 35){
            String s = text.substring(0,35);
            return s+"...";
        }
        else{

            return text;
        }
    }

    static class ViewHolder {

        ImageView   itemImage_iv;
        TextView    itemName_tv, itemExpQual_tv;
        Handler     mHandler;
        UILHandler  uilHandler;
    }

}